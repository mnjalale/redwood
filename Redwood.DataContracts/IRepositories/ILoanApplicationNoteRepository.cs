﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;

namespace Redwood.DataContracts.IRepositories
{
    public interface ILoanApplicationNoteRepository : IRepositoryBase<LoanApplicationNote>
    {
    }
}
