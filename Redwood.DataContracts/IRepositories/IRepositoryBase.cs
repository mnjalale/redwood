﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.DataContracts.IRepositories
{
    public interface IRepositoryBase<T>
    {
        IQueryable<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "", bool showInactive = false);

        T GetById(object id);

        void Delete(object id);

        void Delete(T entityToDelete);

        void InsertOrUpdate(T entity);

        void Inactivate(int id);

        void Activate(int id);

        void Restore(int id);

        IEnumerable<T> GetAddedEntities();
    }
}
