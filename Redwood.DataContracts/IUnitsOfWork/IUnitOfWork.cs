﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.DataContracts.IRepositories;

namespace Redwood.DataContracts.IUnitsOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        ICustomerRepository CustomerRepository { get; }
        IInvitationRepository InvitationRepository { get; }
        ILoanAmortizationScheduleRepository LoanAmortizationScheduleRepository { get; }
        ILoanApplicationRepository LoanApplicationRepository { get; }
        ILoanApplicationAppraisalRepository LoanApplicationAppraisalRepository { get; }
        ILoanApplicationDocumentRepository LoanApplicationDocumentRepository { get; }
        ILoanApplicationNoteRepository LoanApplicationNoteRepository { get;}
        ILoanRepository LoanRepository { get; }
        ILoanPaymentRepository LoanPaymentRepository { get; }
        IOrganizationRepository OrganizationRepository { get; }
        IOrganizationWalletRepository OrganizationWalletRepository { get; }
        IProductCategoryRepository ProductCategoryRepository { get; }
        IProductFeeRepository ProductFeeRepository { get; }
        IProductRepository ProductRepository { get; }
        
    }
}
