﻿(function () {
    'use strict';

    angular.module('app').factory('walletService', ['$http', '$q', 'localStorageService', 'appSettings', walletService]);

    function walletService($http, $q, localStorageService, appSettings) {
        var authData = localStorageService.get('authorizationData');
        var walletBaseUrl = appSettings.walletApiServiceBaseUri,
            baseUrl = appSettings.apiServiceBaseUri,
            service = {
                getWalletByWalletPhoneNo: getWalletByWalletPhoneNo,
                saveWallet: saveWallet,
                saveOrganizationWallet: saveOrganizationWallet,
                getOrganizationWallets: getOrganizationWallets,
                saveWalletAccount: saveWalletAccount,
                getWalletAccounts: getWalletAccounts,
                saveWalletTransaction: saveWalletTransaction,
                getWalletTransactions: getWalletTransactions,
                getOrganizationLogo: getOrganizationLogo,
                getWalletName: getWalletName,
                getWalletBanks: getWalletBanks,
                getWalletCurrencies:getWalletCurrencies
            };
        return service;

        function getWalletByWalletPhoneNo(walletPhoneNumber) {
            var deferred = $q.defer();
            var url = walletBaseUrl + 'api/wallet/getByWalletPhoneNumber';

            $http.get(url, { params: { walletPhoneNumber: walletPhoneNumber } }).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var data = response.data;
                deferred.resolve(data);
            }
        }

        function getWalletBanks() {
            var deferred = $q.defer();
            var url = walletBaseUrl + 'api/wallet/getBanks';

            $http.get(url).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var data = response.data;
                deferred.resolve(data);
            }
        }

        function getWalletCurrencies() {
            var deferred = $q.defer();
            var url = walletBaseUrl + 'api/wallet/getCurrencies';

            $http.get(url).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var data = response.data;
                deferred.resolve(data);
            }
        }

        function getWalletName(walletPhoneNumber) {
            var deferred = $q.defer();
            var url = walletBaseUrl + 'api/wallet/getWalletName';

            $http.get(url, { params: { walletPhoneNumber: walletPhoneNumber } }).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var data = response.data;
                deferred.resolve(data);
            }
        }

        function saveWallet(wallet) {
            var url = walletBaseUrl + 'api/wallet/save',
                deferred = $q.defer();
            $http.post(url, wallet).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function saveOrganizationWallet(wallet) {
            if (authData) {
                wallet.organizationId = authData.organizationId;
                var url = baseUrl + 'api/organizations/saveWallet',
                    deferred = $q.defer();
                $http.post(url, wallet).then(done, fail(deferred));
                return deferred.promise;

                function done(response) {
                    var dto = response && response.data;
                    if (!dto) {
                        var data = response && response.message;
                        var message = data && data.error && data.message || 'Server Error';
                        deferred.reject(new Error(message));
                    }
                    deferred.resolve(dto);
                }
            }
        }

        function getOrganizationWallets() {
            if (authData) {
                var deferred = $q.defer();
                var url = baseUrl + 'api/organizations/getOrganizationWallet';

                $http.get(url, { params: { organizationId: authData.organizationId } }).then(done, fail(deferred));
                return deferred.promise;

                function done(response) {
                    var data = response.data;
                    deferred.resolve(data);
                }
            }
        }


        function saveWalletAccount(walletAccount) {
            var url = walletBaseUrl + 'api/wallet/saveAccount',
                deferred = $q.defer();
            $http.post(url, walletAccount).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function getWalletAccounts(walletPhoneNumber) {
            var deferred = $q.defer();
            var url = walletBaseUrl + 'api/wallet/getAccounts';

            $http.get(url, { params: { walletPhoneNumber: walletPhoneNumber } }).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var data = response.data;
                deferred.resolve(data);
            }
        }

        function saveWalletTransaction(walletTransaction) {
            var url = walletBaseUrl + 'api/wallet/saveTransaction',
                deferred = $q.defer();
            $http.post(url, walletTransaction).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function getWalletTransactions(walletPhoneNumber) {
            var deferred = $q.defer();
            var url = walletBaseUrl + 'api/wallet/getTransactions';

            $http.get(url, { params: { walletPhoneNumber: walletPhoneNumber } }).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var data = response.data;
                deferred.resolve(data);
            }
        }

        function getOrganizationLogo(organizationId) {
            var url = walletBaseUrl + 'api/organizations/getLogo/';

            $("#imgContainerProfile").empty();
            $("#imgContainerProfile").append("<img alt='Logo' id='imgLogo' src='" + url + organizationId + "?" + new Date().getTime() + "' class='bg-warning border-radius' width='53' /> <br />");
        }

        function fail(deferred) {
            return function (rejection) {
                var data = rejection.data;
                var message = data.message;
                deferred.reject(message);
            }
        }
    }

})();