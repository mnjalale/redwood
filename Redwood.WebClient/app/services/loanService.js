﻿(function () {
    'use strict';

    angular.module('app').factory('loanService', ['$http', '$q', 'appSettings','localStorageService', loanService]);

    function loanService($http, $q, appSettings,localStorageService) {
        var baseUrl = appSettings.apiServiceBaseUri;
        var authData = localStorageService.get('authorizationData');
        var organizationId = authData.organizationId;
        var service = {
            save: save,
            get: get,
            getById: getById,
            getByStatus: getByStatus,
            savePayment:savePayment
        };

        return service;

        function get() {
            var url = baseUrl + 'api/loans/get',
               deferred = $q.defer();

            $http.get(url, { params: { organizationId: organizationId } })
                .then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function getByStatus(status) {
            var url = baseUrl + 'api/loans/getByStatus',
               deferred = $q.defer();

            $http.get(url, { params: { organizationId: organizationId, status: status } })
                .then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function getById(loanId) {
            var url = baseUrl + 'api/loans/getById',
               deferred = $q.defer();

            $http.get(url, { params: { loanId: loanId, organizationId: organizationId } })
                .then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function save(loan) {
            var url = baseUrl + 'api/loans/save',
                deferred = $q.defer();
            $http.post(url, loan).then(done,fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function savePayment(payment) {
            var url = baseUrl + 'api/loans/savePayment',
                deferred = $q.defer();
            $http.post(url, payment).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function fail(deferred) {
            return function (rejection) {
                var data = rejection.data;
                var message = data.message;
                deferred.reject(message);
            }
        }

    }

})();