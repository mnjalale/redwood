﻿(function () {
    'use strict';

    angular.module('app').factory('organizationService', ['$http', '$q', 'appSettings','localStorageService', organizationService]);

    function organizationService($http, $q, appSettings,localStorageService) {
        var baseUrl = appSettings.apiServiceBaseUri;
        var authData = localStorageService.get('authorizationData');
        var organizationId = +authData.organizationId;
        var service = {
            save: save,
            getById: getById,
            getProfileLogo: getProfileLogo,
            getUpsertLogo: getUpsertLogo,
            saveLogo: saveLogo,
            deleteLogo: deleteLogo,
            saveBrand: saveBrand
        };

        return service;
        
        function getById() {
            var url = baseUrl + 'api/organizations/getById',
               deferred = $q.defer();

            $http.get(url, { params: { id:organizationId } })
                .then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }
        
        function save(organization) {
            var url = baseUrl + 'api/organizations/save',
                deferred = $q.defer();
            $http.post(url, organization).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }
        
        function saveBrand(organization) {
            var url = baseUrl + 'api/organizations/saveBrand',
                deferred = $q.defer();
            $http.post(url, organization).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function getProfileLogo() {
            var url = baseUrl + 'api/organizations/getLogo/';

            $("#imgContainerProfile").empty();
            $("#imgContainerProfile").append("<img alt='Logo' id='imgLogo' src='" + url + organizationId + "?" + new Date().getTime() + "' class='full-width bg-grey-light border-lighter border-radius' /> <br />");
            
        }

        function getUpsertLogo() {
            var url = baseUrl + 'api/organizations/getLogo/';

            $("#imgContainerUpsert").empty();
            $("#imgContainerUpsert").append("<img alt='Logo' id='imgLogo' src='" + url + organizationId + "?" + new Date().getTime() + "' class='full-width bg-grey-light border-lighter border-radius' /> <br />");
        }

        function saveLogo(data) {
            var url = baseUrl + 'api/organizations/savelogo/' + organizationId;
            var deferred = $q.defer();
            $http.post(url, data, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            }).then(success).catch(deferred.reject);
            return deferred.promise;


            function success(response) {
                var quote = response && response.data;
                deferred.resolve(quote);
            }
        }

        function deleteLogo() {
            var url = baseUrl + 'api/organizations/deleteLogo/' + organizationId ;
            var deferred = $q.defer();
            $http.delete(url).then(done,fail(deferred));
            return deferred.promise;

            function done(response) {
                var succeeded = response && response.data;
                deferred.resolve(succeeded);
            }
        }

        function fail(deferred) {
            return function (rejection) {
                var data = rejection.data;
                var message = data.message;
                deferred.reject(message);
            }
        }
    }

})();