﻿(function() {
    'use strict';

    angular.module('app').factory('onboardingService', ['$http', '$q', 'localStorageService', 'appSettings', onboardingService]);

    function onboardingService($http, $q, localStorageService, appSettings) {
        var baseUrl = appSettings.apiServiceBaseUri;

        var onboardingServiceFactory = {
            registerUserAndOrganization: registerUserAndOrganization,
            requestInvite: requestInvite,
            updateUserSettings: updateUserSettings,
            saveLoginSettings:saveLoginSettings,
            getUser: getUser
        };

        return onboardingServiceFactory;

        function registerUserAndOrganization(model) {
            var url = baseUrl + 'api/onboarding/createUserAndOrganization',
                deferred = $q.defer();
            $http.post(url, model).then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var dto = response && response.data && response.data.result;
                if (!dto) {
                    var data = response && response.data;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function requestInvite(model) {
            var url = baseUrl + 'api/onboarding/sendInvitation',
                deferred = $q.defer();
            $http.post(url, model).then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function getUser(username) {
           // var username = 'mnjalale';
            var url = baseUrl + 'api/onboarding/user/',
               deferred = $q.defer();

            //  $http.get(url, { params: { username: username } })
            $http.get(url +  username  )
                .then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        //function getUser(username) {
        //    // var username = 'mnjalale';
        //    var url = baseUrl + 'api/onboarding/user',
        //       deferred = $q.defer();

        //    //  $http.get(url, { params: { username: username } })
        //    $http.get(url, { params: { username: username } })
        //       .then(done).catch(deferred.reject);
        //    return deferred.promise;

        //    function done(response) {
        //        var dto = response && response.data;
        //        if (!dto) {
        //            var data = response && response.message;
        //            var message = data && data.error && data.message || 'Server Error';
        //            deferred.reject(new Error(message));
        //        }
        //        deferred.resolve(dto);
        //    }
        //}

       
        function updateUserSettings(user) {
            var url = baseUrl + 'api/onboarding/updateUserSettings',
                deferred = $q.defer();
            $http.post(url, user).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function saveLoginSettings(loginModel) {
            var url = baseUrl + 'api/accounts/ChangePassword',
                deferred = $q.defer();
            $http.post(url, loginModel).then(done, fail(deferred));

            return deferred.promise;

            function done(response) {               
                var dto = response && response.data;

                if (!response.statusText == "OK") {
                 if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                }             
               
                deferred.resolve(dto);
            }
        }

        function fail(deferred) {
            return function (rejection) {
                var data = rejection.data;
                var message = data.message;
                deferred.reject(message);
            }
        }

       
    }

})();