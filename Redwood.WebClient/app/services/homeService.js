﻿(function () {
    'use strict';

    angular.module('app').factory('homeService', ['$http', '$q', 'localStorageService', 'appSettings', homeService]);

    function homeService($http, $q, localStorageService, appSettings) {
        var baseUrl = appSettings.apiServiceBaseUri;
        var authData = localStorageService.get('authorizationData');
        var organizationId = authData.organizationId;
        var service = {
            loadDashboard: loadDashboard
        };

        return service;
        

        function loadDashboard() {
            var url = baseUrl + 'api/home/getDashboard',
               deferred = $q.defer();

            $http.get(url, { params: { organizationId: organizationId } })
                .then(done,fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function fail(deferred) {
            return function (rejection) {
                var data = rejection.data;
                var message = data.message;
                deferred.reject(message);
            }
        }
    }

})();