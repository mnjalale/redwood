﻿(function () {
    'use strict';

    angular.module('app').factory('authInterceptorService', ['$q', '$location','localStorageService', authInterceptorService]);

    function authInterceptorService($q, $location,localStorageService) {
        var authInterceptorServiceFactory = {
            request: request,
            responseError: responseError
        };

        return authInterceptorServiceFactory;

        function request(config) {
            config.headers = config.headers || {};

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config || $q.when(config);
        }

        function responseError(rejection) {
            if (rejection.status === 401) {
               // $state.go('public.login');
                $location.path('/login');
            }

            return $q.reject(rejection);
        }


    }
})();