﻿(function () {
    'use strict';

    angular.module('app').factory('applicationService', ['$http', '$q','localStorageService', 'appSettings', applicationService]);

    function applicationService($http, $q,localStorageService, appSettings) {
        var baseUrl = appSettings.apiServiceBaseUri;
        var authData = localStorageService.get('authorizationData');
        var organizationId = authData.organizationId;
        var service = {
            save: save,
            get: get,
            getById: getById,
            getByStatus: getByStatus,
            reject:reject
        };

        return service;

        function get() {
            var url = baseUrl + 'api/applications/get',
               deferred = $q.defer();

            $http.get(url, { params: { organizationId: organizationId } })
                .then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function getByStatus(status) {
            var url = baseUrl + 'api/applications/getByStatus',
               deferred = $q.defer();

            $http.get(url, { params: { organizationId: organizationId,status:status } })
                .then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function getById(applicationId) {
            var url = baseUrl + 'api/applications/getById',
               deferred = $q.defer();

            $http.get(url, { params: { applicationId: applicationId, organizationId: organizationId } })
                .then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function save(application) {
            var url = baseUrl + 'api/applications/save',
                deferred = $q.defer();
            $http.post(url, application).then(done,fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function reject(rejection) {
            var url = baseUrl + 'api/applications/reject',
                deferred = $q.defer();
            $http.post(url, rejection).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function fail(deferred) {
            return function (rejection) {
                var data = rejection.data;
                var message = data.message;
                deferred.reject(message);
            }
        }
    }

})();