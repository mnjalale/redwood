﻿(function () {
    'user strict';

    angular.module('app').factory('authService', [
        '$http', '$location', '$q', '$state', 'localStorageService', 'appSettings', 'retryQueue', 'redirectToUrlAfterLogin', authService]);

    function authService($http, $location, $q, $state, localStorageService, appSettings, queue, redirectToUrlAfterLogin) {
        var serviceBase = appSettings.apiServiceBaseUri;
        var authentication = {
            isAuth: false,
            userName: "",
            brandColorBg: "",
            brandColorText:"",
            organizationId:0
        };
        var service = {
            register: register,
            login: login,
            logout: logout,
            getUsers: getUsers,
            fillAuthData: fillAuthData,
            isAuthenticated: isAuthenticated,
            loggedInUser: loggedInUser,
            requireAuthenticatedUser: requireAuthenticatedUser,
            redirectToAttemptedUrl: redirectToAttemptedUrl,
            authentication:authentication
        };

        queue.onItemAddedCallbacks.push(function(retryItem) {
            if (queue.hasMore()) {
                saveAttemptUrl();
                gotoLogin();
            }
        });

        return service;

        function getUsers() {
            var deferred = $q.defer();

            $http.get(serviceBase + 'api/accounts/users').then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var data = response.data;
                deferred.resolve(data);
            }
        }

        function loggedInUser() {
            console.log(authentication);
            return authentication.userName;
        }

        function register(registration) {
            logout();

            return $http.post(serviceBase + 'api/accounts/create', registration).then(function (response) {
                return response;
            });
        }

        function saveAttemptUrl() {
            if ($location.path().toLowerCase() != '/login') {
                redirectToUrlAfterLogin.url = $location.path();
            }
        }

        function redirectToAttemptedUrl() {
            redirect(redirectToUrlAfterLogin.url);
        }

        function redirect(url) {
            url = url || '/';
            $location.path(url);
        }

        function gotoLogin() {
            $state.go('public.login', null, { reload: true });
        }

        function requireAuthenticatedUser() {
            if (isAuthenticated()) {
                return $q.when();
            } else {
                queue.pushRetryFn('unauthenticated-client', service.requireAuthenticatedUser);
            }
            return $q.reject();;
        }

        
        function isAuthenticated() {
            var authData = localStorageService.get('authorizationData');
            return !!authData;
        }
        
        function login(loginData) {
            var data = "grant_type=password&username=" + loginData.username + "&password=" + loginData.password;
            var url = serviceBase + "oauth/token";
            var deferred = $q.defer();

            $http.post(url, data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
                localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.username, refreshToken: "",organizationId:response.organizationId, useRefreshTokens: false });
                authentication.isAuth = true;
                authentication.userName = loginData.username;
                authentication.useRefreshTokens = false;
                authentication.organizationId = response.organizationId;
                authentication.brandColorBg= response.brandColorBg;                
                authentication.brandColorText = response.brandColorText;
                console.log(authentication);

                deferred.resolve(response);
            }).error(function (err, status) {
                logout();
                deferred.reject(err);
            }).catch(function (err) {
                logout();
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function logout() {
            localStorageService.remove('authorizationData');
           
            authentication.isAuth = false;
            authentication.userName = "";
            authentication.useRefreshTokens = false;
            authentication.organizationId = 0;

            localStorageService.remove('walletAuthorizationData');
        }

        function fillAuthData() {
            var authData = localStorageService.get('authorizationData');
            if (authData) {
                authentication.isAuth = true;
                authentication.userName = authData.userName;
                authentication.organizationId = authData.organizationId;
            }
        }

    };
})();