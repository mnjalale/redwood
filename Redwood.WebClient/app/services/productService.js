﻿(function () {
    'use strict';

    angular.module('app').factory('productService', ['$http', '$q','appSettings','localStorageService', productService]);

    function productService($http, $q, appSettings,localStorageService) {
        var baseUrl = appSettings.apiServiceBaseUri;
        var authData = localStorageService.get('authorizationData');
        var organizationId = authData.organizationId;
        var service = {
            save: save,
            get: get,
            getById: getById
        };

        return service;

        function get() {
            var url = baseUrl + 'api/products/get',
               deferred = $q.defer();

            $http.get(url, { params: { organizationId: organizationId } })
                .then(done).catch(deferred.reject);
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function getById(productId) {
           var url = baseUrl + 'api/products/getById',
               deferred = $q.defer();

            $http.get(url, { params: { productId: productId, organizationId: organizationId } })
                .then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function save(product) {
            var url = baseUrl + 'api/products/save',
                deferred = $q.defer();
            $http.post(url, product).then(done, fail(deferred));
            return deferred.promise;

            function done(response) {
                var dto = response && response.data;
                if (!dto) {
                    var data = response && response.message;
                    var message = data && data.error && data.message || 'Server Error';
                    deferred.reject(new Error(message));
                }
                deferred.resolve(dto);
            }
        }

        function fail(deferred) {
            return function(rejection) {
                var data = rejection.data;
                var message = data.message;
                deferred.reject(message);
            }
        }
    }

})();