﻿(function () {
    'user strict';

    angular.module('app').factory('walletAuthService', [
        '$http', '$location', '$q', '$state', 'localStorageService', 'appSettings', 'walletRetryQueue', 'redirectToUrlAfterLogin', authService]);

    function authService($http, $location, $q, $state, localStorageService, appSettings, queue, redirectToUrlAfterLogin) {
        var serviceBase = appSettings.walletApiServiceBaseUri;
        var authentication = {
            isAuth: false,
            userName: ""
        };
        var service = {
            register: register,
            login: login,
            logout: logout,
            fillAuthData: fillAuthData,
            isAuthenticated: isAuthenticated,
            requireAuthenticatedUser: requireAuthenticatedUser,
            redirectToAttemptedUrl: redirectToAttemptedUrl,
            checkAuthentication: checkAuthentication
        };

        queue.onItemAddedCallbacks.push(function (retryItem) {
            if (queue.hasMore()) {
                saveAttemptUrl();
                gotoLogin();
            }
        });

        return service;

        function register(registration) {
            logout();

            return $http.post(serviceBase + 'api/accounts/create', registration).then(function (response) {
                return response;
            });
        }

        function saveAttemptUrl() {
            if ($location.path().toLowerCase() != '/wallet/enter') {
                redirectToUrlAfterLogin.url = $location.path();
            }
        }

        function redirectToAttemptedUrl() {
            redirect(redirectToUrlAfterLogin.url);
        }

        function redirect(url) {
            url = url || '/';
            $location.path(url);
        }

        function checkAuthentication() {
            var authData = localStorageService.get('walletAuthorizationData');
            if (!authData) {
                $location.path('/wallet/enter');
                //$state.go('wallet.workspace.loginWallet');
            }
        }

        function gotoLogin() {
            $state.go('wallet.workspace.loginWallet', null, { reload: true });
        }

        function requireAuthenticatedUser() {
            if (isAuthenticated()) {
                return $q.when();
            } else {
                $location.path('/wallet/enter/');
                // $state.go('wallet.workspace.loginWallet');
                //queue.pushRetryFn('unauthenticated-client', service.requireAuthenticatedUser);
            }
            return $q.reject();
        }


        function isAuthenticated() {
            var authData = localStorageService.get('walletAuthorizationData');
            return !!authData;
        }

        function login(loginData) {
            var data = "grant_type=password&username=" + loginData.username + "&password=" + loginData.password;
            var url = serviceBase + "oauth/token";
            var deferred = $q.defer();

            $http.post(url, data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
                localStorageService.set('walletAuthorizationData', { token: response.access_token, userName: loginData.username, refreshToken: "", useRefreshTokens: false });

                authentication.isAuth = true;
                authentication.userName = loginData.username;
                authentication.useRefreshTokens = false;

                deferred.resolve(response);
            }).error(function (err, status) {
                logout();
                deferred.reject(err);
            }).catch(function (err) {
                logout();
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function logout() {
            localStorageService.remove('walletAuthorizationData');

            authentication.isAuth = false;
            authentication.userName = "";
            authentication.useRefreshTokens = false;
        }

        function fillAuthData() {
            var authData = localStorageService.get('walletAuthorizationData');
            if (authData) {
                authentication.isAuth = true;
                authentication.userName = authData.userName;
            }
        }

    };
})();