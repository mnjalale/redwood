﻿(function () {
    //var app = angular.module('app', ['ui.router', 'LocalStorageModule', 'angular-loading-bar', 'datatables',
    //    'ui.bootstrap', 'ngMessages', 'ui.mask', 'isteven-multi-select', 'blockUI', 'angularMoment']);
    var app = angular.module('app', ['ui.router', 'LocalStorageModule', 'angular-loading-bar','datatables',
        'ui.bootstrap', 'ngMessages', 'ui.mask', 'isteven-multi-select', 'blockUI', 'ngToast', 'fcsa-number',
        'ui.bootstrap.datetimepicker', 'angularMoment', 'colorpicker.module']);

    app.config(function ($stateProvider, $urlRouterProvider) {
        //$locationProvider.hashPrefix('!').html5Mode(true);
        $stateProvider
            //Public
            .state('public', {
                abstract: true,
                url: "/",
                templateUrl: "/index.html"
            })
            .state('public.landing', {
                url: "landing",
                templateUrl: "/app/landing/partials/landing.html",
                controller: "landingController",
                controllerAs: "vm"
            })
            .state('public.login', {
                url: "login",
                templateUrl: "/app/users/partials/login.html",
                controller: "loginController",
                controllerAs: "vm"
            })
            .state('public.signUp', {
                url: "signUp",
                templateUrl: "/app/users/partials/signUp.html",
                controller: "signUpController",
                controllerAs: "vm"
            })
            .state('public.requestInvite', {
                url: "requestInvite",
                templateUrl: "/app/users/partials/requestInvite.html",
                controller: "requestInviteController",
                controllerAs: "vm"
            })
            .state('public.requestInviteThankYou', {
                url: "requestInviteSuccess",
                templateUrl: "/app/users/partials/requestInviteThankYou.html",
                controller: "requestInviteThankYouController",
                controllerAs: "vm"
            })
            .state('public.applications', {
                abstract: true,
                url: "productApplication/",
                templateUrl: "app/applicationsExternal/partials/applicationsExternalMaster.html",
                controller: "applicationsExternalMasterController",
                controllerAs: "vm"
            })
            .state('public.applications.apply', {
                url: "apply",
                templateUrl: "/app/applicationsExternal/partials/upsertApplication.html",
                controller: "upsertApplicationController",
                controllerAs: "vm"
            })
            .state('public.applications.success', {
                url: "success",
                templateUrl: "/app/applicationsExternal/partials/applicationSuccess.html",
                controller: "applicationSuccessController",
                controllerAs: "vm"
            })
            .state('private', {
                abstract: true,
                url: '/',
                templateUrl: "/app/master/partials/master.html",
                controller: "masterController",
                controllerAs: "vm",
                resolve: {
                    auth: [
                        'authService', function(authService) {
                            return authService.requireAuthenticatedUser();
                        }
                    ]
                }
            })
            .state('private.home', {
                url: "home",
                templateUrl: "app/home/partials/home.html",
                controller: "homeController",
                controllerAs: "vm",
                resolve: {
                    dashboard: [
                        'blockUI', 'homeService', function(blockUI, homeService) {

                            return homeService.loadDashboard()
                                .catch(function(ex) {
                                    console.log(ex.message || 'Error');
                                }).finally(function() {
                                });
                        }
                    ]
                }
            })
            .state('private.products', {
                abstract: true,
                url: "products/",
                templateUrl: "app/products/partials/products.html",
                controller: "productsController",
                controllerAs: "vm"
            })
            .state('private.products.add', {
                url: "add",
                templateUrl: "app/products/partials/upsertProduct.html",
                controller: "upsertProductController",
                controllerAs: "vm"
            })
            .state('private.products.edit', {
                url: "edit/:productId",
                templateUrl: "app/products/partials/upsertProduct.html",
                controller: "upsertProductController",
                controllerAs: "vm"
            })
            .state('private.products.details', {
                url: "details/:productId",
                templateUrl: "app/products/partials/productDetails.html",
                controller: "productDetailsController",
                controllerAs: "vm"
            })
            .state('private.applications', {
                abstract: true,
                url: "applications/",
                templateUrl: "app/applications/partials/applications.html",
                controller: "applicationsController",
                controllerAs: "master"
            })
            .state('private.applications.view', {
                url: "view/:status",
                templateUrl: "app/applications/partials/viewApplications.html",
                controller: "viewApplicationsController",
                controllerAs: "vm"
            })
            .state('private.applicationDetails', {
                abstract: true,
                url: "applicationDetails/:applicationId/",
                templateUrl: "app/applications/partials/applicationDetailsMaster.html",
                controller: "applicationDetailsMasterController",
                controllerAs: "master"
            })
            .state('private.applicationDetails.application', {
                url: "application",
                templateUrl: "app/applications/partials/applicationDetails.html",
                controller: "applicationDetailsController",
                controllerAs: "vm"
            })
            .state('private.applicationDetails.appraisalReport', {
                url: "appraisalReport",
                templateUrl: "app/applications/partials/applicationAppraisalReport.html",
                controller: "applicationAppraisalReportController",
                controllerAs: "vm"
            })
            .state('private.applicationDetails.documents', {
                url: "documents",
                templateUrl: "app/applications/partials/applicationDocuments.html",
                controller: "applicationDocumentsController",
                controllerAs: "vm"
            })
            .state('private.applicationDetails.conversation', {
                url: "conversation",
                templateUrl: "app/applications/partials/applicationConversation.html",
                controller: "applicationConversationsController",
                controllerAs: "vm"
            })
            .state('private.loans', {
                abstract: true,
                url: "loans/",
                templateUrl: "app/loans/partials/loans.html",
                controller: "loansController",
                controllerAs: "master"
            })
            .state('private.loans.view', {
                url: "view/:status",
                templateUrl: "app/loans/partials/viewLoans.html",
                controller: "viewLoansController",
                controllerAs: "vm"
            })
            .state('private.loanDetails', {
                abstract: true,
                url: "loanDetails/:loanId/",
                templateUrl: "app/loans/partials/loanDetailsMaster.html",
                controller: "loanDetailsMasterController",
                controllerAs: "master"
            })
            .state('private.loanDetails.collections', {
                url: "loan",
                templateUrl: "app/loans/partials/loanDetails.html",
                controller: "loanDetailsController",
                controllerAs: "vm"
            })
            .state('private.loanDetails.application', {
                url: "application",
                templateUrl: "app/applications/partials/applicationDetails.html",
                controller: "applicationDetailsController",
                controllerAs: "vm"
            })
            .state('private.loanDetails.conversation', {
                url: "conversation",
                templateUrl: "app/loans/partials/loanConversation.html",
                controller: "loanConversationsController",
                controllerAs: "vm"
            })
            .state('private.portfolio', {
                url: "portfolio",
                templateUrl: "app/portfolio/partials/portfolio.html",
                controller: "portfolioController",
                controllerAs: "vm"
            })
            .state('private.wallet', {
                abstract: true,
                url: "wallet/",
                templateUrl: "app/wallet/partials/wallet.html",
                controller: "walletController",
                controllerAs: "vm"
            })
            .state('private.wallet.register', {
                url: "register",
                templateUrl: "app/wallet/partials/registerWallet.html",
                controller: "registerWalletController",
                controllerAs: "vm"
            })
            .state('private.wallet.login', {
                url: "login",
                templateUrl: "app/wallet/partials/loginWallet.html",
                controller: "loginWalletController",
                controllerAs: "vm"
            })
            .state('private.wallet.main', {
                abstract: true,
                url: "wallet/",
                templateUrl: "app/wallet/partials/walletMaster.html",
                controller: "walletMasterController",
                controllerAs: "master"
            })
            .state('private.wallet.main.transactions', {
                url: "transactions",
                templateUrl: "app/wallet/partials/walletTransactions.html",
                controller: "walletTransactionsController",
                controllerAs: "vm"
    });

        $urlRouterProvider.when('', gotoIndex);
        $urlRouterProvider.when('/', gotoIndex);

        gotoIndex.$inject = ['$state', 'authService'];

        function gotoIndex($state, authService) {
                if (authService.isAuthenticated()) {
                    $state.go('private.home');
                } else {
                    $state.go('public.landing');
                }
            }
        $urlRouterProvider.otherwise("/");


        
    });

    app.constant('appSettings', {
        //apiServiceBaseUri: 'https://redwoodapi.azurewebsites.net/',
        //walletApiServiceBaseUri: 'https://ctwalletapi.azurewebsites.net/',
        apiServiceBaseUri: 'http://localhost:12160/',
        walletApiServiceBaseUri: 'http://localhost:10629/'
    });

    app.value('redirectToUrlAfterLogin', { url: '/' });
    app.value('organizationProducts', []);

    app.config(['$httpProvider', 'blockUIConfig','ngToastProvider', function ($httpProvider, blockUIConfig,ngToastProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
        blockUIConfig.message = 'Please wait...';
        blockUIConfig.autoInjectBodyBlock = false;
        blockUIConfig.delay = 100;
        ngToastProvider.configure({
            animation: 'slide' //slide or fade, "success", "info", "warning" or "danger" shortcut methods:
        });
    }]);

    /* Setup global theme settings */
    app.factory('themeSettings', ['$rootScope', function ($rootScope) {
        // supported languages
        var themeSettings = {
            brandPrimary:{
                color: '#232233',
                textColor: '#fff'
            },
        };

        $rootScope.themeSettings = themeSettings;

        return themeSettings;
    }]);

    app.run([
        "$rootScope", "$timeout", "$window", "authService", "blockUI", "themeSettings", function ($rootScope, $timeout, $window, authService, blockUI, themeSettings) {
            authService.fillAuthData();
            
            init();

            function init() {
                blockView();
                scrollTop();
            }

            function blockView() {
                $rootScope.$on('$stateChangeStart', blockUI.start);
                $rootScope.$on('$stateChangeSuccess', blockUI.stop);
                $rootScope.$on('$stateChangeError', blockUI.stop);
            }

            function scrollTop() {
                $rootScope.$on('$stateChangeSuccess', function () {
                    $timeout(function () {
                        $window.scrollTo(0, 0);
                    });
                });
            }
        }
    ]);

})();