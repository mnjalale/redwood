﻿(function () {
    'use strict';
    angular.module('app').controller('homeController', ['$scope', '$state', '$modal', 'blockUI', 'dashboard', 'localStorageService', 'walletService', 'authService', homeController]);

    function homeController($scope,$state,$modal,blockUI,dashboard,localStorageService,walletService,authService) {
        var vm = this;
        vm.dashboard = dashboard;
        vm.applicationLabels = vm.dashboard.applicationGraphLabels;
        vm.applicationData = vm.dashboard.applicationGraphData;
        vm.loanPortfolioLabels = vm.dashboard.loanPortfolioGraphLabels;
        vm.loanPortfolioData = vm.dashboard.loanPortfolioGraphData;
        vm.wallet = {};
        vm.depositToWallet = depositToWallet;
        vm.walletExists = false;
        var walletPhoneNumber = '';
        var reloaded = false;
        
        
        activate();

        function activate() {           
            var authData = localStorageService.get('walletAuthorizationData');
          
            if (authData) {
                walletPhoneNumber = authData.userName;
                getWallet();
                vm.walletExists = true;
            } else {
                vm.wallet.balance = 0;
            }
        }

        $(document).ready(function () {

            // Animations with Velocity.js
            $(".modal").on('show.bs.modal', function () {
                $(this).velocity('transition.fadeIn', 800);
            });

            $(".modal.in").on('hide.bs.modal', function (e) {
                $(this).velocity('transition.fadeOut', 800);
            });


            // Charting stuff
            var graphApplications = {
                data: {
                    labels: vm.applicationLabels,
                    datasets: [
                        {
                            label: "",
                            fillColor: "rgba(0, 126, 178, 0.4)",
                            strokeColor: "transparent",
                            highlightFill: "rgba(0, 126, 178, 0.62)",
                            highlightStroke: "transparent",
                            data: vm.applicationData
                        }
                    ]
                },
                options: {
                    scaleShowGridLines: false,
                    showScale: true,
                    animationEasing: "easeOutSine",
                    responsive: true,
                    maintainAspectRatio: false,
                    pointDotRadius: 0,
                    bezierCurve: false,
                }
            };

            var graphPorfolio = {
                data: {
                    labels: vm.loanPortfolioLabels,
                    datasets: [
                        {
                            label: "Total Portfolio",
                            fillColor: "rgba(0, 134, 178, .6)",
                            strokeColor: "rgba(151,187,205,0)",
                            pointColor: "rgba(0, 134, 178, 0)",
                            pointStrokeColor: "rgba(0, 134, 178, 0)",
                            pointHighlightFill: "rgba(0, 134, 178, 1)",
                            pointHighlightStroke: "#fff",
                            data: vm.loanPortfolioData
                        },
                        {
                            label: "Portfolio at Risk",
                            fillColor: "#d9534f",
                            strokeColor: "rgba(151,187,205,0)",
                            pointColor: "rgba(255, 23, 0, 0)",
                            pointStrokeColor: "rgba(255, 23, 0, 0)",
                            pointHighlightFill: "#d9534f",
                            pointHighlightStroke: "#fff",
                            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                        }
                    ]
                },
                options: {
                    scaleShowGridLines: false,
                    pointDotRadius: 3,
                    bezierCurve: false,
                    showScale: true,
                    animationEasing: "easeOutSine",
                    responsive: true,
                    maintainAspectRatio: false,
                    barValueSpacing: 10,
                    barDatasetSpacing: 2,
                    legendTemplate: "" +
                        "<ul class=\"<%=name.toLowerCase()%>-legend list\-inline \">" +
                        "<% for (var i=0; i<datasets.length; i++){%>" +
                        "<li>" +
                        "<span style=\"background-color:<%=datasets[i].fillColor%>\" class=\"label text-white \"><%if(datasets[i].label){%><%=datasets[i].label%><%}%></span>" +
                        "</li><%}%>" +
                        "</ul>"
                }
            };
            
            Chart.defaults.global.responsive = true;

            var portfolio = document.getElementById("chart-cashflowgraph").getContext("2d");
            var ctxPortfolio = new Chart(portfolio).Line(graphPorfolio.data, graphPorfolio.options);
            var portfolioLegend = ctxPortfolio.generateLegend();
            $('#portfolioLegend').append(portfolioLegend);

            var applicationsTrend = document.getElementById("chart-applications").getContext("2d");
            var ctxApplicationsTrend = new Chart(applicationsTrend).Bar(graphApplications.data, graphApplications.options);

        });
        
        function depositToWallet() {
            $modal.open({
                templateUrl: '/app/wallet/partials/depositToWallet.html',
                controller: 'depositToWalletController',
                resolve: {
                    wallet: function () {
                        return vm.wallet;
                    }
                }
            }).result.then(function (result) {
                if (result) {
                    getWallet();
                }
            });
        }

        function getWallet() {
            return walletService.getWalletByWalletPhoneNo(walletPhoneNumber).then(function (wallet) {
                vm.wallet = wallet;
            }).catch(function (error) {
                console.log(error);
            });
        }
    }
})();