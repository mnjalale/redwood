﻿(function () {
    'use strict';
    angular.module('app').controller('applicationDetailsMasterController', ['$scope', '$state', '$stateParams','$modal', 'blockUI', 'ngToast', 'applicationService', applicationDetailsMasterController]);

    function applicationDetailsMasterController($scope, $state, $stateParams,$modal, blockUI, ngToast, applicationService) {
        var applicationDetailsBlockUi = blockUI.instances.get('applicationDetailsBlockUI');
        var master = this;
        master.applicationId = $stateParams.applicationId;
        master.approveApplication = approveApplication;
        master.unapproveApplication = unapproveApplication;
        master.disburseApplication = disburseApplication;
        master.denyApplication = denyApplication;
        master.isProcessing = false;
        
        activate();

        function activate() {
            getApplication();
        }

        function denyApplication() {
            $modal.open({
                templateUrl: "/app/applications/partials/rejectApplication.html",
                controller: "rejectApplicationController",
                controllerAs: "vm",
                resolve: {
                    application: function() {
                        return master.application;
                    }
                }
            }).result.then(function (application) {
                master.application = application;
            }).catch(function () {
                console.log("Something went wrong.");
            });
        }

        function approveApplication() {
            master.application.status = "Approved";

            applicationDetailsBlockUi.start("Approving...");
            master.isProcessing = true;

            return applicationService.save(master.application).then(function (applicationModel) {
                master.application = applicationModel;
            }).catch(function (error) {
                ngToast.danger(error);
                master.application.status = "Pending";
            }).finally(function () {
                applicationDetailsBlockUi.stop();
                master.isProcessing = false;
            });
        }

        function unapproveApplication() {
            master.application.status = "Pending";

            applicationDetailsBlockUi.start("Unapproving...");
            master.isProcessing = true;

            return applicationService.save(master.application).then(function (applicationModel) {
                master.application = applicationModel;
            }).catch(function (error) {
                ngToast.danger(error);
                master.application.status = "Approved";
            }).finally(function () {
                applicationDetailsBlockUi.stop();
                master.isProcessing = false;
            });
        }

        function disburseApplication() {
            $modal.open({
                templateUrl: "/app/applications/partials/disburseApplication.html",
                controller: "disburseApplicationController",
                controllerAs: "vm",
                resolve: {
                    application: function() {
                        return master.application;
                    }
                }
            }).result.then(function (result) {

            }).catch(function (error) {
                //ngToast.danger(error);
            });
        }

        function getApplication() {
            if (!master.applicationId) {
                return;
            }
            applicationDetailsBlockUi.start('Loading...');
            return applicationService.getById(master.applicationId).then(function (application) {
                master.application = application;
                master.application.applicationDate = new Date(application.applicationDate);
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                applicationDetailsBlockUi.stop();
            });
        }
    }
})();