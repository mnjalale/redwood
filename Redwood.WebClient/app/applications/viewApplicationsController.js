﻿(function () {
    'use strict';
    angular.module('app').controller('viewApplicationsController', ['$scope', '$rootScope', '$q', '$state', '$stateParams', 'applicationService', 'blockUI', 'DTOptionsBuilder', 'DTColumnDefBuilder', viewApplicationsController]);

    function viewApplicationsController($scope,$rootScope,$q,$state,$stateParams,applicationService,blockUI, DTOptionsBuilder, DTColumnDefBuilder) {
        var applicationsBlockUi = blockUI.instances.get('applicationsBlockUI');
        var vm = this;
        vm.status = $stateParams.status;
        vm.applications = [];

        vm.dtOptions = DTOptionsBuilder.newOptions().withOption('order', []);
        vm.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4)
        ];
        
        activate();

        function activate() {
            loadApplications();
            $rootScope.applicationTitle = $stateParams.status + ' Applications';
        }
        
        function loadApplications() {
            applicationsBlockUi.start("Loading...");
            return applicationService.getByStatus(vm.status).then(function (applications) {
                applications.sort(function (a, b) {

                    if (a.applicationDate < b.applicationDate) {
                        return 1;
                    }

                    if (a.applicationDate > b.applicationDate) {
                        return -1;
                    }

                    return 0;
                });


                vm.applications = applications;
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function() {
                applicationsBlockUi.stop();
            });
        }
    }
})();