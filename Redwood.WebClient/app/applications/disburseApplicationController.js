﻿(function () {
    'use strict';
    angular.module('app').controller('disburseApplicationController', ['$scope', '$state','$location', '$stateParams', '$modalInstance', 'blockUI', 'ngToast','applicationService', 'application','walletService', disburseApplicationController]);

    function disburseApplicationController($scope, $state,$location, $stateParams, $modalInstance, blockUI, ngToast,applicationService, application,walletService) {
        var applicationDetailsBlockUi = blockUI.instances.get('disbursalDetailsBlockUI');
        var vm = this;
        vm.save = save;
        vm.cancel = cancel;
        vm.application = application;
        vm.isProcessing = false;

        function save() {
            vm.application.status = "Disbursed";

            applicationDetailsBlockUi.start("Disbursing...");
            vm.isProcessing = true;

            return applicationService.save(vm.application).then(function (loan) {
                vm.cancel();
                $location.url('loanDetails/' + loan.id + '/loan');


            }).catch(function (error) {
                ngToast.danger(error);
                vm.application.status = "Approved";
            }).finally(function () {
                applicationDetailsBlockUi.stop();
                vm.isProcessing = false;
            });
        }

        function cancel() {
            $modalInstance.dismiss('cancel');
        };


    }
})();