﻿(function () {
    'use strict';
    angular.module('app').controller('rejectApplicationController', ['$scope', '$state', '$stateParams','$modalInstance', 'blockUI', 'ngToast','application', 'applicationService', rejectApplicationController]);

    function rejectApplicationController($scope, $state, $stateParams,$modalInstance, blockUI, ngToast,application, applicationService) {
        var rejectionDetailsBlockUi = blockUI.instances.get('rejectionDetailsBlockUI');
        var vm = this;
        vm.save = save;
        vm.cancel = cancel;
        vm.application = application;
        vm.applicationDenial = {
            applicationId: application.id
        };
        vm.isProcessing = false;
        vm.rejected = false;
        
        function save() {
            
            rejectionDetailsBlockUi.start("Rejecting...");
            vm.isProcessing = true;

            return applicationService.reject(vm.applicationDenial).then(function (app) {
                vm.application = app;
                vm.rejected = true;
                ngToast.success("Saved successfully.");
                vm.cancel();
            }).catch(function (error) {
                ngToast.danger(error);
            }).finally(function () {
                rejectionDetailsBlockUi.stop();
                vm.isProcessing = false;
            });
        }
        
        function cancel() {
            if (vm.rejected) {
                $modalInstance.close(vm.application);
            } else {
                $modalInstance.dismiss('cancel');
            }

        };


    }
})();