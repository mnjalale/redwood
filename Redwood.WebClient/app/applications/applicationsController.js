﻿(function () {
    'use strict';
    angular.module('app').controller('applicationsController', ['$scope','$rootScope','blockUI','ngToast','applicationService', applicationsController]);

    function applicationsController($scope, $rootScope,blockUI,ngToast,applicationService) {
        //var applicationsBlockUi = blockUI.instances.get('applicationsBlockUI');
        var master = this;
        master.applications = [];
        master.applicationsCount = 0;
        master.pendingApplications = [];
        master.pendingApplicationsCount = 0;
        master.approvedApplications = [];
        master.approvedApplicationsCount = 0;
        master.disbursedApplications = [];
        master.rejectedApplications = [];
        master.rejectedApplicationsCount = 0;
        master.cancelledApplications = [];
        master.cancelledApplicationsCount = 0;

        activate();

        function activate() {
            //applicationsBlockUi.start("Loading...");

            loadApplications().then(function() {
                master.pendingApplications = _.filter(master.applications, function (application) {
                    return application.status === 'Pending';
                });
                master.pendingApplicationsCount = master.pendingApplications.length;

                master.approvedApplications = _.filter(master.applications, function (application) {
                    return application.status === 'Approved';
                });
                master.approvedApplicationsCount = master.approvedApplications.length;

                master.disbursedApplications = _.filter(master.applications, function (application) {
                    return application.status === 'Disbursed';
                });
                master.disbursedApplicationsCount = master.disbursedApplications.length;

                master.rejectedApplications = _.filter(master.applications, function (application) {
                    return application.status === 'Rejected';
                });
                master.rejectedApplicationsCount = master.rejectedApplications.length;
                master.cancelledApplications = _.filter(master.applications, function (application) {
                    return application.status === 'Cancelled By User';
                });
                master.cancelledApplicationsCount = master.cancelledApplications.length;
            }).catch(function(error) {
                ngToast.danger(error);
            }).finally(function() {
                //applicationsBlockUi.stop();
            });
        }

        function loadApplications() {
            //applicationsBlockUi.start("Loading...");
            return applicationService.get().then(function (applications) {
                master.applications = applications;
                master.applicationsCount = applications.length;
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                //applicationsBlockUi.stop();
            });
        }
    }
})();