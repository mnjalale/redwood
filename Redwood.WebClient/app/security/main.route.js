﻿(function() {
    'use strict';

    angular.module('app').run(appRun);

    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(), '/not-found');
    }

    function getStates() {
        return [
            // Public/anon routes
            {
                state: 'public',
                config: {
                    abstract: true,
                    template: "<ui-view/>"
                }
            }, {
                state: 'public.not-found',
                config: {
                    url: '/not-found',
                    templateUrl: '/app/layout/not-found.html'
                }
            }, {
                state: 'public.login',
                config: {
                    url: '/login',
                    templateUrl: '/app/security/login.html',
                    controller: 'Login',
                    controllerAs: 'vm'
                }
            },
            // Private routes
            {
                state: 'private',
                config: {
                    abstract: true,
                    templateUrl: '/app/layout/shell.html',
                    requireAuthenticatedUser: true
                }
            }, {
                state: 'private.home',
                config: {
                    url: '',
                }
            }, {
                state: 'private.users',
                config: {
                    url: '/users',
                    templateUrl: '/app/users/users.html',
                    controller: 'Users',
                    controllerAs: 'vm'
                }
            }, {
                state: 'private.companies',
                config: {
                    url: '/companies',
                    templateUrl: '/app/companies/companies.html',
                    controller: 'Companies',
                    controllerAs: 'vm',
                }
            }, {
                state: 'private.payments',
                config: {
                    url: '/payments',
                    templateUrl: '/app/payments/payments.html',
                    controller: 'Payments',
                    controllerAs: 'vm'
                }
            }, {
                state: 'private.subscriptions',
                config: {
                    url: '/subscriptions',
                    templateUrl: '/app/subscriptions/subscriptions.html',
                    controller: 'Subscriptions',
                    controllerAs: 'vm'
                }
            }, {
                state: 'private.notifications',
                config: {
                    url: '/notifications',
                    templateUrl: '/app/notifications/notifications.html'
                }
            }, {
                state: 'private.feedback',
                config: {
                    url: '/feedback',
                    templateUrl: '/app/feedback/feedback.html'
                }
            }
        ];
    }
})();
