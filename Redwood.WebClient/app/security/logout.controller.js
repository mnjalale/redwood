﻿(function() {
    'use strict';

    angular.module('app.security').controller('Logout', Logout);

    Logout.$inject = ['$rootScope', '$window', 'securityService', 'alertsService'];

    function Logout($rootScope, $window, securityService, alertsService) {
        var vm = this;
        vm.closeAlert = alertsService.closeAlert;
        vm.logout = logout;
       
        function logout() {
            return securityService.logout().then(function (response) {
                $window.location = "/login";
            }).catch(function (response) {
                alertsService.RenderErrorMessage(response.ReturnMessage);
            });
        }
    }
})();