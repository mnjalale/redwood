﻿(function() {
    'use strict';

    angular.module('app.security').factory('securityInterceptor', securityInterceptor);

    securityInterceptor.$inject = ['$injector', '$q', 'logger', 'retryQueue'];
    
    function securityInterceptor($injector, $q, logger, queue) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                var securityService = $injector.get('securityService');
                var user = securityService.currentUser;
                if (!!user) {
                    var authInfo = user.userName + ":" + user.password;
                    config.headers['Authorization'] = 'Basic ' + Base64.encode(authInfo);
                }
                return config || $q.when(config);
            },

            requestError: function(rejection) {
                return $q.reject(rejection);
            },

            response: function(response) {
                return response || $q.when(response);
            },

            responseError: function(rejection) {
                if (rejection.status === 401) {
                    // The request bounced because it was not authorized - add a new request to the retry queue
                    queue.pushRetryFn('unauthenticated-server', function () {
                        logger.error('unauthenticated-server');
                        return $injector.get('$http')(rejection.config);
                    });
                }
                if (rejection.status === 403) {
                    // The request bounced because it was not authorized - add a new request to the retry queue
                    queue.pushRetryFn('unauthorized-server', function () {
                        logger.error('unauthorized-server')
                        return $injector.get('$http')(rejection.config);
                    });
                }
                return $q.reject(rejection);
            }
        };
    }

    // We have to add the interceptor to the queue as a string because the interceptor depends upon service instances that are not available in the config block.
    angular.module('app.security').config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('securityInterceptor');
    }]);
})();
