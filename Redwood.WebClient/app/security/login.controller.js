﻿(function() {
    'use strict';

    angular.module('app.security').controller('Login', Login);

    Login.$inject = ['$state', 'securityService', 'alertsService'];

    function Login($state, securityService, alertsService) {
        var vm = this;
        var baseUrl = 'http://localhost:10938/';
        vm.closeAlert = alertsService.closeAlert;
        vm.login = login;

        function login(isValid) {
            vm.isloggedIn = false;
            var credentials = {
                userName: vm.userName,
                password: vm.password
            };
            securityService.login(credentials).then(function (response) {
                securityService.redirectToAttemptedUrl();
            }).catch(function(response) {
                var error = response.data;
                alertsService.renderErrorMessage(error && error.returnMessage);
                alertsService.setValidationErrors(vm, error && error.validationErrors);
            });

            if (!isValid) {
                alert("Please fill out all the form fields");
            }
        }
    }
})();