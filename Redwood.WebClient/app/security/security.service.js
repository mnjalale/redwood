﻿(function () {
    'use strict';

    angular.module('app.security').factory('securityService', securityService);

    securityService.$inject = ['$http', '$location', '$q', '$state', 'config', 'redirectToUrlAfterLogin', 'retryQueue', 'util'];

    function securityService($http, $location, $q, $state, config, redirectToUrlAfterLogin, queue, util) {
        var baseUrl = config.baseUrl;
        var service = {
            login: login,
            logout: logout,
            requestCurrentUser: requestCurrentUser,
            currentUser: null,
            isAuthenticated: isAuthenticated,
            isAdmin: isAdmin,
            requireAdminUser: requireAdminUser,
            requireAuthenticatedUser: requireAuthenticatedUser,
            gotoLogin: gotoLogin,
            saveAttemptUrl: saveAttemptUrl,
            redirectToAttemptedUrl: redirectToAttemptedUrl,
            isAuthorized: isAuthorized
        };

        // Register a handler for when an item is added to the retry queue
        queue.onItemAddedCallbacks.push(function(retryItem) {
            if (queue.hasMore()) {
                saveAttemptUrl();
                gotoLogin();
            }
        });

        return service;

        function login(credentials) {
            var request = $http.post(baseUrl + 'user-account/login', credentials);
            return request.then(function(response) {
                service.currentUser = response.data;
                if (service.isAuthenticated()) {
                    queue.clear();
                }
                return service.isAuthenticated();
            });
        }

        function logout(redirectTo) {
            return $http.post(baseUrl + 'user-account/logout').then(function() {
                service.currentUser = null;
                redirect('login');
            });
        }

        function requestCurrentUser() {
            if (service.isAuthenticated()) {
                return $q.when(service.currentUser);
            } else {
                return $http.get(baseUrl + 'user-account/current-user').then(function(response) {
                    service.currentUser = response.data;
                    return service.currentUser;
                });
            }
        }

        function isAuthenticated() {
            return !!service.currentUser;
        }

        function isAdmin() {
            return !!(service.currentUser && service.currentUser.admin);
        }

        function requireAdminUser() {
            var promise = requestCurrentUser().then(function(userInfo) {
                if (!isAdmin()) {
                    return queue.pushRetryFn('unauthorized-client', service.requireAdminUser);
                }
            });
            return promise;
        }

        function requireAuthenticatedUser() {
            var promise = requestCurrentUser().then(function(userInfo) {
                if (!isAuthenticated()) {
                    return queue.pushRetryFn('unauthenticated-client', service.requireAuthenticatedUser);
                }
            });
            return promise;
        }

        function saveAttemptUrl() {
            if ($location.path().toLowerCase() != '/login') {
                redirectToUrlAfterLogin.url = $location.path();
            }
            //else
            //    redirectToUrlAfterLogin.url = '/';
        }

        function redirectToAttemptedUrl() {
            redirect(redirectToUrlAfterLogin.url);
        }

        function gotoLogin() {
            $location.path('/login');
        }

        function redirect(url) {
            url = url || '/';
            $location.path(url);
        }

        function isAuthorized(accessLevel) {
            if (util.anyMatchInArray(accessLevel, ["public", "anon"])) {
                return true;
            }
            return isAuthenticated();
        }
    }
})();