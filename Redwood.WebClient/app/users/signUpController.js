﻿(function () {
    'use strict';
    angular.module('app').controller('signUpController', ['$scope','$q','ngToast','blockUI', 'onboardingService', signUpController]);

    function signUpController($scope, $q, ngToast, blockUI, onboardingService) {
        var signUpBlockUi = blockUI.instances.get('signUpFormBlockUI');
        var vm = this;
        vm.model = {};

        vm.signUp = signUp;

        function signUp(isValid) {
            if (!isValid) {
                return;
            }

            signUpBlockUi.start("Signing Up...");

            return onboardingService.registerUserAndOrganization(vm.model).then(function() {
                console.log("Registered");
            }).catch(function (error) {
                ngToast.danger('An error occurred.');
                console.log(error);
            }).finally(function() {
                signUpBlockUi.stop();
            });
        }

    }
})();