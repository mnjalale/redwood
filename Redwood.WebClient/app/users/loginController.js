﻿(function () {
    'use strict';
    angular.module('app').controller('loginController', ['$scope','$rootScope','$state','ngToast','blockUI','authService','walletAuthService','walletService', loginController]);

    function loginController($scope,$rootScope, $state, ngToast, blockUI, authService,walletAuthService,walletService) {
        var loginBlockUi = blockUI.instances.get('loginFormBlockUI');
        var vm = this;
        vm.model = {};
        vm.login = login;

        function login(isValid) {
            if (!isValid) {
                return;
            }

            //loginBlockUi.start("Logging in...");
            //authService.login(vm.model).then(function (response) {
            //    $state.go('private.home');
            //},
            //function (err) {
            //    ngToast.danger(err.error_description);
            //    vm.message = err.error_description;
            //}).finally(function() {
            //    loginBlockUi.stop();
            //});

            loginBlockUi.start("Logging in...");
            authService.login(vm.model).then(function (response) {               
                //Get the saved wallet details and log the guy in
                //Commented because of inexplicible bug 12 Nov 2016 MNjalale
                //walletService.getOrganizationWallets().then(function(wallets) {
                //    if (wallets.length > 0) {
                //        var username = wallets[0].username;
                //        var password = wallets[0].password;

                //        walletAuthService.login({username:username,password:password}).catch(function (error) {
                //            console.log('Wallet login error:' + error);
                //        });
                //    }
                //}).catch(function(error) {
                //    console.log(error);
                //}).finally(function() {
                //    authService.redirectToAttemptedUrl();
                //});

               
                if (authService.authentication.brandColorBg != null )
                {
                  
                    $rootScope.themeSettings.brandPrimary.color = authService.authentication.brandColorBg;
                }
                if (authService.authentication.brandColorText != null) {
                   
                    $rootScope.themeSettings.brandPrimary.textColor = authService.authentication.brandColorText;
                }

                authService.redirectToAttemptedUrl();
            }).catch(function(err) {
                ngToast.danger('Invalid username or password!');
                vm.message = err.error_description;
            }).finally(function () {
                loginBlockUi.stop();
            });
        };
    }
})();