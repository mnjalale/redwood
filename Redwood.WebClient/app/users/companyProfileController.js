﻿(function () {
    'use strict';
    angular.module('app').controller('companyProfileController', ['$scope','$rootScope', '$state', '$q','$modal','$modalInstance', 'ngToast', 'blockUI', 'organizationService', companyProfileController]);

    function companyProfileController($scope,$rootScope, $state, $q,$modal,$modalInstance, ngToast, blockUI, organizationService) {
        var organizationBlockUi = blockUI.instances.get('organizationBlockUI');
        var vm = this;
        vm.organization = {};
        vm.cancel = cancel;
        vm.editProfile = editProfile;

        activate();

        function activate() {
            getOrganization().then(function() {
                loadCompanyLogo();
            });
        }
        
        function cancel() {
            $modalInstance.dismiss('cancel');
        }

        function editProfile() {
            $modal.open({
                templateUrl: "/app/users/partials/upsertCompanyProfile.html",
                controller: "upsertCompanyProfileController",
                controllerAs: "vm"
            }).result.then(function (model) {
                vm.organization = model;
                loadCompanyLogo();
            }).catch(function () {
                console.log("Something went wrong.");
            });
        }

        function getOrganization() {
            organizationBlockUi.start('Loading...');
            return organizationService.getById().then(function (organization) {
                vm.organization = organization;
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                organizationBlockUi.stop();
            });
        }

        function loadCompanyLogo() {
            return organizationService.getProfileLogo();
        }
    }
})();