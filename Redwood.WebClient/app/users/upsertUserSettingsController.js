﻿(function () {
    'use strict';
    angular.module('app').controller('upsertUserSettingsController', ['$scope', '$state', '$q', '$modalInstance', 'ngToast', 'blockUI', 'onboardingService', 'authService', upsertUserSettingsController]);

    function upsertUserSettingsController($scope, $state, $q, $modalInstance, ngToast, blockUI, onboardingService, authService) {
        var accountSettingsBlockUi = blockUI.instances.get('accountSettingsBlockUi');
        var loginSettingsBlockUi = blockUI.instances.get('loginSettingsBlockUi');

        var vm = this;
        vm.model = {};
        vm.cancel = cancel;
        vm.saveAccountSettings = saveAccountSettings;
        vm.saveLoginSettings = saveLoginSettings;
        vm.isSaving = false;
        vm.saveAccountSettingsClicked = false;

        activate();

        function activate() {
            getUserSettings().then(function () {
            });
        }

        function cancel() {
            $modalInstance.dismiss('cancel');
        };

        function saveAccountSettings(accountSettingsValid) {
            if (!accountSettingsValid) {
                return;
            }
            vm.saveAccountSettingsClicked = false;
            return saveSettings(accountSettingsBlockUi);
        }

        function saveLoginSettings(loginSettingsValid) {

            if (!loginSettingsValid) {
                return;
            }

            return resetPassword(loginSettingsBlockUi);
        }

        function saveSettings(blockUi) {         
            blockUi.start('Saving...');
            vm.isSaving = true;
            return onboardingService.updateUserSettings(vm.model).then(function () {              
                ngToast.success('Saved Successfully!');
                vm.model.password = "";
            }).catch(function (error) {
                ngToast.danger(error);
            }).finally(function () {
                blockUi.stop();
                vm.isSaving = false;
            });
        }

        function resetPassword(blockUi) {

            blockUi.start('Saving...');
            vm.isSaving = true;
            return onboardingService.saveLoginSettings({
                oldPassword: vm.model.oldPassword,
                newPassword: vm.model.password, confirmPassword: vm.model.confirmPassword
            }).then(function () {
                ngToast.success('Saved Successfully!');

                vm.model.oldPassword = "";
                vm.model.password = "";
                vm.model.confirmPassword = "";
            }).catch(function (error) {
                ngToast.danger(error);
            }).finally(function () {
                blockUi.stop();
                vm.isSaving = false;
            });
        }

        function getUserSettings() {
            accountSettingsBlockUi.start('Loading...');
            loginSettingsBlockUi.start('Loading...');
            return onboardingService.getUser(authService.authentication.userName).then(function (user) {
                vm.model = user;
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                accountSettingsBlockUi.stop();
                loginSettingsBlockUi.stop();
            });
        }

    }
})();