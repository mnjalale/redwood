﻿(function () {
    'use strict';
    angular.module('app').controller('requestInviteController', ['$scope','$state', '$q', 'ngToast', 'blockUI', 'onboardingService', requestInviteController]);

    function requestInviteController($scope,$state, $q, ngToast, blockUI, onboardingService) {
        var vm = this;
        var invitationBlockUi = blockUI.instances.get('invitationFormBlockUI');
        vm.model = {};

        vm.requestInvitation = requestInvitation;

        function requestInvitation(isValid) {
            if (!isValid) {
                return;
            }


            invitationBlockUi.start("Sending Request...");

            return onboardingService.requestInvite(vm.model).then(function () {
                $state.go('public.requestInviteThankYou');
                console.log("request sent");
            }).catch(function (error) {
                ngToast.danger('An error occurred.');
                console.log(error);
            }).finally(function () {
                invitationBlockUi.stop();
            });
        }
    }
})();