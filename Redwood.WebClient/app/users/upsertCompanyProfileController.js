﻿(function () {
    'use strict';
    angular.module('app').controller('upsertCompanyProfileController', ['$scope','$rootScope', '$state', '$q', '$modalInstance', 'ngToast', 'blockUI', 'organizationService','authService' ,'$window', upsertCompanyProfileController]);

    function upsertCompanyProfileController($scope,$rootScope, $state, $q, $modalInstance, ngToast, blockUI, organizationService,authService, $window) {
        var companyDetailsBlockUi = blockUI.instances.get('companyDetailsBlockUi');
        var contactDetailsBlockUi = blockUI.instances.get('contactDetailsBlockUi');
        var legalDetailsBlockUi = blockUI.instances.get('legalDetailsBlockUi');
        var logoBlockUi = blockUI.instances.get('logoBlockUi');
        var brandColorsBlockUi = blockUI.instances.get('brandColorsBlockUi');

        var vm = this;
        vm.organization = {};
        vm.cancel = cancel;
        vm.saveCompanyDetails = saveCompanyDetails;
        vm.saveContactDetails = saveContactDetails;
        vm.saveLegalDetails = saveLegalDetails;
        vm.isSaving = false;
        vm.organizationHasChanged = false;
        vm.saveBrandDetails = saveBrandDetails;
        vm.saveBrandClicked = false;

        activate();

        function activate() {          
            getOrganization().then(function() {
                loadCompanyLogo();
            });
        }

        function cancel() {
            if (vm.organizationHasChanged) {
                $modalInstance.close(vm.organization);
            } else {
                $modalInstance.dismiss('cancel');
            }
            
        };

       
        function saveCompanyDetails(companyDetailsValid, contactDetailsValid, legalDetailsValid) {
            if (!companyDetailsValid || !contactDetailsValid || !legalDetailsValid) {
                return;
            }

            return saveOrganization(companyDetailsBlockUi);
        }

        function saveContactDetails(companyDetailsValid, contactDetailsValid, legalDetailsValid) {
            if (!companyDetailsValid || !contactDetailsValid || !legalDetailsValid) {
                return;
            }

            return saveOrganization(contactDetailsBlockUi);
        }

        function saveLegalDetails(companyDetailsValid, contactDetailsValid, legalDetailsValid) {
            if (!companyDetailsValid || !contactDetailsValid || !legalDetailsValid) {
                return;
            }

            return saveOrganization(legalDetailsBlockUi);
        }

        function saveBrandDetails(brandDetailsForm) {
            if (!brandDetailsForm) {
                return;
            }          
            return saveOrganization(brandColorsBlockUi);
        }

        function saveOrganization(blockUi) {
            blockUi.start('Saving...');         
            vm.isSaving = true;
            return organizationService.save(vm.organization).then(function (organizationModel) {
                ngToast.success('Saved Successfully!');
                vm.organizationHasChanged = true;
                setTheme(organizationModel);
            }).catch(function (error) {
                ngToast.danger(error);
            }).finally(function () {
                blockUi.stop();
                vm.isSaving = false;
            });
        }

        function getOrganization() {
            companyDetailsBlockUi.start('Loading...');
            contactDetailsBlockUi.start('Loading...');
            legalDetailsBlockUi.start('Loading...');
            return organizationService.getById().then(function (organization) {
                vm.organization = organization;
                setTheme(organization);
              }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                companyDetailsBlockUi.stop();
                contactDetailsBlockUi.stop();
                legalDetailsBlockUi.stop();
            });
        }

        function setTheme(organization){

            if (organization.brandColorBg != null )
            {                  
                $rootScope.themeSettings.brandPrimary.color = organization.brandColorBg;
            }
            if (organization.brandColorText != null) {
                   
                $rootScope.themeSettings.brandPrimary.textColor = organization.brandColorText;
            }
        }

        //Logo section

        $(document).on('click', '#btnUpload', function () {
            var files = $("#file1").get(0).files;
            if (files.length > 0) {
                var data = new FormData();
                for (var i = 0; i < files.length; i++) {
                    data.append("file" + i, files[i]);
                }

                logoBlockUi.start('Uploading...');
                organizationService.saveLogo(data).
                    then(function(response) {
                        loadCompanyLogo();
                        vm.organizationHasChanged = true;
                    }).catch(function(error) {
                        ngToast.danger('Errors occurred while loading logo.');
                    }).finally(function() {
                        logoBlockUi.stop();
                    });

            }
        });

        $(document).on('click', '#btnShow', function () {
            loadCompanyLogo();
        });

        $(document).on('click', '#btnDelete', function () {
            deleteCompanyLogo();
        });

        function loadCompanyLogo() {
            return organizationService.getUpsertLogo();
        }

        function deleteCompanyLogo() {
            logoBlockUi.start('Deleting...');
            return organizationService.deleteLogo().
                then(function(response) {
                    loadCompanyLogo();
                    vm.organizationHasChanged = true;
                }).catch(function(error) {
                    ngToast.danger('Errors occurred while deleting logo');
                }).finally(function() {
                    logoBlockUi.stop();
                });
        }

        //End Logo section
    }
})();