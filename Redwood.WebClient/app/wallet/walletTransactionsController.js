﻿(function () {
    'use strict';

    angular.module('app').controller('walletTransactionsController',
        ['$scope', '$modal', '$window', '$state', 'ngToast', '$location', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'localStorageService', 'blockUI', 'walletService', controller]);

    function controller($scope, $modal, $window, $state,ngToast, $location,DTOptionsBuilder, DTColumnDefBuilder,localStorageService, blockUI, walletService) {
        var vm = this;
      
        vm.transactions = [];
        vm.wallet = {};
        var walletPhoneNumber = '';

        vm.dtOptions = DTOptionsBuilder.newOptions().withOption('order', []);
        vm.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0),
            DTColumnDefBuilder.newColumnDef(1),
            DTColumnDefBuilder.newColumnDef(2),
            DTColumnDefBuilder.newColumnDef(3),
            DTColumnDefBuilder.newColumnDef(4)
        ];


        activate();

        function activate() {
            var authData = localStorageService.get('walletAuthorizationData');
            if (authData) {
                walletPhoneNumber = authData.userName;
                getTransactions();
            }
        }

        function getTransactions() {
            return walletService.getWalletTransactions(walletPhoneNumber).then(function (transactions) {
                transactions.sort(function (a, b) {
                    
                    if (a.transactionDate < b.transactionDate) {
                        return 1;
                    }

                    if (a.transactionDate > b.transactionDate) {
                        return -1;
                    }

                    return 0;
                });
                vm.transactions = transactions;
            }).catch(function (error) {
                ngToast.danger(error);
            });
        }

        $scope.dtColumnDefs = [
           DTColumnDefBuilder.newColumnDef(0).notSortable()
        ];
    }
})();