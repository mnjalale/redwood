﻿(function () {
    'use strict';
    angular.module('app').controller('walletMasterController', ['$scope','$state','$modal','ngToast','blockUI','localStorageService','walletService', walletMasterController]);

    function walletMasterController($scope,$state,$modal,ngToast,blockUI,localStorageService,walletService) {
        var master = this;
        var walletPhoneNumber = '';
        master.showAccounts = showAccounts;
        master.depositToWallet = depositToWallet;
        master.withdrawFromWallet = withdrawFromWallet;
        master.transferToOtherWallet = transferToOtherWallet;
        var sideBarBlockUi = blockUI.instances.get('sideBarBlockUI');
        activate();

        function activate() {
            var authData = localStorageService.get('walletAuthorizationData');
            if (!authData) {
                $state.go('private.wallet.login');
            } else {
                walletPhoneNumber = authData.userName;
                getWallet();
            }
        }

        function getWallet() {
            sideBarBlockUi.start('Loading...');
            return walletService.getWalletByWalletPhoneNo(walletPhoneNumber).then(function (wallet) {
                master.wallet = wallet;
            }).catch(function (error) {
                ngToast.danger(error);
            }).finally(function() {
                sideBarBlockUi.stop();
            });
        }

        function showAccounts() {
            $modal.open({
                templateUrl: '/app/wallet/partials/walletAccounts.html',
                controller: 'walletAccountsController'
            });
        }

        function depositToWallet() {
            $modal.open({
                templateUrl: '/app/wallet/partials/depositToWallet.html',
                controller: 'depositToWalletController',
                resolve: {
                    wallet: function () {
                        return master.wallet;
                    }
                }
            }).result.then(function (result) {
                if (result) {
                    reloadTransactions();
                    getWallet();
                }
            });
        }

        function withdrawFromWallet() {
            $modal.open({
                templateUrl: '/app/wallet/partials/withdrawFromWallet.html',
                controller: 'withdrawFromWalletController',
                resolve: {
                    wallet: function () {
                        return master.wallet;
                    }
                }
            }).result.then(function (result) {
                if (result) {
                    reloadTransactions();
                    getWallet();
                }
            });
        }

        function transferToOtherWallet() {
            $modal.open({
                templateUrl: '/app/wallet/partials/transferToOtherWallet.html',
                controller: 'transferToOtherWalletController',
                resolve: {
                    wallet: function () {
                        return master.wallet;
                    }
                }
            }).result.then(function (result) {
                if (result) {
                    reloadTransactions();
                    getWallet();
                }
            });
        }

        function reloadTransactions() {
            $state.go('private.wallet.main.transactions');
        }
    }
})();