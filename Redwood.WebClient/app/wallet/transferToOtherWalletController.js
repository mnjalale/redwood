﻿(function () {
    'use strict';

    angular.module('app').controller('transferToOtherWalletController',
        ['$scope', '$window', '$modal', '$modalInstance','localStorageService', 'ngToast', 'blockUI','wallet', 'walletService', controller]);

    function controller($scope, $window, $modal, $modalInstance,localStorageService, ngToast, blockUI,wallet, walletService) {
        $scope.cancel = cancel;
        $scope.save = save;
        $scope.upsertAccount = upsertAccount;
        $scope.accounts = [];
        $scope.transaction = {};
        $scope.isSaving = false;
        $scope.wallet = wallet;
        var transferBlockUI = blockUI.instances.get('transferBlockUI');
        var authData = localStorageService.get('walletAuthorizationData');
        var walletPhoneNumber = authData.userName;
        $scope.transaction.walletPhoneNumber = walletPhoneNumber;

        init();

        function init() {
            transferBlockUI.start('Loading...');
            createTransaction();
            getAccounts().then(function () {
                transferBlockUI.stop();
            });
        }

        function save(isValid) {
            if (!isValid) {
                return;
            }

            //Get the wallet's name 
            transferBlockUI.start('Please wait...');
            return walletService.getWalletName($scope.transaction.destination).then(function (walletName) {
                $scope.walletName = walletName;
                transferBlockUI.stop();
                confirm().result.then(function (result) {
                    if (result) {
                        $scope.isSaving = true;
                        transferBlockUI.start('Saving...');
                        walletService.saveWalletTransaction($scope.transaction).
                            then(function (response) {
                                $scope.transaction = response;
                                ngToast.success('Done', null, true);
                                $modalInstance.close($scope.transaction);
                                return true;
                            }).catch(function (error) {
                                ngToast.danger(error);
                            }).finally(function () {
                                transferBlockUI.stop();
                                $scope.isSaving = false;
                            });
                    }
                    return false;
                });
            }).catch(function (error) {
                ngToast.danger(error);
                transferBlockUI.stop();
            });

            //confirm().result.then(function (result) {
            //    if (result) {
            //        $scope.isSaving = true;
            //        transferBlockUI.start('Saving...');
            //        dataService.walletService.saveWalletTransaction($scope.transaction).
            //            then(function (response) {
            //                $scope.transaction = response;
            //                logger.success('Done', null, true);
            //                $modalInstance.close($scope.transaction);
            //                return true;
            //            }).catch(function (error) {
            //                logger.error(error, error, true);
            //            }).finally(function () {
            //                transferBlockUI.stop();
            //                $scope.isSaving = false;
            //            });
            //    }
            //    return false;
            //});
        }

        function confirm() {
            return $modal.open({
                templateUrl: '/app/wallet/partials/transferConfirm.html',
                controller: 'transferConfirmController',
                windowClass: 'small-Modal',
                resolve: {
                    transaction: function () {
                        return $scope.transaction;
                    },
                    recipientWallet: function () {
                        return $scope.walletName;
                    },
                    wallet: function () {
                        return $scope.wallet;
                    }

                }
            });
        }

        function getAccounts() {
            return walletService.getWalletAccounts(walletPhoneNumber).then(function (accounts) {
                $scope.accounts = accounts;
            }).catch(function (error) {
                ngToast.danger(error);
            });
        }

        function getWallet() {

        }

        function upsertAccount(account) {
            $modal.open({
                templateUrl: '/app/wallet/partials/upsertWalletAccount.html',
                controller: 'upsertWalletAccountController',
                resolve: {
                    account: function () {
                        return account || {};
                    }
                }
            }).result.then(function (result) {
                if (result) {
                    $scope.accounts = _.filter($scope.accounts, function (l) {
                        return l.id != result.id;
                    });
                    $scope.accounts.push(result);
                }
            });
        };

        function cancel() {
            $modalInstance.dismiss('cancel');
        }

        function createTransaction() {
            $scope.transaction.transactionType = 'Transfer';
            $scope.transaction.transactionDate = new Date();
        }
    }
})();