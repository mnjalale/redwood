﻿(function () {
    'use strict';

    angular.module('app').controller('upsertWalletAccountController',
        ['$scope', '$window', '$modalInstance', '$q','localStorageService', 'blockUI', 'ngToast', 'account', 'walletService', controller]);

    function controller($scope, $window, $modalInstance, $q,localStorageService, blockUI, ngToast, account, walletService) {
        $scope.model = account;
        $scope.save = save;
        $scope.cancel = cancel;
        $scope.delete = deleteAccount;
        $scope.isSaving = false;
        $scope.bankChanged = bankChanged;
        $scope.bankBranchChanged = bankBranchChanged;
        $scope.banks = [];
        $scope.branches = [];
        $scope.bank = {};
        $scope.branch = {};
        var walletAccountBlockUI = blockUI.instances.get('walletAccountBlockUI');
        var authData = localStorageService.get('walletAuthorizationData');
        $scope.model.walletPhoneNumber = authData.userName;

        init();

        function init() {
            loadBanks().then(function () {
                if ($scope.model.id) {
                    $scope.operation = 'Edit';
                    var filteredBanks = $scope.banks.filter(function (val) {
                        return val.code === $scope.model.bankCode;
                    });
                    $scope.bank = filteredBanks[0];
                    $scope.branches = $scope.bank.branches;

                    var filteredBranches = $scope.branches.filter(function (val) {
                        return val.code === $scope.model.bankBranchCode;
                    });
                    $scope.branch = filteredBranches[0];
                } else {
                    $scope.operation = 'Add';
                }
            });

        }

        function cancel() {
            $modalInstance.dismiss('cancel');
        }

        function save(isValid) {
            if (!isValid) {
                return;
            }

            $scope.isSaving = true;
            walletAccountBlockUI.start('Saving...');
            walletService.saveWalletAccount($scope.model).
                then(function (response) {
                    $scope.model = response;
                    ngToast.success('Done');
                    $modalInstance.close($scope.model);
                    return true;
                }).catch(function (error) {
                    ngToast.danger(error);
                }).finally(function () {
                    walletAccountBlockUI.stop();
                    $scope.isSaving = false;
                });
        }

        function deleteAccount() {

        }

        function loadBanks() {
            walletAccountBlockUI.start('Loading...');
            return walletService.getWalletBanks().then(function (banks) {
                banks.sort(function (a, b) {
                    var nameA = a.name.toUpperCase(); // ignore upper and lowercase
                    var nameB = b.name.toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }
                    return 0;
                });
                $scope.banks = banks;
            }).catch(function (error) {
                ngToast.danger(error);
            }).finally(function () {
                walletAccountBlockUI.stop();
            });
        }

        function bankChanged(bank) {
            $scope.bank = bank;
            $scope.model.bankName = bank.name;
            $scope.model.bankCode = bank.code;
            $scope.branches = bank.branches.sort(function (a, b) {
                var nameA = a.name.toUpperCase(); // ignore upper and lowercase
                var nameB = b.name.toUpperCase(); // ignore upper and lowercase
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            });
        }

        function bankBranchChanged(branch) {
            $scope.branch = branch;
            $scope.model.bankBranch = branch.name;
            $scope.model.bankBranchCode = branch.code;
        }
    }
})();