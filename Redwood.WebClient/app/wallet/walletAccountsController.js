﻿(function () {
    'use strict';

    angular.module('app').controller('walletAccountsController',
        ['$scope', '$modal', '$modalInstance', '$window', '$q','localStorageService', 'ngToast', 'blockUI', 'walletService', controller]);

    function controller($scope, $modal, $modalInstance, $window, $q,localStorageService, ngToast, blockUI, walletService) {
        $scope.accounts = [];
        $scope.cancel = cancel;
        $scope.upsertAccount = upsertAccount;
        var localStorage = $window.localStorage;
        var authData = localStorageService.get('walletAuthorizationData');
        var walletPhoneNumber = authData.userName;
        var walletAccountBlockUI = blockUI.instances.get('walletAccountsBlockUI');

        init();

        function init() {
            walletAccountBlockUI.start('Loading...');
            getAccounts().then(function () {
                walletAccountBlockUI.stop();
            });
        }

        function upsertAccount(account) {
            $modal.open({
                templateUrl: '/app/wallet/partials/upsertWalletAccount.html',
                controller: 'upsertWalletAccountController',
                resolve: {
                    account: function () {
                        return account || {};
                    }
                }
            }).result.then(function (result) {
                if (result) {
                    $scope.accounts = _.filter($scope.accounts, function (l) {
                        return l.id != result.id;
                    });
                    $scope.accounts.push(result);
                }
            });
        };

        function getAccounts() {
            return walletService.getWalletAccounts(walletPhoneNumber).then(function (accounts) {
                $scope.accounts = accounts;
            }).catch(function (error) {
                ngToast.danger(error);
            });
        }

        function cancel() {
            $modalInstance.dismiss('cancel');
        }
    }
})();