﻿(function () {
    'use strict';

    angular.module('app').controller('depositToWalletController',
       ['$scope', '$window', '$modal', '$modalInstance','localStorageService', 'ngToast', 'blockUI','wallet', 'walletService', controller]);

    function controller($scope, $window, $modal, $modalInstance,localStorageService, ngToast, blockUI,wallet, walletService) {
        $scope.cancel = cancel;
        $scope.save = save;
        $scope.upsertAccount = upsertAccount;
        $scope.accounts = [];
        $scope.transaction = {};
        $scope.isSaving = false;
        $scope.wallet = wallet;
        var depositBlockUI = blockUI.instances.get('depositBlockUI');
        var authData = localStorageService.get('walletAuthorizationData');
        var walletPhoneNumber = authData.userName;
        $scope.transaction.walletPhoneNumber = walletPhoneNumber;

        init();

        function init() {
            depositBlockUI.start('Loading...');
            createTransaction();
            getAccounts().then(function () {
                depositBlockUI.stop();
            });
        }

        function save(isValid) {
            if (!isValid) {
                return;
            }

            confirm().result.then(function (result) {
                if (result) {
                    $scope.isSaving = true;
                    depositBlockUI.start('Saving...');
                    walletService.saveWalletTransaction($scope.transaction).
                        then(function (response) {
                            $scope.transaction = response;
                            ngToast.success('Done');
                            $modalInstance.close($scope.transaction);
                            return true;
                        }).catch(function (error) {
                            ngToast.danger(error);
                        }).finally(function () {
                            depositBlockUI.stop();
                            $scope.isSaving = false;
                        });
                }
                return false;
            });
        }

        function confirm() {
            return $modal.open({
                templateUrl: '/app/wallet/partials/depositConfirm.html',
                controller: 'depositConfirmController',
                windowClass: 'small-Modal',
                resolve: {
                    transaction: function () {
                        return $scope.transaction;
                    },
                    wallet:function() {
                        return $scope.wallet;
                    }
                }
            });
        }

        function getAccounts() {
            return walletService.getWalletAccounts(walletPhoneNumber).then(function (accounts) {
                $scope.accounts = accounts;
            }).catch(function (error) {
                ngToast.danger(error);
            });
        }

        function upsertAccount(account) {
            $modal.open({
                templateUrl: '/app/wallet/partials/upsertWalletAccount.html',
                controller: 'upsertWalletAccountController',
                resolve: {
                    account: function () {
                        return account || {};
                    }
                }
            }).result.then(function (result) {
                if (result) {
                    $scope.accounts = _.filter($scope.accounts, function (l) {
                        return l.id != result.id;
                    });
                    $scope.accounts.push(result);
                }
            });
        };

        function cancel() {
            $modalInstance.dismiss('cancel');
        }

        function createTransaction() {
            $scope.transaction.transactionType = 'Deposit';
            $scope.transaction.transactionDate = new Date();
        }
    }
})();