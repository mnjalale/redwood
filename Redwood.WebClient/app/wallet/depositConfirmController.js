﻿(function () {
    'use strict';
    angular.module('app').controller('depositConfirmController',
    ['$scope', '$modalInstance','wallet', 'transaction', controller]);

    function controller($scope, $modalInstance,wallet, transaction) {
        $scope.transaction = transaction;
        $scope.wallet = wallet;

        $scope.ok = function () {
            $modalInstance.close(true);
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };
})();