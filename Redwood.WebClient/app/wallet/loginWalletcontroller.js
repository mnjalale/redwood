﻿(function () {
    'use strict';
    angular.module('app').controller('loginWalletController', ['$scope', '$state', '$window', 'ngToast', 'blockUI', 'walletAuthService','walletService', loginController]);

    function loginController($scope, $state, $window, ngToast, blockUI, authService,walletService) {
        var loginBlockUi = blockUI.instances.get('loginFormBlockUI');
        var vm = this;
        vm.model = {};
        vm.login = login;
        vm.isProcessing = false;

        function login(isValid) {
            if (!isValid) {
                return;
            }

            vm.isProcessing = true;
            loginBlockUi.start("Entering wallet...");
            authService.login(vm.model).then(function (response) {
                //if login is successful, update the organization wallet
                walletService.saveOrganizationWallet(vm.model).then(function(result) {
                    $state.go('private.wallet.main.transactions');
                }).catch(function(error) {
                    ngToast.danger(error);
                });
               
            }).catch(function (error) {
                ngToast.danger(error.error_description);
            }).finally(function () {
                loginBlockUi.stop();
                vm.isProcessing = false;
            });
        };
    }
})();