﻿(function () {
    'use strict';

    angular.module('app').controller('withdrawFromWalletController',
        ['$scope', '$window', '$modal', '$modalInstance','localStorageService', 'ngToast', 'blockUI','wallet', 'walletService', controller]);

    function controller($scope, $window, $modal, $modalInstance,localStorageService, ngToast, blockUI,wallet, walletService) {
        $scope.cancel = cancel;
        $scope.save = save;
        $scope.upsertAccount = upsertAccount;
        $scope.accounts = [];
        $scope.transaction = {};
        $scope.isSaving = false;
        $scope.wallet = wallet;

        var withdrawalBlockUI = blockUI.instances.get('withdrawalBlockUI');
        var authData = localStorageService.get('walletAuthorizationData');
        var walletPhoneNumber = authData.userName;
        $scope.transaction.walletPhoneNumber = walletPhoneNumber;

        init();

        function init() {
            withdrawalBlockUI.start('Loading...');
            createTransaction();
            getAccounts().then(function () {
                withdrawalBlockUI.stop();
            });
        }

        function save(isValid) {
            if (!isValid) {
                return;
            }

            confirm().result.then(function (result) {
                if (result) {
                    $scope.isSaving = true;
                    withdrawalBlockUI.start('Saving...');
                    walletService.saveWalletTransaction($scope.transaction).
                        then(function (response) {
                            $scope.transaction = response;
                            ngToast.success('Done');
                            $modalInstance.close($scope.transaction);
                            return true;
                        }).catch(function (error) {
                            ngToast.error(error);
                        }).finally(function () {
                            withdrawalBlockUI.stop();
                            $scope.isSaving = false;
                        });
                }
                return false;
            });
        }

        function confirm() {
            return $modal.open({
                templateUrl: '/app/wallet/partials/withdrawConfirm.html',
                controller: 'withdrawConfirmController',
                windowClass: 'small-Modal',
                resolve: {
                    transaction: function () {
                        return $scope.transaction;
                    },
                    wallet:function() {
                        return $scope.wallet;
                    }
                }
            });
        }

        function getAccounts() {
            return walletService.getWalletAccounts(walletPhoneNumber).then(function (accounts) {
                $scope.accounts = accounts;
            }).catch(function (error) {
                ngToast.error(error);
            });
        }

        function upsertAccount(account) {
            $modal.open({
                templateUrl: '/app/wallet/partials/upsertWalletAccount.html',
                controller: 'upsertWalletAccountController',
                resolve: {
                    account: function () {
                        return account || {};
                    }
                }
            }).result.then(function (result) {
                if (result) {
                    $scope.accounts = _.filter($scope.accounts, function (l) {
                        return l.id != result.id;
                    });
                    $scope.accounts.push(result);
                }
            });
        };

        function cancel() {
            $modalInstance.dismiss('cancel');
        }

        function createTransaction() {
            $scope.transaction.transactionType = 'Withdrawal';
            $scope.transaction.transactionDate = new Date();
        }
    }
})();