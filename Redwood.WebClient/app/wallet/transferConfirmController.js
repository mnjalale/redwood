﻿(function () {
    'use strict';
    angular.module('app').controller('transferConfirmController',
    ['$scope', '$modalInstance', 'recipientWallet','wallet', 'transaction', controller]);

    function controller($scope, $modalInstance, recipientWalletName,wallet, transaction) {
        $scope.transaction = transaction;
        $scope.wallet = wallet;
        $scope.recipientWalletName = recipientWalletName;

        $scope.ok = function () {
            $modalInstance.close(true);
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };
})();