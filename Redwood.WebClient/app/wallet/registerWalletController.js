﻿(function () {
    'use strict';

    angular.module('app').controller('registerWalletController',
        ['$scope', '$state', '$window', 'ngToast', 'blockUI', 'walletService', controller]);

    function controller($scope, $state, $window,ngToast, blockUI, walletService) {
        var vm = this;
        vm.wallet = {
            walletType: "Business"
        };
        var storage = $window.localStorage;
        var walletBlockUI = blockUI.instances.get('walletBlockUI');
        vm.save = save;
        vm.isSaving = false;
        vm.operation = "Add";
        vm.pinsMatch = pinsMatch;
        vm.currencies = [];
        vm.currency = {};
        vm.currencyChanged = currencyChanged;
        activate();

        function activate() {
            loadCurrencies();
        }

        function save(isValid) {
            if (!isValid || !pinsMatch()) {
                return;
            }

            vm.isSaving = true;
            if (vm.operation === "Add") {
                walletBlockUI.start('Registering...');
                vm.wallet.username = vm.wallet.phoneNumber;
            } else {
                walletBlockUI.start('Saving...');
            }
            return walletService.saveWallet(vm.wallet).
                then(function (response) {
                    vm.application = response;
                    ngToast.success('Done');
                    $state.go('private.wallet.login');
                }).catch(function (error) {
                    ngToast.danger(error);
                }).finally(function () {
                    walletBlockUI.stop();
                    vm.isSaving = false;
                });
        }

        function loadCurrencies() {
            walletBlockUI.start('Loading...');
            return walletService.getWalletCurrencies().then(function (currencies) {
               vm.currencies = currencies;
            }).catch(function (error) {
                ngToast.danger(error);
            }).finally(function () {
                walletBlockUI.stop();
            });
        }

        function currencyChanged(currency) {
            vm.wallet.currency = currency.name;
        }

        function pinsMatch() {
            if (vm.wallet.password === vm.wallet.confirmPassword) {
                return true;
            } else {
                ngToast.danger("The Confirmation PIN does not match the PIN.");
                return false;
            }
        }

    }
})();
