﻿(function () {
    'use strict';
    angular.module('app').controller('loanDetailsMasterController', ['$scope', '$state', '$stateParams', '$modal', 'blockUI', 'ngToast', 'loanService', loanDetailsMasterController]);

    function loanDetailsMasterController($scope, $state, $stateParams, $modal, blockUI, ngToast, loanService) {
        var loanDetailsBlockUi = blockUI.instances.get('loanDetailsBlockUI');
        var master = this;
        master.loanId = $stateParams.loanId;
        master.addPayment = addPayment;
        master.isProcessing = false;
        master.loan = {};
        master.application = {};

        activate();

        function activate() {
            getLoan();
        }

        function addPayment() {
            $modal.open({
                templateUrl: "/app/loans/partials/upsertPayment.html",
                controller: "upsertPaymentController",
                controllerAs: "vm",
                resolve: {
                    loan: function () {
                        return master.loan;
                    }
                }
            }).result.then(function (loan) {
                master.loan = loan;
            }).catch(function (error) {
                //ngToast.danger(error);
            });
        }

        function getLoan() {
            if (!master.loanId) {
                return;
            }
            loanDetailsBlockUi.start('Loading...');
            return loanService.getById(master.loanId).then(function (loan) {
                master.loan = loan;
                master.application = loan.application;
                master.loan.disbursalDate = new Date(loan.disbursalDate);
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                loanDetailsBlockUi.stop();
            });
        }
    }
})();