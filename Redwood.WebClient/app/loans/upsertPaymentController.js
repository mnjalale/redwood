﻿(function () {
    'use strict';
    angular.module('app').controller('upsertPaymentController', ['$scope', '$state', '$stateParams', '$modalInstance', 'blockUI', 'ngToast', 'loanService', 'loan', upsertPaymentController]);

    function upsertPaymentController($scope, $state, $stateParams, $modalInstance, blockUI, ngToast, loanService, loan) {
        var paymentDetailsBlockUi = blockUI.instances.get('paymentDetailsBlockUI');
        var vm = this;
        vm.save = save;
        vm.cancel = cancel;
        vm.loan = loan;
        vm.payment = {
            loanId: loan.id,
            date: new Date()
        };
        vm.isProcessing = false;
        vm.paymentMade = false;

        function save(isValid) {
            if (!isValid) {
                return;
            }
           
            paymentDetailsBlockUi.start("Saving...");
            vm.isProcessing = true;

            return loanService.savePayment(vm.payment).then(function (loanModel) {
                vm.loan = loanModel;
                vm.paymentMade = true;
                ngToast.success("Saved successfully.");
                vm.cancel();
            }).catch(function (error) {
                ngToast.danger(error);
            }).finally(function () {
                paymentDetailsBlockUi.stop();
                vm.isProcessing = false;
            });
        }

        
        function cancel() {
            if (vm.paymentMade) {
                $modalInstance.close(vm.loan);
            } else {
                $modalInstance.dismiss('cancel');
            }

        };

    }
})();