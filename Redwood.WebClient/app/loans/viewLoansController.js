﻿(function () {
    'use strict';
    angular.module('app').controller('viewLoansController', ['$scope', '$rootScope', '$q', '$state', '$stateParams', 'loanService', 'blockUI', viewLoansController]);

    function viewLoansController($scope, $rootScope, $q, $state, $stateParams, loanService, blockUI) {
        var loansBlockUi = blockUI.instances.get('loansBlockUI');
        var vm = this;
        vm.status = $stateParams.status;
        vm.loans = [];

        activate();

        function activate() {
            loadLoans();
            $rootScope.loanTitle = $stateParams.status + ' Loans';
        }

        function loadLoans() {
            loansBlockUi.start("Loading...");
            return loanService.getByStatus(vm.status).then(function (loans) {
                vm.loans = loans;
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                loansBlockUi.stop();
            });
        }
    }
})();