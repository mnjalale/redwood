﻿(function () {
    'use strict';
    angular.module('app').controller('loansController', ['$scope', '$rootScope', 'blockUI', 'ngToast', 'loanService', loansController]);

    function loansController($scope, $rootScope, blockUI, ngToast, loanService) {
        var master = this;
        master.loans = [];
        master.loansCount = 0;
        master.notStartedLoans = [];
        master.notStartedLoansCount = 0;
        master.activeLoans = [];
        master.activeLoansCount = 0;
        master.completedLoans = [];
        master.completedLoansCount = [];
        master.latePaymentLoans = [];
        master.latePaymentLoansCount = 0;
        master.defaultedLoans = [];
        master.defaultedLoansCount = 0;

        activate();

        function activate() {
            //loansBlockUi.start("Loading...");

            loadloans().then(function () {
                master.notStartedLoans = _.filter(master.loans, function (loan) {
                    return loan.loanStatus === 'Not Started';
                });
                master.notStartedLoansCount = master.notStartedLoans.length;

                master.activeLoans = _.filter(master.loans, function (loan) {
                    return loan.loanStatus === 'Active';
                });
                master.activeLoansCount = master.activeLoans.length;

                master.completedLoans = _.filter(master.loans, function (loan) {
                    return loan.loanStatus === 'Completed';
                });
                master.completedLoansCount = master.completedLoans.length;

                master.latePaymentLoans = _.filter(master.loans, function (loan) {
                    return loan.loanStatus === 'Late Payment';
                });
                master.latePaymentLoansCount = master.latePaymentLoans.length;

                master.defaultedLoans = _.filter(master.loans, function (loan) {
                    return loan.loanStatus === 'Defaulted';
                });
                master.defaultedLoansCount = master.defaultedLoans.length

            }).catch(function (error) {
                ngToast.danger(error);
            }).finally(function () {
                //loansBlockUi.stop();
            });
        }

        function loadloans() {
            //loansBlockUi.start("Loading...");
            return loanService.get().then(function (loans) {
                master.loans = loans;
                master.loansCount = loans.length;
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                //loansBlockUi.stop();
            });
        }

    }
})();