﻿(function () {
    'use strict';
    angular.module('app').controller('masterController', ['$scope','$state','$modal','authService','organizationService', masterController]);

    function masterController($scope,$state,$modal,authService,organizationService) {
        var vm = this;
        vm.organization = {};
        vm.logout = logout;
        vm.loadCompanyProfile = loadCompanyProfile;
        vm.loadUserSettings = loadUserSettings;

        activate();

        function activate() {
            loadOrganizationDetails().then(function () {
                loadCompanyLogo();
            });
        }

        function logout() {
            authService.logout();
            $state.go('public.login');
        }

        function loadCompanyProfile() {
            $modal.open({
                templateUrl: "/app/users/partials/companyProfile.html",
                controller: "companyProfileController",
                controllerAs: "vm"
            }).result.then(function(result) {

            }).catch(function() {
                console.log("Something went wrong.");
            });
        }

        function loadUserSettings() {
            $modal.open({
                templateUrl: "/app/users/partials/upsertUserSettings.html",
                controller: "upsertUserSettingsController",
                controllerAs: "vm"
            }).result.then(function (result) {

            }).catch(function () {
                console.log("Something went wrong.");
            });
        }

        function loadOrganizationDetails() {
            return organizationService.getById().then(function (organization) {
                vm.organization = organization;
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
            });
        }

        function loadCompanyLogo() {
            return organizationService.getProfileLogo();
        }

    }
})();