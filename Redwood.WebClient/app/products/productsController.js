﻿(function () {
    'use strict';
    angular.module('app').controller('productsController', ['$scope','$rootScope', 'ngToast', 'organizationProducts', 'productService', productsController]);

    function productsController($scope,$rootScope, ngToast, organizationProducts,productService) {
        var vm = this;
        vm.products = [];

        activate();

        function activate() {
            getProducts();
        }

        function getProducts() {
            return productService.get()
                .then(function (products) {
                    $rootScope.organizationProducts = products;
                    organizationProducts = products;
                    vm.products = products;
                })
            .catch(function(error) {
                ngToast.danger('An error occurred.');
            });
        }
        

    }
})();