﻿(function () {
    'use strict';
    angular.module('app').controller('productDetailsController', ['$scope', '$state','$stateParams', 'blockUI','ngToast', 'productService', productDetailsController]);

    function productDetailsController($scope,$state,$stateParams,blockUI,ngToast,productService) {
        var productDetailsBlockUi = blockUI.instances.get('productDetailsFormBlockUI');
        var vm = this;
        vm.product = {published:true};
        vm.productId = $stateParams.productId;
        vm.publishProduct = publishProduct;
        vm.isPublishing = false;

        activate();

        function activate() {
            getProduct();
        }

        function publishProduct() {
            vm.product.published = true;

            productDetailsBlockUi.start("Publishing...");
            vm.isPublishing = true;

            return productService.save(vm.product).then(function (productModel) {
                vm.product = productModel;
            }).catch(function (error) {
                ngToast.danger('An error occurred.');
                vm.product.published = false;
                console.log(error);
            }).finally(function () {
                productDetailsBlockUi.stop();
                vm.isPublishing = false;
            });
        }

        function getProduct() {
            if (!vm.productId) {
                return;
            }
            productDetailsBlockUi.start('Loading...');
            return productService.getById(vm.productId).then(function (prod) {
                vm.product = prod;
                vm.product.startDate = new Date(prod.startDate);
                vm.product.endDate = new Date(prod.endDate);
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function () {
                productDetailsBlockUi.stop();
            });
        }
    }
})();