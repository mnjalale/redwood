﻿(function () {
    'use strict';
    angular.module('app').controller('upsertProductController', ['$scope', '$rootScope','$q','localStorageService', '$state', '$stateParams', 'blockUI', 'ngToast', 'organizationProducts', 'productService', upsertProductController]);

    function upsertProductController($scope,$rootScope,$q,localStorageService, $state,$stateParams, blockUI, ngToast, organizationProducts, productService) {
        var productBlockUi = blockUI.instances.get('productFormBlockUI');
        var productFeeBlockUi = blockUI.instances.get('productFeeFormBlockUI');
        var authData = localStorageService.get('authorizationData');
        var organizationId = authData.organizationId;
        var vm = this;
        vm.productId = $stateParams.productId;
        vm.operation = !vm.productId ? "Add" : "Edit";
        vm.product = {
            id: 0,
            minimumEmploymentPeriod: 'months',
            interestPeriod: 'yearly',
            productFees: []
        };
        vm.productFee = {};
        vm.isSaving = false;
        vm.isSavingFee = false;
        vm.save = save;
        vm.saveFee = saveFee;


        activate();

        function activate() {
            getProduct();
        }

        function save(isValid,publish) {
            if (!isValid) {
                return;
            }

            vm.product.organizationId = organizationId;

            if (vm.operation==="Add") {
                vm.product.published = publish;
            }

            productBlockUi.start("Saving...");
            vm.isSaving = true;

            return productService.save(vm.product).then(function (productModel) {
                loadProducts();
                $state.go('private.products.details', { productId: productModel.id });
            }).catch(function (error) {
                ngToast.danger(error);
                console.log(error);
            }).finally(function () {
                productBlockUi.stop();
                vm.isSaving = false;
            });

        }

        function saveFee(isValid) {
            if (!isValid) {
                return;
            }

            productFeeBlockUi.start("Saving...");
            vm.isSavingFee = true;

            vm.product.productFees.push(vm.productFee);
            vm.productFee = {};
            productFeeBlockUi.stop();
            vm.isSavingFee = false;
            
        }

        function getProduct() {
            if (!vm.productId) {
                return;
            }
            productBlockUi.start('Loading...');
            return productService.getById(vm.productId).then(function (prod) {
                vm.product = prod;
                vm.product.startDate = new Date(prod.startDate);
                vm.product.endDate = new Date(prod.endDate);
            }).catch(function (ex) {
                console.log(ex.message || 'Error');
            }).finally(function() {
                productBlockUi.stop();
            });
        }

        function loadProducts() {
            return productService.get()
                .then(function (products) {
                    $rootScope.organizationProducts = products;
                    organizationProducts = products;
                })
            .catch(function (error) {
                ngToast.danger('An error occurred.');
            });
        }
    }
})();