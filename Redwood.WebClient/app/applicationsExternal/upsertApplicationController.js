﻿(function () {
    'use strict';
    angular.module('app').controller('upsertApplicationController', ['$scope','$state', 'blockUI', 'ngToast', 'applicationService', 'productService', upsertApplicationController]);

    function upsertApplicationController($scope,$state,blockUI,ngToast,applicationService,productService) {
        var applicationBlockUi = blockUI.instances.get('applicationFormBlockUI');
        var vm = this;
        vm.customer = {};
        vm.application = {};
        vm.product = {};
        vm.products = [];
        vm.totalFees = 0;
        vm.isSaving = false;
        vm.selectedProductChange = selectedProductChange;
        vm.save = save;

        activate();

        function activate() {
            getProducts();
            
        }

        function save(isValid) {
            if (!isValid) {
                return;
            }

            applicationBlockUi.start("Saving...");
            vm.isSaving = true;
            vm.application.customer = vm.customer;
            vm.application.status = "Pending";

            return applicationService.save(vm.application).then(function () {
                $state.go('public.applications.success');
            }).catch(function (error) {
                ngToast.danger('An error occurred.');
                console.log(error);
            }).finally(function () {
                applicationBlockUi.stop();
                vm.isSaving = false;
            });
        }

        function getProducts() {
            return productService.get()
                .then(function (products) {
                    vm.products = products;
                    console.log(products);
                })
            .catch(function (error) {
                ngToast.danger('An error occurred.');
            });
        }

        function selectedProductChange() {
            vm.totalFees = 0;
            var productId = parseInt(vm.application.productId);
            angular.forEach(vm.products, function (product) {
                if (product.id === productId) {
                    vm.product = product;
                    angular.forEach(vm.product.productFees, function(productFee) {
                        vm.totalFees += productFee.amount;
                    });
                }
            });
        }
    }
})();