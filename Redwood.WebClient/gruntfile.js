module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'Built/redwood.min.css': [
                        "css/bootstrap.min.css",
                        "css/datetimepicker.css",
                        "css/font-awesome.min.css",
                        "css/global.min.css",
                        "css/app.min.css",
                        "css/ngToast.min.css",
                        "css/ngToast-animations.min.css",
                        "css/angular-block-ui.min.css",
                        "css/dataTables.bootstrap.min.css"
                    ]
                }
            }
        },
        concat: {
            vendor: {
                src: [
                    "js/jquery.min.js",
                    "js/moment.js",
                    "js/bootstrap.min.js",
                    "js/tether.min.js",
                    "js/pace.min.js",

                    "scripts/lodash.core.min.js",
                    "js/jquery.dataTables.min.js",
                    "js/dataTables.bootstrap.min.js",
                    "js/Chart.min.js",
                    "js/tinycolor.js",
                    "js/app-theme.js",
                    "js/velocity.min.js",
                    "js/velocity.ui.min.js",

                    "scripts/angular.js",
                    "scripts/angular-ui-router.min.js",
                    "scripts/angular-local-storage.min.js",
                    "scripts/angular-datatables.min.js",
                    "scripts/loading-bar.js",
                    "scripts/ui-bootstrap-tpls-0.12.0.min.js",
                    "scripts/angular-messages.min.js",
                    "scripts/mask.min.js",
                    "scripts/isteven-multi-select.js",
                    "scripts/angular-block-ui.min.js",
                    "scripts/angular-animate.min.js",
                    "scripts/angular-sanitize.min.js",
                    "scripts/ngToast.min.js",
                    "scripts/fcsaNumber.min.js",
                    "scripts/datetimepicker.js",
                    "scripts/angular-moment.min.js",
                    "scripts/bootstrap-colorpicker-module.min.js",
                ],
                dest: 'Built/vendor.js'
            }
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            app: {
                files: {
                    'Built/redwood.js': ['app/**/*.js', '!app/security/*.js'],
                }
            }
        },
        uglify: {
            app: {
                src: ['Built/redwood.js'],
                dest: 'Built/redwood.min.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-ng-annotate');

    grunt.registerTask('default', ['cssmin', 'concat', 'ngAnnotate', 'uglify']);
};