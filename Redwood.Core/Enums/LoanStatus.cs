﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum LoanStatus
    {
        [EnumMember(Value = "Not Started")]
        [Description("Not Started")]
        NotStarted = 1,
        [EnumMember(Value = "Active")]
        [Description("Active")]
        Active = 2,
        [EnumMember(Value = "Completed")]
        [Description("Completed")]
        Completed = 3,
        [EnumMember(Value = "Late Payment")]
        [Description("Late Payment")]
        LatePayment = 4,
        [EnumMember(Value = "Defaulted")]
        [Description("Defaulted")]
        Defaulted = 4,
    }
}
