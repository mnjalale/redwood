﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum InvoiceStatus
    {
        [EnumMember(Value = "Unconfirmed")]
        [Description("Unconfirmed")]
        Unconfirmed = 0,
        [EnumMember(Value = "Accepted")]
        [Description("Accepted")]
        Accepted = 1,
        [EnumMember(Value = "Verified")]
        [Description("Verified")]
        Verified = 2,
        [EnumMember(Value = "Trusted")]
        [Description("Trusted")]
        Trusted = 3
    }
}
