﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CompanyLegalStatus
    {
        [EnumMember(Value = "Sole Proprietorship")]
        [Description("Sole Proprietorship")]
        SoleProprietorship = 1,
        [EnumMember(Value = "Partnership")]
        [Description("Partnership")]
        Partnership = 2,
        [EnumMember(Value = "Limited Liability Company")]
        [Description("Limited Liability Company")]
        LimitedLiabilityCompany = 3,
        [EnumMember(Value = "Public Liability Company")]
        [Description("Public Liability Company")]
        PublicLiabilityCompany = 3,
        [EnumMember(Value = "NGO")]
        [Description("NGO")]
        NGO = 4,
        [EnumMember(Value = "Not Registered")]
        [Description("Not Registered")]
        NotRegistered = 5,
    }
}
