﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum InterestPeriod
    {
        [EnumMember(Value = "Daily")]
        [Description("Daily")]
        Daily = 1,

        [EnumMember(Value = "Monthly")]
        [Description("Monthly")]
        Monthly = 2,

        [EnumMember(Value = "Yearly")]
        [Description("Yearly")]
        Annually = 3
    }
}
