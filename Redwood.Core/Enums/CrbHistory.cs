﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CrbHistory
    {
        [EnumMember(Value = "No history")]
        [Description("No history")]
        NoHistory = 1,
        [EnumMember(Value = "Excellent")]
        [Description("Excellent")]
        Excellent = 2,
        [EnumMember(Value = "Good")]
        [Description("Good")]
        Good = 3,
        [EnumMember(Value = "Average")]
        [Description("Average")]
        Average = 4,
        [EnumMember(Value = "Below Average")]
        [Description("Below Average")]
        BelowAverage = 5,
        [EnumMember(Value = "Poor")]
        [Description("Poor")]
        Poor = 6
    }
}
