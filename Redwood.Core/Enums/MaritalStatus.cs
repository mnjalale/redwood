﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MaritalStatus
    {
        [EnumMember(Value = "")]
        [Description("")]
        Undefined = 0,
        [EnumMember(Value = "Single")]
        [Description("Single")]
        Single = 1,
        [EnumMember(Value = "Married")]
        [Description("Married")]
        Married = 2,
        [EnumMember(Value = "Divorced")]
        [Description("Divorced")]
        Divorced = 3

    }
}
