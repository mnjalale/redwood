﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FeeComputation
    {
        [EnumMember(Value = "fixed amount")]
        [Description("fixed amount")]
        FixedAmount = 1,

        [EnumMember(Value = "% of loan amount")]
        [Description("% of loan amount")]
        PercentageOfLoanAmount = 2,
    }
}
