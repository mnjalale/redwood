﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FeeRecovery
    {
        [EnumMember(Value = "added to principal")]
        [Description("added to principal")]
        AddedToPrincipal = 1,

        [EnumMember(Value = "deducted from principal")]
        [Description("deducted from principal")]
        DeductedFromPrincipal = 2,

        [EnumMember(Value = "paid upfront")]
        [Description("paid upfront")]
        PaidUpfront = 3,
    }
}
