﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Enums
{
    public enum EntityStatus
    {
        Active = 1,
        Deleted = 2,
        Inactive = 3
    }
}
