﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AppraisalType
    {
        [EnumMember(Value = "Undefined")]
        [Description("Undefined")]
        Undefined = 0,
        [EnumMember(Value = "Risk")]
        [Description("Risk")]
        Risk = 1,
        [EnumMember(Value = "Confidence")]
        [Description("Confidence")]
        Confidence = 1
    }
}
