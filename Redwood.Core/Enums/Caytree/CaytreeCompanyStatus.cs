﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums.Caytree
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CaytreeCompanyStatus
    {
        [EnumMember(Value = "Registered")]
        [Description("Registered")]
        Registered = 0,
        [EnumMember(Value = "Verified")]
        [Description("Verified")]
        Verified = 1,
        [EnumMember(Value = "Trusted")]
        [Description("Trusted")]
        Trusted = 2
    }
}
