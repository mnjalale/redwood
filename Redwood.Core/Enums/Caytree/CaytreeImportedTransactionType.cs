﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums.Caytree
{

    [JsonConverter(typeof(StringEnumConverter))]
    public enum CaytreeImportedTransactionType
    {
        [EnumMember(Value = "KopoKopo")]
        [Description("KopoKopo")]
        KopoKopo = 1,
        [EnumMember(Value = "M-Pesa")]
        [Description("M-Pesa")]
        MPesa = 2
    }
}
