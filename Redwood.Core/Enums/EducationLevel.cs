﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum EducationLevel
    {
        [EnumMember(Value = "")]
        [Description("")]
        Undefined = 0,

        [EnumMember(Value = "Degree")]
        [Description("Degree")]
        Degree = 1
       
    }
}
