﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ApplicationNoteType
    {
         [EnumMember(Value = "Rejection Reason")]
         [Description("Rejection Reason")]
         RejectionReason = 1,
         [EnumMember(Value = "Recommendation")]
         [Description("Recommendation")]
         Recommendation = 2,
    }
}
