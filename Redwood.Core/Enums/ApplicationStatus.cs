﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ApplicationStatus
    {
        [EnumMember(Value = "All")]
        [Description("All")]
        All = 0, //This is the default value
        [EnumMember(Value = "Pending")]
        [Description("Pending")]
        Pending = 1,
        [EnumMember(Value = "Approved")]
        [Description("Approved")]
        Approved = 2,
        [EnumMember(Value = "Disbursed")]
        [Description("Disbursed")]
        Disbursed = 3,
        [EnumMember(Value = "Rejected")]
        [Description("Rejected")]
        Rejected = 4,
        [EnumMember(Value = "Cancelled By User")]
        [Description("Cancelled By User")]
        CancelledByUser = 5
    }
}
