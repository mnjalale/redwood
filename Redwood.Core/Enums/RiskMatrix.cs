﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RiskMatrix
    {
        [EnumMember(Value = "Low")]
        [Description("Low")]
        Low = 1,
        [EnumMember(Value = "Medium")]
        [Description("Medium")]
        Medium = 2,
        [EnumMember(Value = "High")]
        [Description("High")]
        High = 3
    }
}
