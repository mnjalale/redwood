﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
     [JsonConverter(typeof(StringEnumConverter))]
    public enum EmploymentStatus
    {
        [EnumMember(Value = "Employed")]
        [Description("Employed")]
        Employed = 1,
        [EnumMember(Value = "Self Employed")]
        [Description("Self Employed")]
        SelfEmployed = 2,
        [EnumMember(Value = "Not Employed")]
        [Description("Not Employed")]
        NotEmployed = 3
    }
}
