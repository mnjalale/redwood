﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum PaymentMethod
    {
        [EnumMember(Value = "M-Pesa")]
        [Description("M-Pesa")]
        MPesa = 1,
        [EnumMember(Value = "Bank Transfer")]
        [Description("Bank Transfer")]
        BankTransfer = 2,
        [EnumMember(Value = "Bank Deposit")]
        [Description("Bank Deposit")]
        BankDeposit = 3,
        [EnumMember(Value = "Wallet")]
        [Description("Wallet")]
        Wallet = 3,

    }
}
