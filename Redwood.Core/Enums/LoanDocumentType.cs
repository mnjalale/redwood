﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum LoanDocumentType
    {
        [EnumMember(Value = "National ID")]
        [Description("National ID")]
        NationalId = 1,

        [EnumMember(Value = "Passport")]
        [Description("Passport")]
        Passport = 2,

        [EnumMember(Value = "Bank Statement")]
        [Description("Bank Statement")]
        BankStatement = 3,
    }
}
