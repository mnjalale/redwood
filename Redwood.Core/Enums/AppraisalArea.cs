﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Redwood.Core.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AppraisalArea
    {
        [EnumMember(Value = "Debt to Income Ratio")]
        [Description("Debt to Income Ratio")]
        DebtToIncomeRatio = 1,
        [EnumMember(Value = "Employment Length")]
        [Description("Employment Length")]
        EmploymentLength = 2,
        [EnumMember(Value = "Collateral Coverage")]
        [Description("Collateral Coverage")]
        CollateralCoverage = 3,
        [EnumMember(Value = "System Usage - Duration")]
        [Description("System Usage - Duration")]
        SystemUsageDuration = 4,
        [EnumMember(Value = "System Usage - Frequency")]
        [Description("System Usage - Frequency")]
        SystemUsageFrequency = 5,
        [EnumMember(Value = "Financial Report Completeness")]
        [Description("Financial Report Completeness")]
        FinancialReportCompleteness = 6,
        [EnumMember(Value = "Cash Balance Coverage")]
        [Description("Cash Balance Coverage")]
        CashBalanceCoverage = 7,
        [EnumMember(Value = "Caytree Confidence Score")]
        [Description("Caytree Confidence Score")]
        CaytreeConfidenceScore =8 ,
        [EnumMember(Value = "CRB Score")]
        [Description("CRB Score")]
        CrbScore = 9,
        [EnumMember(Value = "Appraisal Summary")]
        [Description("Appraisal Summary")]
        AppraisalSummary = 10,
        [EnumMember(Value = "Appraisal Report")]
        [Description("Appraisal Report")]
        AppraisalReport = 11,
        [EnumMember(Value = "Imported Transactions")]
        [Description("Imported Transactions")]
        ImportedTransactions = 12
    }
}
