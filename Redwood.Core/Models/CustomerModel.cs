﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Enums;
using Redwood.Core.Utils.Helpers;

namespace Redwood.Core.Models
{
    public class CustomerModel : EntityModel
    {
        [StringLength(30,ErrorMessage = "Maximum length for first name is 30.")]
        [Required(ErrorMessage = "First name is required!")]
        public string FirstName { get; set; }

        [StringLength(30,ErrorMessage = "Maximum length for middle name is 30.")]
        public string MiddleName { get; set; }

        [StringLength(30,ErrorMessage = "Maximim length for last name is 30")]
        [Required(ErrorMessage = "Last name is requried!")]
        public string LastName { get; set; }

        [StringLength(20,ErrorMessage = "Maximum length for ID number is 30")]
        [Required(ErrorMessage = "Id number is required!")]
        public string IdNo { get; set; }

        public int OrganizationId { get; set; }

        [Required(ErrorMessage = "Date of birth is required.")]
        public DateTime DateOfBirth { get; set; }

        public int Age
        {
            get { return Utility.GetAgeInYears(DateOfBirth); }
        }

        //[Required(ErrorMessage = "Gender is required.")]
        public Gender? Gender { get; set; }

        [DataType(DataType.EmailAddress,ErrorMessage = "Enter valid email address.")]
        [StringLength(50,ErrorMessage = "Maximum length for email is 50.")]
        public String Email { get; set; }

        [Required(ErrorMessage = "Phone number is required.")]
        [StringLength(30,ErrorMessage = "Maximum length for phone number is 30.")]
        public string PhoneNumber { get; set; }

        [StringLength(100,ErrorMessage = "Maximum length for postal address is 100.")]
        public string PostalAddress { get; set; }

        [StringLength(100,ErrorMessage = "Maximum length for physical address is 100")]
        public string PhysicalAddress { get; set; }

        [StringLength(30,ErrorMessage = "Maximum length for city is 30.")]
        public string City { get; set; }

        [StringLength(30,ErrorMessage = "Maximum length for county is 30.")]
        public string County { get; set; }

        [Required(ErrorMessage = "Employment status is required.")]
        public EmploymentStatus EmploymentStatus { get; set; }

        public DateTime? EmploymentStartDate { get; set; }

        public string CompanyName { get; set; }

        public decimal GrossMonthlySalary { get; set; }

        public CompanyLegalStatus CompanyLegalStatus { get; set; }

        public decimal BusinessIncome { get; set; }

        public bool OtherBusiness { get; set; }

        public decimal OtherBusinessMonthlyIncome { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public EducationLevel EducationLevel { get; set; }
        
        [StringLength(30,ErrorMessage = "Maximum length for bank name is 30.")]
        public string BankName { get; set; }

        [StringLength(30,ErrorMessage = "Maximum length for bank account no is 30.")]
        public string BankAccountNo { get; set; }

        [StringLength(30,ErrorMessage = "Maximum length for bank branch is 30.")]
        public string BankBranch { get; set; }

        [StringLength(30,ErrorMessage = "Maximum length for mobile payment phone numer is 30.")]
        public string MobilePaymentPhoneNumber { get; set; }

        public int CaytreeCompanyId { get; set; }

        [StringLength(30, ErrorMessage = "Maximum length for caytree wallet phone numer is 30.")]
        public string CaytreeWalletPhoneNumber { get; set; } //0721111111
    }
}
