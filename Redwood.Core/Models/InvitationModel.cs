﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Models
{
    public class InvitationModel : EntityModel
    {
        [Required(ErrorMessage = "Organization name is required.")]
        [StringLength(50,ErrorMessage = "Maximum length for organization name is 50.")]
        public string OrganizationName { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [StringLength(50,ErrorMessage = "Maximum length for first name is 50.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        [StringLength(50,ErrorMessage = "Maximum length for last name is 50.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        [StringLength(50,ErrorMessage = "Maximum length for email is 50.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone number is required.")]
        [StringLength(50,ErrorMessage = "Maximum length for phone number is 50.")]
        public string PhoneNumber { get; set; }

        public bool Processed { get; set; }
    }
}
