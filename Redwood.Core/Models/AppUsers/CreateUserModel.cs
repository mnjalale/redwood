﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Models.AppUsers
{
    public class CreateUserModel : BaseUserModel
    {

        [Display(Name = "Role Name")]
        public string RoleName { get; set; }

        [Required]
        public int OrganizationId { get; set; }
      
    }
}
