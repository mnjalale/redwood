﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Models.AppUsers
{
    public class CreateUserAndOrganizationModel : BaseCreateUserModel
    {
        [Required(ErrorMessage = "Organization name is required.")]
        [StringLength(100,ErrorMessage = "Maximum length for organization name is 100.")]
        public string OrganizationName { get; set; }
    }
}
