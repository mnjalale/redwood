﻿namespace Redwood.Core.Models.Caytree
{
    public class CaytreeApplicationDashboard 
    {
        public int ApplicationsCount { get; set; }
        public decimal ApplicationsAmount { get; set; }
    }
}
