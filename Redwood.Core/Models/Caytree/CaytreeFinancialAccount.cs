﻿namespace Redwood.Core.Models.Caytree
{
    public class CaytreeFinancialAccount
    {
        public string Name { get; set; }
        public decimal Balance { get; set; }
    }
}
