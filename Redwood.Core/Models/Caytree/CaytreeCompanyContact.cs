﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Enums.Caytree;

namespace Redwood.Core.Models.Caytree
{
    public class CaytreeCompanyContact
    {
        public string Name { get; set; }
        public int ContactCaytreeCompanyId { get; set; }
        public string Description { get; set; }
        public decimal ApprovedLimit { get; set; }
        public CaytreeCompanyStatus Status { get; set; }

    }
}
