﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Models.Caytree
{
    public class CaytreePayment
    {
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
    }
}
