﻿using System;
using System.Collections.Generic;

namespace Redwood.Core.Models.Caytree
{
    public class CaytreeLoanDashboard
    {
        public DateTime NextRepaymentDate { get; set; }
        public decimal NextRepaymentAmount { get; set; }
        public int LoansCount { get; set; }
        public decimal LoansAmount { get; set; }
        public decimal LoansBalance { get; set; }
        public List<LoanModel> Loans { get; set; }
        public List<LoanApplicationModel> Applications { get; set; }
        public int OverduePaymentsCount { get; set; }
        public decimal QualificationAmount { get; set; }
        public decimal PotentialQualificationAmount { get; set; }

    }
}
