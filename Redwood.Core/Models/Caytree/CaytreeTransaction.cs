﻿using System;

namespace Redwood.Core.Models.Caytree
{
    public class CaytreeTransaction
    {
        public string TransactionType { get; set; }
        public DateTime Date { get; set; }
        public Decimal Amount { get; set; }

    }
}
