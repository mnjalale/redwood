﻿using System;
using System.Collections.Generic;
using Redwood.Core.Enums;

namespace Redwood.Core.Models.Caytree
{
    public class CaytreeInvoice
    {
        public CaytreeInvoice()
        {
            PaymentHistory = new List<CaytreePayment>();
        }

        public Guid Id { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal OutstandingAmount { get; set; }
        public InvoiceStatus InvoiceStatus { get; set; }
        public int CompanyContactId { get; set; }
        public bool InvoiceIsVerified { get; set; }
        public List<CaytreePayment> PaymentHistory { get; set; }
        public CaytreeCompanyContact Supplier { get; set; }
    }
}
