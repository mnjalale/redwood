﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Models
{
    public class HomeDashboardModel
    {
        public HomeDashboardModel()
        {
            ApplicationGraphLabels = new List<string>();
            ApplicationGraphData = new List<int>();
            LoanPortfolioGraphLabels = new List<string>();
            LoanPortfolioGraphData = new List<int>();
        }

        public string OrganizationName { get; set; }
        public decimal TotalLoanPortfolio { get; set; }
        public int LoansAtRisk { get; set; }
        public decimal LoansAtRiskPercent { get; set; }
        public decimal LoansAtRiskAmount { get; set; }
        public decimal WalletBalance { get; set; }
        public int ApplicationsInTheLast12Months { get; set; }
        public int PendingApplications { get; set; }
        public decimal PendingApplicationsAmount { get; set; }
        public int ApprovedApplications { get; set; }
        public decimal ApprovedApplicationsAmount { get; set; }
        public List<String> ApplicationGraphLabels { get; set; }
        public List<int> ApplicationGraphData { get; set; }
        public List<String> LoanPortfolioGraphLabels { get; set; }
        public List<int> LoanPortfolioGraphData { get; set; }
    }
}
