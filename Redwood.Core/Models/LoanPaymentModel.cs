﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Enums;

namespace Redwood.Core.Models
{
    public class LoanPaymentModel : EntityModel
    {
        public LoanPaymentModel()
        {
            Date = DateTime.Today;
            PaymentMethod = PaymentMethod.Wallet;
        }

        [Required(ErrorMessage = "Associated loan is required.")]
        [Range(1, int.MaxValue,ErrorMessage = "Associated loan is required")]
        public int LoanId { get; set; }

        [Required(ErrorMessage = "Amount is required.")]
        [Range(1,double.MaxValue,ErrorMessage = "Amount is required.")]
        public decimal Amount { get; set; }

        public DateTime Date { get; set; }

        [StringLength(30,ErrorMessage = "Maximum length for reference is 30.")]
        public string Reference { get; set; }

        [Required(ErrorMessage = "Payment method is required.")]
        public PaymentMethod PaymentMethod { get; set; }

        //Relations
        public LoanModel Loan { get; set; }
    }
}
