﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Enums;

namespace Redwood.Core.Models
{
    public class ProductModel : EntityModel
    {
        public ProductModel()
        {
            ProductFees = new List<ProductFeeModel>();
        }
        //[Required(ErrorMessage = "Category is required.")]
        public int? CategoryId { get; set; }
        [Required(ErrorMessage = "Organization is required.")]
        [Range(1, int.MaxValue,ErrorMessage = "Organization is required.")]
        public int OrganizationId { get; set; }
        [StringLength(50,ErrorMessage = "Maximum length for name is 50.")]
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        [StringLength(250,ErrorMessage = "Maximum length for description is 250")]
        public string Description { get; set; }
        public ProductVisibility Visibility { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal MinimumAmount { get; set; }
        public decimal MaximumAmount { get; set; }
        public decimal InterestRate { get; set; }
        public InterestPeriod InterestPeriod { get; set; }
        public bool Published { get; set; }
        public int MinimumRepaymentPeriod { get; set; }
        public int MaximumRepaymentPeriod { get; set; }
        public int RepaymentStartDays { get; set; }
        public decimal MinimumPercentageOfMonthlyIncome { get; set; }
        public bool Employed { get; set; }
        public int MinimumPeriodOfEmployment { get; set; }
        public EmploymentPeriod MinimumEmploymentPeriod { get; set; }
        public bool CrbHistory { get; set; }
        public CrbHistory CrbHistoryDetails { get; set; }
        public bool Collateral { get; set; }
        public decimal CollateralPercentage { get; set; }
        public bool PersonalNationalId { get; set; }
        public bool PersonalKraPin { get; set; }
        public bool PersonalBankStatements { get; set; }
        public int PersonalBankStatementMonths { get; set; }
        public bool BusinessOwnerNationalId { get; set; }
        public bool BusinessOwnerKraPin { get; set; }
        public bool DirectorNationalId { get; set; }
        public bool DirectorKraPin { get; set; }
        public bool CompanyRegistration { get; set; }
        public bool CompanyBusinessPermit { get; set; }
        public bool CompanyPin { get; set; }
        public bool FinancialCollateral { get; set; }
        public bool FinancialBankStatements { get; set; }
        public int FinancialBankStatementMonths { get; set; }
        public bool FinancialStatements { get; set; }
        public bool FinancialCrbReport { get; set; }
        
        public List<ProductFeeModel> ProductFees { get; set; }
        public OrganizationModel Organization { get; set; }
    }
}
