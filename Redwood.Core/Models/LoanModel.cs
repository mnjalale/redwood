﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Enums;

namespace Redwood.Core.Models
{
    public class LoanModel : EntityModel
    {
        public LoanModel()
        {
            LoanPayments= new List<LoanPaymentModel>();
            AmortizationSchedule = new List<LoanAmortizationScheduleModel>();
        }

        [Required(ErrorMessage = "Customer is required.")]
        [Range(1,int.MaxValue,ErrorMessage = "Customer is required.")]
        public int CustomerId { get; set; }

        [Required(ErrorMessage = "Product is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "Product is required.")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Application is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "Application is required.")]
        public int ApplicationId { get; set; }

        [Required(ErrorMessage = "Organization is required.")]
        [Range(1, int.MaxValue,ErrorMessage = "Organization is required.")]
        public int OrganizationId { get; set; }

        [Required(ErrorMessage = "Disbursal date is required.")]
        public DateTime DisbursalDate { get; set; }

        [Required(ErrorMessage = "Maturity date is required.")]
        public DateTime MaturityDate { get; set; }

        [Required(ErrorMessage = "Repayment period is required.")]
        public int RepaymentPeriod { get; set; }

        [Required(ErrorMessage = "Amount is required.")]
        public decimal Amount { get; set; }

        public LoanStatus LoanStatus { get; set; }

       
        //Relations
        public virtual CustomerModel Customer { get; set; }
        public virtual ProductModel Product { get; set; }
        public virtual LoanApplicationModel Application { get; set; }

        public List<LoanPaymentModel> LoanPayments { get; set; }
        public List<LoanAmortizationScheduleModel> AmortizationSchedule { get; set; } 

        //Computed fields
        public decimal PaidAmount { get { return LoanPayments.Sum(c => c.Amount); } }
        public decimal Balance { get { return RepaymentAmount - LoanPayments.Sum(c => c.Amount); } }

        public decimal PercentageComplete{get{return RepaymentAmount==0?0: (PaidAmount/RepaymentAmount)*100;}}

        public decimal PercentageRemaining { get { return 100 - PercentageComplete; } }
        public decimal RepaymentAmount { get { return AmortizationSchedule.Sum(c => c.Repayment); } }
        public decimal Interest { get { return AmortizationSchedule.Sum(c => c.Interest); } }
    }
}
