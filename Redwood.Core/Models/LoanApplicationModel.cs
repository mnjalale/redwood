﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Enums;

namespace Redwood.Core.Models
{
    public class LoanApplicationModel : EntityModel
    {
        public LoanApplicationModel()
        {
            Status = ApplicationStatus.Pending;
            ApplicationDate = DateTime.Today;
            Notes = new List<LoanApplicationNoteModel>();
            Documents = new List<LoanApplicationDocumentModel>();
            Appraisals = new List<LoanApplicationAppraisalModel>();
            AmortizationSchedule = new List<LoanAmortizationScheduleModel>();
        }

        //[Required(ErrorMessage = "Customer is required.")]
        public int CustomerId { get; set; }

        [Required(ErrorMessage = "Product is required.")]
        public int ProductId { get; set; }

        public int OrganizationId { get; set; }

        //[Required(ErrorMessage = "Application date is required.")]
        public DateTime ApplicationDate { get; set; }

        //[Required(ErrorMessage = "Repayment date is required.")]
        public int RepaymentPeriod { get; set; }

        public ApplicationStatus Status { get; set; }

        [Required(ErrorMessage = "Amount is required.")]
        [Range(1, double.MaxValue, ErrorMessage = "Minimum amount is 1.")]
        public decimal Amount { get; set; }

        public decimal InterestRate { get; set; }

        public decimal TotalRepayment
        {
            get { return AmortizationSchedule.Sum(c => c.Repayment); }
        }

        public bool IsSmsApplication { get; set; }

        public decimal MaximumQualificationAmount { get; set; }
        public string ApprovalBasis { get; set; } //Invoice vs Imported Transactions
        public int InvoiceTransactionsCount { get; set; }
        public int VerifiedTrustedInvoiceCount { get; set; }
        public decimal TotalOutstandingInvoices { get; set; }
        public decimal TotalOutstandingTrustedVerifiedInvoices { get; set; }
        public decimal NumberOfMonthsForImportedTransactions { get; set; }
        public decimal AverageNumberOfInflowsPerMonth { get; set; }
        public decimal AverageNetInflows { get; set; }
        public string CrbScore { get; set; }


    //Relations
        public CustomerModel Customer { get; set; }
        public ProductModel Product { get; set; }
        public List<LoanApplicationNoteModel> Notes { get; set; }
        public List<LoanApplicationDocumentModel> Documents { get; set; }
        public List<LoanApplicationAppraisalModel> Appraisals { get; set; }
        public List<LoanAmortizationScheduleModel> AmortizationSchedule { get; set; } 

        public bool HasBankStatement
        {
            get { return Documents.Any(c => c.DocumentType == LoanDocumentType.BankStatement); }
        }

        public LoanApplicationDocumentModel BankStatement
        {
            get { return Documents.FirstOrDefault(c => c.DocumentType == LoanDocumentType.BankStatement); }
        }

        public bool HasNationalId
        {
            get { return Documents.Any(c => c.DocumentType == LoanDocumentType.NationalId); }
        }

        public LoanApplicationDocumentModel NationalId
        {
            get { return Documents.FirstOrDefault(c => c.DocumentType == LoanDocumentType.NationalId); }
        }

        public bool HasPassport
        {
            get { return Documents.Any(c => c.DocumentType == LoanDocumentType.Passport); }
        }

        public LoanApplicationDocumentModel Passport
        {
            get { return Documents.FirstOrDefault(c => c.DocumentType == LoanDocumentType.Passport); }
        }
    }
}
