﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Models
{
    public class CrbScoreModel
    {
        public DateTime Date { get; set; }
        public string Grade { get; set; }
        public decimal ProbabilityOfDefault { get; set; }
        public decimal Score { get; set; }
    }
}
