﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;

namespace Redwood.Core.Models
{
    public class ProductCategoryModel : EntityModel
    {
        [StringLength(20,ErrorMessage = "Maximim length for name is 20.")]
        [Required(ErrorMessage = "Name is requried.")]
        public string Name { get; set; }

        [StringLength(100,ErrorMessage = "Maximum length for description is 100.")]
        public string Description { get; set; }

        //Relations
        public List<ProductModel> Products { get; set; } 
    }
}
