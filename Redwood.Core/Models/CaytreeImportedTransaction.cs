﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Enums.Caytree;

namespace Redwood.Core.Models
{
    public class CaytreeImportedTransaction
    {
        public CaytreeImportedTransactionType ImportedTransactionType { get; set; }
        public decimal Amount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Category { get; set; }
    }
}
