﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Models
{
    public class LoanAmortizationScheduleModel : EntityModel
    {
        public int LoanId { get; set; }
        public int Period { get; set; }
        public decimal Principal { get; set; }
        public decimal Interest { get; set; }
        public decimal Repayment { get; set; }
        public decimal Balance { get; set; }
        public decimal InterestPaid { get; set; }
        public decimal PrincipalPaid { get; set; }
    }
}
