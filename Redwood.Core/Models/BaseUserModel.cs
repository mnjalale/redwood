﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Models
{
    public abstract class BaseUserModel
    {
        [Required(ErrorMessage = "Username is required.")]
        [StringLength(256, ErrorMessage = "Maximum length for username is 256.")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Password is required.")]
        [StringLength(100, ErrorMessage = "The password must be at least 6 characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [StringLength(50,ErrorMessage = "Maximum length for first name is 50.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        [StringLength(50, ErrorMessage = "Maximum length for last name is 50.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email address is required.")]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        [StringLength(50,ErrorMessage = "Maximum length for email address is 50.")]
        public string Email { get; set; }

        [StringLength(50, ErrorMessage = "Maximum length for phone number is 50.")]
        public string PhoneNumber { get; set; }
        
    }
}
