﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Models
{
    public class OrganizationModel : EntityModel
    {
        [Required(ErrorMessage = "Organization name is required!")]
        [StringLength(50,ErrorMessage = "Maximum length for name is 50.")]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress,ErrorMessage = "Enter valid email address.")]
        [StringLength(50,ErrorMessage = "Maximum length for email address is 50.")]
        public string Email { get; set; }

        [StringLength(50,ErrorMessage = "Maximum length for phone number is 50.")]
        public string PhoneNumber { get; set; }

        [StringLength(100,ErrorMessage = "Maximum length for postal address is 100")]
        public string PostalAddress { get; set; }

        [StringLength(100,ErrorMessage = "Maximum length for website is 100")]
        public string Website { get; set; }

        [StringLength(100,ErrorMessage = "Maximum length for contact person name is 100.")]
        public string ContactPersonName { get; set; }

        [StringLength(50,ErrorMessage = "Maximum length for contact person email is 50.")]
        [DataType(DataType.EmailAddress,ErrorMessage = "Enter valid contact person email.")]
        public string ContactPersonEmail { get; set; }

        [StringLength(50,ErrorMessage = "Maximum length for contact person phone is 50.")]
        public string ContactPersonPhone { get; set; }

        [StringLength(100,ErrorMessage = "Maximum length for privacy policy link is 100.")]
        public string PrivacyPolicyLink { get; set; }

        [StringLength(100,ErrorMessage = "Maximum length for terms link is 100.")]
        public string TermsLink { get; set; }

        [StringLength(50,ErrorMessage = "Maximum length for city is 50")]
        public string City { get; set; }

        [StringLength(50,ErrorMessage = "Maximum length for country is 50.")]
        public string Country { get; set; }

        public byte[] Logo { get; set; }

        [StringLength(7, ErrorMessage = "Maximum length for Barand Bg Color is 7.")]
        public string BrandColorBg { get; set; }

        [StringLength(7, ErrorMessage = "Maximum length for Brand Text Color is 7.")]
        public string BrandColorText { get; set; }
    }
}
