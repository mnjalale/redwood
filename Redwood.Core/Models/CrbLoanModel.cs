﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Models
{
    public class CrbLoanModel
    {
        public string Type { get; set; }
        public string Status { get; set; }
        public decimal OriginalAmount { get; set; }
        public decimal OutstandingAmount { get; set; }
        public decimal OverdueBalance { get; set; }
        public decimal InstallmentAmount { get; set; }
        public int DaysInArrears { get; set; }
        public DateTime? StartDate { get; set; }
    }
}
