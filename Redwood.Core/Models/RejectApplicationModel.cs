﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Models
{
    public class RejectApplicationModel
    {
        [Required(ErrorMessage = "Associated application is required.")]
        [Range(1,int.MaxValue,ErrorMessage = "Associated application is required.")]
        public int ApplicationId { get; set; }
        public bool ApplicationIncomplete { get; set; }
        public bool LoanAmountNotQualified { get; set; }
        public bool IncompleteDocuments { get; set; }
        public string OtherReasons { get; set; }
        public string Recommendations { get; set; }
    }
}
