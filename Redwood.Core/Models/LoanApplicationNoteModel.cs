﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Enums;

namespace Redwood.Core.Models
{
    public class LoanApplicationNoteModel : EntityModel
    {
        public int LoanApplicationId { get; set; }

        [StringLength(250,ErrorMessage = "Maximum length for notes is 250")]
        public string Note { get; set; }

        public ApplicationNoteType ApplicationNoteType { get; set; }

    }
}
