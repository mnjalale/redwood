﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Utils.Helpers;

namespace Redwood.Core.Enums
{
    public class LoanApplicationDocumentModel
    {
        public int Id { get; set; }
        public int LoanApplicationId { get; set; }
        public LoanDocumentType DocumentType { get; set; }
        public string ImageName { get; set; }
        public Guid ImageGuid { get; set; }
        public DateTime UploadDate { get; set; }

        public string DisplayImageName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(ImageName))
                {
                    return string.Empty;
                }
                else
                {
                    var extension = ImageName.Substring(ImageName.IndexOf(".", System.StringComparison.Ordinal));
                    return DocumentType.GetDescription() + extension;
                }
            }
        }

        public string Extension
        {
            get
            {
                if (String.IsNullOrWhiteSpace(ImageName))
                {
                    return string.Empty;
                }
                else
                {
                    var extension = ImageName.Substring(ImageName.IndexOf(".", System.StringComparison.Ordinal));
                    return extension;
                }
            }
        }
    }
}
