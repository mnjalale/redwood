﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Enums;

namespace Redwood.Core.Models
{
    public class ProductFeeModel : EntityModel
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public FeeComputation Computation { get; set; }
        public decimal Amount { get; set; }
        public FeeRecovery Recovery { get; set; }

    }
}
