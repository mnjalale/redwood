﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Utils.Helpers
{
    public static class Utility
    {
        public static int GetAgeInYears(DateTime dateOfBirth)
        {
            try
            {
                var age = DateTime.Now.Year - dateOfBirth.Year;
                if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                    age -= 1;

                return age;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
