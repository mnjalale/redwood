﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Utils
{
    public class OperationResponse
    {
        public OperationResponse()
        {

        }

        public OperationResponse(bool succeeded, string msg)
        {
            HasSucceeded = succeeded;
            Message = msg;
        }

        public Boolean HasSucceeded { get; set; }
        public string Message { get; set; }
        public object ReturnObject { get; set; }
    }
}
