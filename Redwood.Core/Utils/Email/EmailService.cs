﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Redwood.Core.Utils.Email
{
    public class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            await
                Task.Run(
                    () => EmailManager.SendEmail(message.Subject, message.Body, new List<string> { message.Destination }));
        }

    }
}
