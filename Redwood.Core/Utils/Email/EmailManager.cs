﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Utils.Settings;

namespace Redwood.Core.Utils.Email
{
    public class EmailManager
    {
        public static void SendEmail(string subject, string body, List<string> to, List<string> cc = null, List<string> bcc = null)
        {
            try
            {
                ValidateEmail(subject, body, to);

                var email = new MailMessage()
                {
                    From = new MailAddress(GlobalSettings.EmailUser, GlobalSettings.EmailDisplayName),
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true
                };

                foreach (var address in to.Where(address => !String.IsNullOrEmpty(address.Trim())))
                {
                    email.To.Add(address.Trim());
                }

                if (cc != null)
                {
                    foreach (var address in cc.Where(address => !String.IsNullOrEmpty(address.Trim())))
                    {
                        email.CC.Add(address.Trim());
                    }
                }

                if (bcc != null)
                {
                    foreach (var address in bcc.Where(address => !String.IsNullOrEmpty(address.Trim())))
                    {
                        email.Bcc.Add(address.Trim());
                    }
                }

                SendEmail(email);

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        private static void SendEmail(MailMessage message)
        {
            try
            {
                EmailClient.Send(message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static SmtpClient EmailClient
        {
            get
            {
                return new SmtpClient()
                {
                    UseDefaultCredentials = false,
                    Host = GlobalSettings.EmailHost,
                    Port = GlobalSettings.EmailPort,
                    EnableSsl = GlobalSettings.EmailEnableSsl,
                    Credentials =
                        GlobalSettings.EmailUseDefaultCredentials
                            ? CredentialCache.DefaultNetworkCredentials
                            : new NetworkCredential(GlobalSettings.EmailUser, GlobalSettings.EmailPassword)
                };
            }
        }

        private static void ValidateEmail(string subject, string body, List<string> to)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(subject))
                    throw new Exception("Email subject is required!");

                if (string.IsNullOrWhiteSpace(body))
                    throw new Exception("Email body is required!");

                if (to == null || to.Count == 0 || !to.Exists(c => !string.IsNullOrWhiteSpace(c)))
                    throw new Exception("Recipient address is required!");
            }
            catch (Exception ex)
            {
                throw new Exception("Errors occurred while validating email!");
            }
        }
    }
}
