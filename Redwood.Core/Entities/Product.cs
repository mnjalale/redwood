﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities
{
    public class Product : Entity
    {
        public Product()
        {
            ProductFees = new List<ProductFee>();
        }
        [ForeignKey("Category")]
        public int? CategoryId { get; set; }
        [Required]
        [Range(1,int.MaxValue)]
        public int OrganizationId { get; set; }
        [StringLength(50)]
        [Required]
        public string Name { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        public ProductVisibility Visibility { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal MinimumAmount { get; set; }
        public decimal MaximumAmount { get; set; }
        public decimal InterestRate { get; set; }
        public InterestPeriod InterestPeriod { get; set; }
        public bool Published { get; set; }
        public int MinimumRepaymentPeriod { get; set; }
        public int MaximumRepaymentPeriod { get; set; }
        public int RepaymentStartDays { get; set; }
        public decimal MinimumPercentageOfMonthlyIncome { get; set; }
        public bool Employed { get; set; }
        public int MinimumPeriodOfEmployment { get; set; }
        public EmploymentPeriod MinimumEmploymentPeriod { get; set; }
        public bool CrbHistory { get; set; }
        public CrbHistory CrbHistoryDetails { get; set; }
        public bool Collateral { get; set; }
        public decimal CollateralPercentage { get; set; }
        public bool PersonalNationalId { get; set; }
        public bool PersonalKraPin { get; set; }
        public bool PersonalBankStatements { get; set; }
        public int PersonalBankStatementMonths { get; set; }
        public bool BusinessOwnerNationalId { get; set; }
        public bool BusinessOwnerKraPin { get; set; }
        public bool DirectorNationalId { get; set; }
        public bool DirectorKraPin { get; set; }
        public bool CompanyRegistration { get; set; }
        public bool CompanyBusinessPermit { get; set; }
        public bool CompanyPin { get; set; }
        public bool FinancialCollateral { get; set; }
        public bool FinancialBankStatements { get; set; }
        public int FinancialBankStatementMonths { get; set; }
        public bool FinancialStatements { get; set; }
        public bool FinancialCrbReport { get; set; }  

        //Relationships
        public virtual ProductCategory Category { get; set; }
        public virtual List<ProductFee> ProductFees { get; set; }
        
    }
}
