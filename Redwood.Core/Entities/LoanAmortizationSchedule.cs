﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;

namespace Redwood.Core.Entities
{
    public class LoanAmortizationSchedule : Entity
    {
        [ForeignKey("Loan")]
        public int LoanId { get; set; }
        public int Period { get; set; }
        public decimal Principal { get; set; }
        public decimal Interest { get; set; }
        public decimal Repayment { get; set; }
        public decimal Balance { get; set; }
        public decimal InterestPaid { get; set; }
        public decimal PrincipalPaid { get; set; }

        //Relations
        public Loan Loan { get; set; }

    }
}
