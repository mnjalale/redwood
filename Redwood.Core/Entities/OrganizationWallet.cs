﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;
using Redwood.Core.Utils.Helpers;

namespace Redwood.Core.Entities
{
    public class OrganizationWallet : Entity
    {
        [ForeignKey("Organization")]
        public int OrganizationId { get; set; }
        
        public string UsernameE { get; set; }

        public string PasswordE { get; set; }

        //Relations
        public Organization Organization { get; set; }


        #region Not Mapped

        [NotMapped]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string Username
        {
            get { return StringCipher.Decrypt(UsernameE); }
            set { UsernameE = StringCipher.Encrypt(value); }
        }

        [NotMapped]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string Password
        {
            get { return StringCipher.Decrypt(PasswordE); }
            set { PasswordE = StringCipher.Encrypt(value); }
        }

        #endregion
    }
}
