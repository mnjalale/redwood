﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities
{
    public class Customer : Entity
    {
        [StringLength(30)]
        [Required]
        public string FirstName { get; set; }

        [StringLength(30)]
        public string MiddleName { get; set; }

        [StringLength(30)]
        [Required]
        public string LastName { get; set; }

        [StringLength(30)]
        [Required]
        public string IdNo { get; set; }

        [Required]
        public int OrganizationId { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        //[Required]
        public Gender? Gender { get; set; }

        [DataType(DataType.EmailAddress)]
        [StringLength(50)]
        public String Email { get; set; }

        [Required]
        [StringLength(30)]
        public string PhoneNumber { get; set; }

        [StringLength(100)]
        public string PostalAddress { get; set; }

        [StringLength(100)]
        public string PhysicalAddress { get; set; }

        [StringLength(30)]
        public string City { get; set; }

        [StringLength(30)]
        public string County { get; set; }

        [Required]
        public EmploymentStatus EmploymentStatus { get; set; }

        public DateTime? EmploymentStartDate { get; set; }

        public string CompanyName { get; set; }

        public decimal GrossMonthlySalary { get; set; }

        public CompanyLegalStatus CompanyLegalStatus { get; set; }

        public decimal BusinessIncome { get; set; }

        public bool OtherBusiness { get; set; }

        public decimal OtherBusinessMonthlyIncome { get; set; }

        public MaritalStatus MaritalStatus { get; set; }
        public EducationLevel EducationLevel { get; set; }

        [StringLength(30)]
        public string BankName { get; set; }

        [StringLength(30)]
        public string BankAccountNo { get; set; }

        [StringLength(30)]
        public string BankBranch { get; set; }

        [StringLength(30)]
        public string MobilePaymentPhoneNumber { get; set; }

        public int CaytreeCompanyId { get; set; }
        [StringLength(30)]
        public string CaytreeWalletPhoneNumber { get; set; } //0721111111
    }
}
