﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities
{
    public class LoanApplicationDocument : Entity
    {
        [ForeignKey("LoanApplication")]
        public int LoanApplicationId { get; set; }
        public LoanDocumentType DocumentType { get; set; }
        public string ImageName { get; set; }
        public Guid ImageGuid { get; set; }

        //Relations
        public LoanApplication LoanApplication { get; set; }
    }
}
