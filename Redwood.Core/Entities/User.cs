﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Redwood.Core.Entities.Base;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities
{
    public class User : IdentityUser,IEntity
    {
        public User()
        {
            DateCreated = DateTime.UtcNow;
            LastUpdated = DateTime.UtcNow;
            EntityStatus = EntityStatus.Active;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(
                UserManager<User> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in 
            // CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        [Required]
        public int OrganizationId { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string Surname { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime LastUpdated { get; set; }

        public EntityStatus EntityStatus { get; set; }
    }
}
