﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities
{
    public class LoanApplication : Entity
    {
        public LoanApplication()
        {
            Status = ApplicationStatus.Pending;
            ApplicationDate = DateTime.Today;
            Notes = new List<LoanApplicationNote>();
            Documents = new List<LoanApplicationDocument>();
            Appraisals = new List<LoanApplicationAppraisal>();
        }

        [ForeignKey("Customer")]
        [Required]
        public int CustomerId { get; set; }

        [ForeignKey("Product")]
        [Required]
        public int ProductId { get; set; }

        [Required]
        public int OrganizationId { get; set; }

        [Required]
        public DateTime ApplicationDate { get; set; }

        [Required]
        public int RepaymentPeriod { get; set; }

        public ApplicationStatus Status { get; set; }

        [Required]
        [Range(1, double.MaxValue)]
        public decimal Amount { get; set; }

        public decimal CollateralAmount { get; set; }

        public decimal InterestRate { get; set; }

        //Quick fix, will probably get rid of these, poor design, but we had only one day to do this
        public decimal MaximumQualificationAmount { get; set; }
        public string ApprovalBasis { get; set; } //Invoice vs Imported Transactions
        public int InvoiceTransactionsCount { get; set; }
        public int VerifiedTrustedInvoiceCount { get; set; }
        public decimal TotalOutstandingInvoices { get; set; }
        public decimal TotalOutstandingTrustedVerifiedInvoices { get; set; }
        public decimal NumberOfMonthsForImportedTransactions { get; set; }
        public decimal AverageNumberOfInflowsPerMonth { get; set; }
        public decimal AverageNetInflows { get; set; }
        public string CrbScore { get; set; }


        //Relations
        public virtual Customer Customer { get; set; }
        public virtual Product Product { get; set; }
        public virtual List<LoanApplicationNote> Notes { get; set; }
        public virtual List<LoanApplicationDocument> Documents { get; set; }
        public virtual List<LoanApplicationAppraisal> Appraisals { get; set; } 

    }
}
