﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities
{
    public class Loan : Entity
    {
        
        public Loan()
        {
            LoanStatus = LoanStatus.NotStarted;
            DisbursalDate = DateTime.Today;
            LoanPayments = new List<LoanPayment>();
            AmortizationSchedule = new List<LoanAmortizationSchedule>();
        }

        [ForeignKey("Customer")]
        [Required]
        [Range(1,int.MaxValue)]
        public int CustomerId { get; set; }

        [ForeignKey("Product")]
        [Range(1, int.MaxValue)]
        [Required]
        public int ProductId { get; set; }

        //[ForeignKey("Application")]
        [Range(1, int.MaxValue)]
        [Required]
        public int ApplicationId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int OrganizationId { get; set; }

        [Required]
        public DateTime DisbursalDate { get; set; }

        [Required]
        public DateTime MaturityDate { get; set; }

        public LoanStatus LoanStatus { get; set; }

        [Required]
        public int RepaymentPeriod { get; set; }
        
        [Required]
        public decimal Amount { get; set; }


        //Relations
        public virtual Customer Customer { get; set; }
        public virtual Product Product { get; set; }
        public virtual List<LoanPayment> LoanPayments { get; set; }
        public virtual List<LoanAmortizationSchedule> AmortizationSchedule { get; set; } 
        //public virtual LoanApplication Application { get; set; }
    }
}
