﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities
{
    public class LoanApplicationNote : Entity
    {
        [ForeignKey("LoanApplication")]
        public int LoanApplicationId { get; set; }

        [StringLength(250)]
        public string Note { get; set; }

        public ApplicationNoteType ApplicationNoteType { get; set; }

        //Relations
        public LoanApplication LoanApplication { get; set; }
    }
}
