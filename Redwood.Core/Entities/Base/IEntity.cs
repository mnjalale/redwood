﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities.Base
{
    public interface IEntity
    {
        DateTime DateCreated { get; set; }
        DateTime LastUpdated { get; set; }
        EntityStatus EntityStatus { get; set; }
    }
}
