﻿using System;
using System.ComponentModel.DataAnnotations;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities.Base
{
    public abstract class Entity : IEntity
    {
        protected Entity()
        {
            DateCreated = DateTime.UtcNow;
            LastUpdated = DateTime.UtcNow;
            EntityStatus = EntityStatus.Active;
        }

        [Key]
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastUpdated { get; set; }
        public EntityStatus EntityStatus { get; set; }
    }
}
