﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.Core.Entities.Base
{
    public abstract class EntityWithNameAndDescription : Entity
    {
        [StringLength(30)]
        [Required]
        public string Name { get; set; }

        [StringLength(150)]
        public string Description { get; set; }
    }
}
