﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;

namespace Redwood.Core.Entities
{
    public class ProductCategory : Entity
    {
        [StringLength(20)]
        [Required]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        //Relations
        public List<Product> Products { get; set; } 
    }
}
