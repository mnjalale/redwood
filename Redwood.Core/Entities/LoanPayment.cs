﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities
{
    public class LoanPayment : Entity
    {
        public LoanPayment()
        {
            Date = DateTime.Today;
        }

        [ForeignKey("Loan")]
        [Required]
        [Range(1,int.MaxValue)]
        public int LoanId { get; set; }

        [Required]
        [Range(1,double.MaxValue)]
        public decimal Amount { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [StringLength(30)]
        public string Reference { get; set; }

        [Required]
        public PaymentMethod PaymentMethod { get; set; }

        //Relations
        public Loan Loan { get; set; }
    }
}
