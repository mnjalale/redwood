﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;

namespace Redwood.Core.Entities
{
    public class Organization : Entity
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(100)]
        public string PostalAddress { get; set; }

        [StringLength(100)]
        public string Website { get; set; }

        [StringLength(100)]
        public string ContactPersonName { get; set; }

        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        public string ContactPersonEmail { get; set; }

        [StringLength(50)]
        public string ContactPersonPhone { get; set; }

        [StringLength(100)]
        public string PrivacyPolicyLink { get; set; }

        [StringLength(100)]
        public string TermsLink { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [Column(TypeName = "image")]
        public byte[] Logo { get; set; }

        [StringLength(7)]
        public string BrandColorBg { get; set; }

        [StringLength(7)]
        public string BrandColorText { get; set; }
    }
}
