﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities
{
    public class ProductFee : Entity
    {
        [ForeignKey("Product")]
        [Required]
        public int ProductId { get; set; }
        public string Name { get; set; }
        [Required]
        public FeeComputation Computation { get; set; }
        [Required]
        [Range(0.01,double.MaxValue)]
        public decimal Amount { get; set; }
        [Required]
        public FeeRecovery Recovery { get; set; }

        //Relations
        public Product Product { get; set; }
    }
}
