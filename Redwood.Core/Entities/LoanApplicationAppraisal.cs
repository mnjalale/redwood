﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;
using Redwood.Core.Enums;

namespace Redwood.Core.Entities
{
    public class LoanApplicationAppraisal : Entity
    {
        [ForeignKey("Application")]
        public int ApplicationId { get; set; }

        public AppraisalType Type { get; set; }

        public decimal Weight { get; set; }

        public AppraisalArea Area { get; set; }

        public decimal Calculation { get; set; }

        public string RatingCode { get; set; }

        public decimal Rating { get; set; }

        public string Score { get; set; }

        public string StrengthText { get; set; }

        public string WeaknessText { get; set; }
        

        //Relations
        public LoanApplication Application { get; set; }
    }
}
