﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Redwood.Services.IoC;

namespace Redwood.TestApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());

            MigrateDatabase();
            InitializeIoC();
        }

        private static void MigrateDatabase()
        {
            var redwoodConfiguration = new Data.Migrations.Configuration();
            var redwoodMigrator = new DbMigrator(redwoodConfiguration);
            redwoodMigrator.Update();
        }

        private static void InitializeIoC()
        {
            IoC.Init();
        }
    }
}
