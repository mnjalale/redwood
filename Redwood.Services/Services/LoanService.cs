﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Enums;
using Redwood.Core.Models;
using Redwood.Core.Utils;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.Services
{
    internal class LoanService : ServiceBase, ILoanService
    {
        public LoanService(IEntityToModelFactory modelFactory, IModelToEntityFactory entityFactory) : base(modelFactory, entityFactory)
        {
        }

        public List<Loan> Get(int organizationId)
        {
            try
            {
                return
                   Uow.LoanRepository.Get(c => c.OrganizationId == organizationId,
                       c => c.OrderBy(cc => cc.DateCreated)).ToList();
            }
            catch (Exception)
            {
                
                throw new Exception("Errors occurred while getting loans.");
            }
        }

        public List<Loan> Get(int organizationId, LoanStatus status)
        {
            try
            {
                return
                    Uow.LoanRepository.Get(c => c.OrganizationId == organizationId && c.LoanStatus == status,
                        c => c.OrderBy(cc => cc.DateCreated)).ToList();
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while getting loan applications.");
            }
        }

        public List<Loan> GetLoansForCaytreeUser(int caytreeCompanyId)
        {
            try
            {
                var customer = Uow.CustomerRepository.Get(c => c.CaytreeCompanyId == caytreeCompanyId).FirstOrDefault();
                if (customer == null) return new List<Loan>();

                return
                    Uow.LoanRepository.Get(c => c.CustomerId == customer.Id,
                        c => c.OrderByDescending(cc => cc.DisbursalDate)).ToList();
            }
            catch (Exception)
            {
                throw new Exception("Errors occurred while getting applications.");
            }
        }

        public Loan GetById(int id, int organizationId)
        {
            try
            {
                return Uow.LoanRepository.Get(c => c.Id == id && c.OrganizationId == organizationId).FirstOrDefault();
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while getting loan.");
            }
        }

        public OperationResponse InsertOrUpdate(LoanModel loanModel)
        {
            try
            {
                //Get the product
                var product = Uow.ProductRepository.GetById(loanModel.ProductId);

                //Save the customer
                var customer = Uow.CustomerRepository.Get(c => c.IdNo == loanModel.Customer.IdNo && c.OrganizationId == product.OrganizationId).FirstOrDefault();

                if (customer==null)
                {
                    return ComposeResponse(new Exception("Customer not found."));
                }
                
                //Save the loan
                var loan = EntityFactory.Create(loanModel);
                loan.CustomerId = customer.Id;
                loan.OrganizationId = customer.OrganizationId;

                Uow.LoanRepository.InsertOrUpdate(loan);
                Uow.Save();

                return new OperationResponse()
                {
                    HasSucceeded = true,
                    ReturnObject = ModelFactory.Create(loan)
                };
            }
            catch (Exception ex)
            {
                return ComposeResponse(ex);
            }
        }

        public OperationResponse InsertOrUpdatePayment(LoanPaymentModel paymentModel)
        {
            //todo => This whole method will have to be refactored. It's ugly but it works for now
            try
            {
                var payment = EntityFactory.Create(paymentModel);
                Uow.LoanPaymentRepository.InsertOrUpdate(payment);
                Uow.Save();
                
                //We should return the loan object
                var loan = Uow.LoanRepository.GetById(paymentModel.LoanId);

                var paymentAmount = paymentModel.Amount;
                var amortizations = loan.AmortizationSchedule.OrderBy(c => c.Period).ToList();
                foreach (var loanAmortization in amortizations)
                {
                    var interestAmount = loanAmortization.Interest;
                    var interestPaid = loanAmortization.InterestPaid;
                    var principalAmount = loanAmortization.Repayment-loanAmortization.Interest;
                    var principalPaid = loanAmortization.PrincipalPaid;

                    if (interestAmount>interestPaid)
                    {
                        var interestPayable = interestAmount - interestPaid;

                        if (paymentAmount>interestPayable)
                        {
                            loanAmortization.InterestPaid += interestPayable;
                            paymentAmount -= interestPayable;
                        }
                        else
                        {
                            loanAmortization.InterestPaid += paymentAmount;
                            paymentAmount = 0;
                        }
                        
                    }

                    if (principalAmount > principalPaid)
                    {
                        var principalPayable = principalAmount - principalPaid;

                        if (paymentAmount>principalPayable)
                        {
                            loanAmortization.PrincipalPaid += principalPayable;
                            paymentAmount -= principalPayable;
                        }
                        else
                        {
                            loanAmortization.PrincipalPaid += paymentAmount;
                            paymentAmount = 0;
                        }
                    }

                    Uow.LoanAmortizationScheduleRepository.InsertOrUpdate(loanAmortization);

                }

                Uow.Save();

                //Refetch the loan with the updated amortization schedule.
                loan = Uow.LoanRepository.GetById(paymentModel.LoanId);
                if (loan.AmortizationSchedule.Sum(c=>c.PrincipalPaid)>=loan.Amount)
                {
                    loan.LoanStatus = LoanStatus.Completed;
                }
                else
                {
                    loan.LoanStatus = LoanStatus.Active;
                }

                Uow.LoanRepository.InsertOrUpdate(loan);
                Uow.Save();

                return new OperationResponse()
                {
                    HasSucceeded = true,
                    ReturnObject = ModelFactory.Create(loan)
                };
            }
            catch (Exception ex)
            {
                return ComposeResponse(ex);
            }
        }

        
    }
}
