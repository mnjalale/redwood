﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Utils;
using Redwood.Core.Utils.Validations;
using Redwood.Data.UnitsOfWork;
using Redwood.Services.CRBInfoAPI;
using Redwood.ServicesContracts.IFactories;

namespace Redwood.Services.Services
{
    public abstract class ServiceBase
    {
        //protected IUnitOfWork Uow;
        protected UnitOfWork Uow;
        protected IEntityToModelFactory ModelFactory;
        protected IModelToEntityFactory EntityFactory;
        protected ReportPublicServiceClient CrbClient;

        protected ServiceBase(IEntityToModelFactory modelFactory, IModelToEntityFactory entityFactory)
        {
            Uow = new UnitOfWork();
            ModelFactory = modelFactory;
            EntityFactory = entityFactory;
            InitializeCrb();
           
        }

        private void InitializeCrb()
        {
            CrbClient = new ReportPublicServiceClient();
            if (CrbClient.ClientCredentials != null)
            {
                CrbClient.ClientCredentials.UserName.UserName = "david.yen@barclays.com";
                CrbClient.ClientCredentials.UserName.Password = "Caytree!0";
            }
        }

        protected OperationResponse SuccessResult(string message, object returnObject)
        {
            return new OperationResponse()
            {
                HasSucceeded = true,
                Message = message,
                ReturnObject = returnObject
            };
        }

        protected OperationResponse ComposeResponse(Exception ex)
        {
            var response = new OperationResponse {HasSucceeded = false, Message = ex.Message};

            //if (ex is DomainValidationException)
            //{
            //    response.Message = ex.Message;
            //}
            //else if (ex is DbUpdateException || ex is NullReferenceException || ex is DbEntityValidationException)
            //{
            //    response.Message = "Internal server error";
            //}
            //else
            //{
            //    response.Message = ex.InnerException != null ? ex.Message + ex.InnerException.Message : ex.Message;
            //}

            return response;
        }

        protected T Resolve<T>() where T : class
        {
            return IoC.IoC.Resolve<T>();
        }
    }
}
