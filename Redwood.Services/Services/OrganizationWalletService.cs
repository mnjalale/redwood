﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Models;
using Redwood.Core.Utils;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.Services
{
    internal class OrganizationWalletService : ServiceBase, IOrganizationWalletService
    {
        public OrganizationWalletService(IEntityToModelFactory modelFactory, IModelToEntityFactory entityFactory) : base(modelFactory, entityFactory)
        {
        }

        public List<OrganizationWallet> GetOrganizationWallets(int organizationId)
        {
            try
            {
                return Uow.OrganizationWalletRepository.Get(c => c.OrganizationId == organizationId).ToList();
            }
            catch (Exception)
            {
                
                throw new Exception("Errors occurred while getting organization wallets.");
            }
        }

        public OrganizationWallet GetById(int id, int organizationId)
        {
            try
            {
                return
                    Uow.OrganizationWalletRepository.Get(c => c.Id == id && c.OrganizationId == organizationId)
                        .FirstOrDefault();
            }
            catch (Exception)
            {
                throw new Exception("Errors occurred while getting organization wallet.");
            }
        }

        public OperationResponse InsertOrUpdate(OrganizationWalletModel walletModel)
        {
            try
            {
                //We are limiting the wallet organizations to 1
                var organization = Uow.OrganizationRepository.GetById(walletModel.OrganizationId);
                if (organization == null)
                    return ComposeResponse(new Exception("The organization entered was not found."));

                var organizationWallet =
                    Uow.OrganizationWalletRepository.Get(c => c.OrganizationId == organization.Id).FirstOrDefault();

                OrganizationWallet wallet;
                if (organizationWallet == null)
                {
                    wallet = EntityFactory.Create(walletModel);
                }
                else
                {
                    organizationWallet.Username = walletModel.Username;
                    organizationWallet.Password = walletModel.Password;
                    wallet = organizationWallet;
                }

                
                Uow.OrganizationWalletRepository.InsertOrUpdate(wallet);
                Uow.Save();

                return new OperationResponse()
                {
                   HasSucceeded = true,
                   ReturnObject = ModelFactory.Create(wallet)
                };

            }
            catch (Exception)
            {
                return ComposeResponse(new Exception("Errors occurred while updating wallet."));
            }
        }
    }
}
