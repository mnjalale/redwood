﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Redwood.Core.Enums;
using Redwood.Core.Enums.Caytree;
using Redwood.Core.Models;
using Redwood.Core.Models.Caytree;
using Redwood.DataContracts.IUnitsOfWork;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.Services
{
    internal class CaytreeService : ServiceBase, ICaytreeService
    {
#if DEBUG

        private const string BaseUrl = "http://localhost:25467/";
#else
        private const string BaseUrl = "https://oaktreeapi.caytree.com/";
#endif

        public CaytreeService(IEntityToModelFactory modelFactory, IModelToEntityFactory entityFactory) : base(modelFactory, entityFactory)
        {
        }


        public List<CaytreeTransaction> GetCaytreeTransactions(int companyId,int months)
        {
            try
            {
                const string url = BaseUrl + "api/company/transactions/getall";
                var httpRequest = (HttpWebRequest)WebRequest.Create(url);

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/json";
                
                var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                var json = new JavaScriptSerializer().Serialize(new
                {
                    companyId = companyId,
                    startDate = DateTime.Today.AddMonths(-months)
                });

                streamWriter.Write(json);
                streamWriter.Dispose();

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var result = streamReader.ReadToEnd();

                var array = JArray.Parse(result);

                var transactions = new List<CaytreeTransaction>();
                foreach (var obj in array)
                {
                    transactions.Add(new CaytreeTransaction()
                    {
                        Amount = (decimal)obj["netAmount"],
                        Date = (DateTime)obj["documentDate"],
                        TransactionType = (string)obj["documentType"]
                    });
                }
                return transactions;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<CaytreeFinancialAccount> GetCaytreeCashAndCashEquivalentAccounts(int companyId)
        {
            try
            {
                const string url = BaseUrl + "api/company/accounts/cashandcashequivalents";
                var httpRequest = (HttpWebRequest)WebRequest.Create(url);

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/json";

                var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                var json = new JavaScriptSerializer().Serialize(new
                {
                    companyId = companyId,
                    startDate = DateTime.Today
                });

                streamWriter.Write(json);
                streamWriter.Dispose();

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var result = streamReader.ReadToEnd();

                var array = JArray.Parse(result);

                var accounts = new List<CaytreeFinancialAccount>();
                foreach (var obj in array)
                {
                    accounts.Add(new CaytreeFinancialAccount()
                    {
                        Balance = (decimal)obj["balance"],
                        Name = (string)obj["name"]
                    });
                }
                return accounts;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<CaytreeInvoice> GetCaytreeInvoices(int companyId)
        {
            try
            {
                const string url = BaseUrl + "api/company/invoices/getOutstanding";
                var httpRequest = (HttpWebRequest)WebRequest.Create(url);

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/json";

                var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                var json = new JavaScriptSerializer().Serialize(new
                {
                    companyId = companyId,
                    startDate = DateTime.Today
                });

                streamWriter.Write(json);
                streamWriter.Dispose();

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var result = streamReader.ReadToEnd();

                var array = JArray.Parse(result);

                var invoices = new List<CaytreeInvoice>();
                foreach (var obj in array)
                {
                    var caytreeInvoice = new CaytreeInvoice()
                    {
                        Id = (Guid)obj["documentId"],
                        InvoiceAmount = (decimal) obj["netAmount"],
                        CompanyContactId = (int) obj["companyContactId"],
                        Supplier = GetInvoiceSupplier(companyId,(Guid)obj["documentId"]),
                        InvoiceIsVerified = (bool) obj["documentIsVerified"],
                        InvoiceStatus =
                            (InvoiceStatus) Enum.Parse(typeof (InvoiceStatus), (string) obj["invoiceStatus"])

                    };
                    
                    var paymentHistory = (JArray) obj["paymentHistory"];

                    foreach (var payment in paymentHistory)
                    {
                        caytreeInvoice.PaymentHistory.Add(new CaytreePayment()
                        {
                            Amount = (decimal)payment["netAmount"],
                            Date = (DateTime)payment["dueDate"]
                        });
                    }

                    caytreeInvoice.OutstandingAmount = caytreeInvoice.InvoiceAmount -
                                                       caytreeInvoice.PaymentHistory.Sum(c => c.Amount);

                    invoices.Add(caytreeInvoice);
                }
                return invoices;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<CaytreeImportedTransaction> GetCaytreeImportedTransactions(int companyId)
        {
            try
            {
                const string url = BaseUrl + "api/company/importedTransactions/getAll";
                var httpRequest = (HttpWebRequest)WebRequest.Create(url);

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/json";

                var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                var json = new JavaScriptSerializer().Serialize(new
                {
                    companyId = companyId,
                    startDate = DateTime.Today
                });

                streamWriter.Write(json);
                streamWriter.Dispose();

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var result = streamReader.ReadToEnd();

                var array = JArray.Parse(result);

                var importedTransactions = new List<CaytreeImportedTransaction>();
                foreach (var obj in array)
                {
                    var importedTransaction = new CaytreeImportedTransaction()
                    {
                        Amount = (decimal)obj["amount"],
                        TransactionDate = (DateTime)obj["transactionDate"],
                        Category = (string)obj["category"],
                        ImportedTransactionType = (string)obj["importedTransactionType"] == "M-Pesa"? CaytreeImportedTransactionType.MPesa : 
                            (CaytreeImportedTransactionType)Enum.Parse(typeof(CaytreeImportedTransactionType), (string)obj["importedTransactionType"])

                    };

                    
                    importedTransactions.Add(importedTransaction);
                }
                return importedTransactions;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public CaytreeCompanyContact GetInvoiceSupplier(int companyId,Guid invoiceId)
        {
            try
            {
                const string url = BaseUrl + "api/company/getInvoiceSupplier";
                var httpRequest = (HttpWebRequest)WebRequest.Create(url);

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/json";

                var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                var json = new JavaScriptSerializer().Serialize(new
                {
                    companyId = companyId,
                    invoiceId = invoiceId,
                    startDate = DateTime.Today
                });

                streamWriter.Write(json);
                streamWriter.Dispose();

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var result = streamReader.ReadToEnd();

                var supplier = JObject.Parse(result);
                var invoiceSupplier = new CaytreeCompanyContact()
                {
                    ContactCaytreeCompanyId = (int)supplier["id"],
                    ApprovedLimit = (decimal) supplier["approvedLimit"],
                    Status =
                        (CaytreeCompanyStatus)
                            Enum.Parse(typeof (CaytreeCompanyStatus), (string) supplier["companyStatus"])
                };

                return invoiceSupplier;
            }
            catch (Exception ex)
            {
                return null;
                //throw new Exception(ex.Message);
            }
        }
    }
}
