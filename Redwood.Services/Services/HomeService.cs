﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Enums;
using Redwood.Core.Models;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.Services
{
    internal class HomeService : ServiceBase,IHomeService
    {
        public HomeService(IEntityToModelFactory modelFactory, IModelToEntityFactory entityFactory) : base(modelFactory, entityFactory)
        {
        }

        public HomeDashboardModel GetDashboard(int organizationId)
        {
            try
            {
                var organization = Uow.OrganizationRepository.GetById(organizationId);
                var applications = Uow.LoanApplicationRepository.Get(c => c.OrganizationId == organizationId).ToList();
                var loans = Uow.LoanRepository.Get(c => c.OrganizationId == organizationId).ToList();
                var dashboard = new HomeDashboardModel()
                {
                    OrganizationName = organization.Name,
                    ApplicationsInTheLast12Months =
                        applications.Count(c => c.ApplicationDate > DateTime.Today.AddMonths(-12)),
                    PendingApplications = applications.Count(c => c.Status == ApplicationStatus.Pending),
                    PendingApplicationsAmount =
                        applications.Where(c => c.Status == ApplicationStatus.Pending).Sum(c => c.Amount),
                    ApprovedApplications = applications.Count(c => c.Status == ApplicationStatus.Approved),
                    ApprovedApplicationsAmount =
                        applications.Where(c => c.Status == ApplicationStatus.Approved).Sum(c => c.Amount),
                    TotalLoanPortfolio = loans.Sum(c=>c.Amount),
                    LoansAtRiskAmount = 0,
                    LoansAtRisk = 0,
                    LoansAtRiskPercent = 0,
                    WalletBalance = 0
                };

                //Compute the last 12 months
                var date = DateTime.Today.AddMonths(-11);
                var i = 0;
                var totalLoanPortfolio = 0;
                while (i<12)
                {
                    dashboard.ApplicationGraphLabels.Add(date.ToString("MMM"));
                    dashboard.LoanPortfolioGraphLabels.Add(date.ToString("MMM"));

                    var monthStartDate = new DateTime(date.Year, date.Month, 1);
                    var monthEndDate = monthStartDate.AddMonths(1).AddDays(-1);
                    var monthApplications =
                        applications.Count(c => c.ApplicationDate >= monthStartDate && c.ApplicationDate <= monthEndDate);
                    var monthLoans =
                        loans.Count(c => c.DisbursalDate >= monthStartDate && c.DisbursalDate <= monthEndDate);
                    totalLoanPortfolio += monthLoans;

                    dashboard.ApplicationGraphData.Add(monthApplications);
                    dashboard.LoanPortfolioGraphData.Add(totalLoanPortfolio);
                    date = date.AddMonths(1);
                    i += 1;
                }

                return dashboard;
            }
            catch (Exception)
            {
                throw new Exception("Errors occurred while getting dashboard.");
            }
        }
    }
}
