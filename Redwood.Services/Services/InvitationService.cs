﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Models;
using Redwood.Core.Utils;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.Services
{
    internal class InvitationService : ServiceBase,IInvitationService
    {
        public InvitationService(IEntityToModelFactory modelFactory, IModelToEntityFactory entityFactory) : base(modelFactory, entityFactory)
        {
        }

        public bool OrganizationInvitationExists(string organizationName)
        {
            try
            {
                var invite =
                    Uow.InvitationRepository.Get()
                        .FirstOrDefault(c => c.OrganizationName.Equals(organizationName, StringComparison.OrdinalIgnoreCase));

                return invite != null;
            }
            catch (Exception)
            {
                
                throw new Exception("Errors occurred while checking if organization invite exists.");
            }
        }

        public void CreateInvite(InvitationModel model)
        {
            try
            {
                var invitation = EntityFactory.Create(model);
                Uow.InvitationRepository.InsertOrUpdate(invitation);
                Uow.Save();
            }
            catch (Exception ex)
            {
                throw new Exception("Errors occurred while generating invitation.");
            }
        }
    }
}
