﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Models;
using Redwood.Core.Utils;
using Redwood.Data.Context;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.Services
{
    internal class ProductService : ServiceBase, IProductService
    {
        public ProductService(IEntityToModelFactory modelFactory, IModelToEntityFactory entityFactory) 
            : base(modelFactory, entityFactory)
        {
        }

        public List<Product> Get(int organizationId)
        {
            try
            {
                return
                    Uow.ProductRepository.Get(c => c.OrganizationId == organizationId,
                        c => c.OrderBy(cc => cc.Name)).ToList();
            }
            catch (Exception)
            {
                
                throw new Exception("Errors occurred while getting products.");
            }
        }

        public List<Product> GetAll()
        {
            try
            {
                return
                    Uow.ProductRepository.Get(orderBy: c => c.OrderBy(cc => cc.Name).ThenBy(cc => cc.OrganizationId))
                        .ToList();
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while getting products.");
            }
        }

        public Product GetById(int id, int organizationId)
        {
            try
            {
                return Uow.ProductRepository.Get(c => c.Id == id && c.OrganizationId == organizationId).FirstOrDefault();
            }
            catch (Exception)
            {
                
                throw new Exception("Errors occurred while getting product.");
            }
        }

        public Product GetByName(string name, int organizationId)
        {
            try
            {
                var product =
                    Uow.ProductRepository.Get()
                        .FirstOrDefault(
                            c =>
                                c.Name.Equals(name, StringComparison.OrdinalIgnoreCase) &&
                                c.OrganizationId == organizationId);
                return product;
            }
            catch (Exception)
            {
                throw new Exception("Errors occurred while getting organization.");
            }
        }

        public bool ProductExists(int id, int organizationId)
        {
            return GetById(id, organizationId) != null;
        }

        public bool ProductExists(string name, int organizationId)
        {
            return GetByName(name, organizationId) != null;
        }

        public OperationResponse InsertOrUpdate(ProductModel productModel)
        {
            try
            {
                //Ensure unique name
                if (productModel.Id == 0)
                {
                    if (ProductExists(productModel.Name, productModel.OrganizationId))
                        return ComposeResponse(new Exception("A product with a similar name exists."));
                }
                else
                {
                    var similarProduct = GetByName(productModel.Name, productModel.OrganizationId);
                    if (similarProduct!=null)
                        if (similarProduct.Id != productModel.Id)
                            return ComposeResponse(new Exception("A product with a similar name exists."));
                    
                }
                
                //Save product
                var product = EntityFactory.Create(productModel);
                Uow.ProductRepository.InsertOrUpdate(product);
                Uow.Save();

                //Save fees
                SaveProductFees(product);

                return new OperationResponse()
                {
                    HasSucceeded = true,
                    ReturnObject = ModelFactory.Create(product)
                };
            }
            catch (Exception ex)
            {
                return ComposeResponse(ex);
            }
        }

        private void SaveProductFees(Product product)
        {
            try
            {
                //Delete any deleted ones
                var originalFees = Uow.ProductFeeRepository.Get(c => c.ProductId == product.Id).ToList();

                foreach (var originalFee in originalFees)
                {
                    if (!product.ProductFees.Exists(c => c.Id == originalFee.Id))
                    {
                        Uow.ProductFeeRepository.Delete(originalFee);
                    }
                }

                //Insert or update
                foreach (var productFee in product.ProductFees)
                {
                    if (productFee.Id==0)
                    {
                        productFee.ProductId = product.Id;
                    }

                    Uow.ProductFeeRepository.InsertOrUpdate(productFee);
                }
                
                Uow.Save();
            }
            catch (Exception)
            {
                throw new Exception("Errors occured while saving product fees.");
            }
        }
    }
}
