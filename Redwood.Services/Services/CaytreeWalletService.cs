﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.Services
{
    internal class CaytreeWalletService : ServiceBase, ICaytreeWalletService
    {
#if DEBUG

        private const string BaseUrl = "http://localhost:10629/";
#else
        private const string BaseUrl = "https://ctwalletapi.azurewebsites.net/";
#endif

        public CaytreeWalletService(IEntityToModelFactory modelFactory, IModelToEntityFactory entityFactory)
            : base(modelFactory, entityFactory)
        {
        }


        public bool WalletExists(string walletPhoneNumber)
        {
            try
            {
                var url = BaseUrl + "api/wallet/getByWalletPhoneNumber/?walletPhoneNumber=" + walletPhoneNumber;
                var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                httpRequest.Method = "GET";
                httpRequest.ContentType = "application/json";
                httpRequest.GetResponse(); //This throws an exception if the response is 400
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool PostTransferTransaction(string customerWalletPhoneNumber,decimal amount)
        {
            try
            {
                const string url = BaseUrl + "api/wallet/saveTransaction";
                var httpRequest = (HttpWebRequest)WebRequest.Create(url);

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/json";

                var organizationWallet = Uow.OrganizationWalletRepository.Get().First();

                var streamWriter = new StreamWriter(httpRequest.GetRequestStream());
                var json = new JavaScriptSerializer().Serialize(new
                {
                    walletPhoneNumber = organizationWallet.Username,
                    destination = customerWalletPhoneNumber,
                    amount = amount,
                    notes = "Loan disbursal.",
                    transactionType = "Transfer",
                    transactionDate = DateTime.Now,
                    transactionStatus = "Pending"

                });
                
                streamWriter.Write(json);
                streamWriter.Dispose();

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var result = streamReader.ReadToEnd();

                var transaction = JObject.Parse(result);

                if (transaction == null)
                    return false;

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
