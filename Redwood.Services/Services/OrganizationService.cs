﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Models;
using Redwood.Core.Utils;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.Services
{
    internal class OrganizationService : ServiceBase,IOrganizationService
    {
        public OrganizationService(IEntityToModelFactory modelFactory, IModelToEntityFactory entityFactory) 
            : base(modelFactory, entityFactory)
        {
        }

        public Organization GetById(int id)
        {
            try
            {
                var organization = Uow.OrganizationRepository.GetById(id);
                return organization;
            }
            catch (Exception ex)
            {
                throw new Exception("Errors occurred while getting organization.");
            }
        }

        public Organization GetByName(string name)
        {
            try
            {

                var organization =
                    Uow.OrganizationRepository.Get()
                        .FirstOrDefault(c => c.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
                return organization;
            }
            catch (Exception)
            {
                throw new Exception("Errors occurred while getting organization.");
            }
        }

        public bool OrganizationExists(int id)
        {
            return GetById(id) != null;
        }

        public bool OrganizationExists(string organizationName)
        {
            return GetByName(organizationName) != null;
        }

        public OperationResponse InsertOrUpdate(OrganizationModel organizationModel)
        {
            try
            {
                var organization = EntityFactory.Create(organizationModel);
                Uow.OrganizationRepository.InsertOrUpdate(organization);
                Uow.Save();

                return new OperationResponse()
                {
                    HasSucceeded = true,
                    ReturnObject = ModelFactory.Create(organization)
                };

            }
            catch (Exception ex)
            {
                return ComposeResponse(ex);
            }
        }

        public OperationResponse UpdateLogo(int organizationId, byte[] image)
        {
            try
            {
                var response = new OperationResponse();

                var organization = Uow.OrganizationRepository.GetById(organizationId);
                organization.Logo = image;
                Uow.OrganizationRepository.InsertOrUpdate(organization);
                Uow.Save();

                response.HasSucceeded = true;
                response.Message = "Success";
                response.ReturnObject = ModelFactory.Create(organization);

                return response;

            }
            catch (Exception ex)
            {
                return ComposeResponse(ex);
            }
        }

        public OperationResponse DeleteLogo(int organizationId)
        {
            try
            {
                var response = new OperationResponse();

                var organization = Uow.OrganizationRepository.GetById(organizationId);
                organization.Logo = null;
                Uow.OrganizationRepository.InsertOrUpdate(organization);
                Uow.Save();

                response.HasSucceeded = true;
                response.Message = "Task completed successfully";
                response.ReturnObject = ModelFactory.Create(organization);

                return response;

            }
            catch (Exception ex)
            {
                return ComposeResponse(ex);
            }
        }

    }
}
