﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Models;
using Redwood.Services.CRBInfoAPI;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.Services
{
    internal class CrbService : ServiceBase,ICrbService
    {
        public CrbService(IEntityToModelFactory modelFactory, IModelToEntityFactory entityFactory) : base(modelFactory, entityFactory)
        {
        }

        public List<CrbLoanModel> GetUserActiveLoans(string idNo)
        {
            try
            {
                var reportSections = new[] { "ContractDetail" };
                var parameters = new CustomReportParams
                {
                    Consent = true,
                    IDNumber = idNo,
                    IDNumberType = IDNumberTypeResolvable.NationalID,
                    InquiryReason = InquiryReasons.ApplicationForCreditOrAmendmentOfCreditTerms,
                    SubjectType = SubjectType.Individual,
                    Sections = reportSections
                };


                var response = CrbClient.GetCustomReport(parameters);

                var crbLoans = new List<CrbLoanModel>();
                foreach (var contract in response.Contracts.ContractList)
                {
                    if (contract.AccountStatus != ContractStatus.Closed)
                    {
                        var crbLoan = new CrbLoanModel()
                        {
                            Type = contract.AccountProductType.ToString(),
                            Status = contract.AccountStatus.ToString(),
                            OutstandingAmount = contract.OutstandingAmount.LocalValue,
                            OverdueBalance = contract.OverdueBalance.LocalValue,
                            DaysInArrears = contract.DaysInArrears,
                            StartDate = contract.DateAccountOpened,
                            OriginalAmount = contract.OriginalAmount.LocalValue,
                            InstallmentAmount = contract.InstallmentAmount.LocalValue
                        };
                        crbLoans.Add(crbLoan);
                    }

                }

                return crbLoans;
            }
            catch (Exception)
            {
                
                return new List<CrbLoanModel>();
            }
        }

        public List<CrbScoreModel> GetUserCrbScores(string idNo)
        {
            try
            {
                var reportSections = new[] { "CIP" };
                var parameters = new CustomReportParams
                {
                    Consent = true,
                    IDNumber = idNo,
                    IDNumberType = IDNumberTypeResolvable.NationalID,
                    InquiryReason = InquiryReasons.ApplicationForCreditOrAmendmentOfCreditTerms,
                    SubjectType = SubjectType.Individual,
                    Sections = reportSections
                };


                var response = CrbClient.GetCustomReport(parameters);

                var crbScores = new List<CrbScoreModel>();
                foreach (var crbScore in response.CIP.RecordList)
                {
                    var crbLoan = new CrbScoreModel()
                    {
                        Date = crbScore.Date,
                        Grade = crbScore.Grade.ToString(),
                        ProbabilityOfDefault = crbScore.ProbabilityOfDefault,
                        Score = crbScore.Score
                    };
                    crbScores.Add(crbLoan);

                }

                return crbScores;
            }
            catch (Exception)
            {
                return new List<CrbScoreModel>();
            }
        }
    }
}
