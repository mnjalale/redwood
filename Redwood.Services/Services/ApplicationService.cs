﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Enums;
using Redwood.Core.Enums.Caytree;
using Redwood.Core.Models;
using Redwood.Core.Models.Caytree;
using Redwood.Core.Utils;
using Redwood.Core.Utils.Helpers;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.Services
{
    internal class ApplicationService : ServiceBase, IApplicationService
    {
        public ApplicationService(IEntityToModelFactory modelFactory, IModelToEntityFactory entityFactory) : base(modelFactory, entityFactory)
        {
        }

        public List<LoanApplicationModel> Get(int organizationId)
        {
            try
            {
                var applications = Uow.LoanApplicationRepository.Get(c => c.OrganizationId == organizationId,
                    c => c.OrderBy(cc => cc.DateCreated)).ToList().Select(ModelFactory.Create).ToList();

                foreach (var application in applications)
                {
                    application.AmortizationSchedule =
                        GetAmortizationSchedule(application.Amount, application.RepaymentPeriod,
                            application.InterestRate).Select(ModelFactory.Create).ToList();
                    if (string.IsNullOrWhiteSpace(application.CrbScore))
                    {
                        application.CrbScore =
                            application.Appraisals.FirstOrDefault(c => c.Area == AppraisalArea.CrbScore) == null
                                ? ""
                                : application.Appraisals.FirstOrDefault(c => c.Area == AppraisalArea.CrbScore).Score;
                    }
                }


                return applications;
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while getting loan applications.");
            }
        }

        public List<LoanApplicationModel> Get(int organizationId,ApplicationStatus status)
        {
            try
            {
                if (status == ApplicationStatus.All)
                {
                    return Get(organizationId);
                }
                var applications =
                    Uow.LoanApplicationRepository.Get(c => c.OrganizationId == organizationId && c.Status == status,
                        c => c.OrderBy(cc => cc.DateCreated)).ToList().Select(ModelFactory.Create).ToList();

                foreach (var application in applications)
                {
                    application.AmortizationSchedule =
                        GetAmortizationSchedule(application.Amount, application.RepaymentPeriod,
                            application.InterestRate).Select(ModelFactory.Create).ToList();
                }
                
                return applications;
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while getting loan applications.");
            }
        }

        public List<LoanApplicationModel> GetApplicationsForCaytreeUser(int caytreeCompanyId)
        {
            try
            {
                var customer = Uow.CustomerRepository.Get(c => c.CaytreeCompanyId == caytreeCompanyId).FirstOrDefault();
                if (customer==null) return new List<LoanApplicationModel>();

                var applications = Uow.LoanApplicationRepository.Get(c => c.CustomerId == customer.Id,
                    c => c.OrderByDescending(cc => cc.ApplicationDate)).ToList().Select(ModelFactory.Create).ToList();

                foreach (var application in applications)
                {
                    application.AmortizationSchedule =
                        GetAmortizationSchedule(application.Amount, application.RepaymentPeriod,
                            application.InterestRate).Select(ModelFactory.Create).ToList();
                }
                    
                return applications;
            }
            catch (Exception)
            {
                throw new Exception("Errors occurred while getting applications.");
            }
        }

        public LoanApplicationModel GetById(int id, int organizationId)
        {
            try
            {
                var application = Uow.LoanApplicationRepository.Get(c => c.Id == id && c.OrganizationId == organizationId).FirstOrDefault();

                if (application == null) return null;

                var model = ModelFactory.Create(application);

                model.AmortizationSchedule =
                    GetAmortizationSchedule(model.Amount, model.RepaymentPeriod, model.InterestRate)
                        .Select(ModelFactory.Create)
                        .ToList();

                if (string.IsNullOrWhiteSpace(application.CrbScore))
                {
                    model.CrbScore =
                        model.Appraisals.FirstOrDefault(c => c.Area == AppraisalArea.CrbScore) == null
                            ? ""
                            : application.Appraisals.FirstOrDefault(c => c.Area == AppraisalArea.CrbScore).Score;
                }

                return model;
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while getting loan application.");
            }
        }

        public OperationResponse InsertOrUpdate(LoanApplicationModel application)
        {
            try
            {
                //Get the product
                var product = Uow.ProductRepository.GetById(application.ProductId);
                application.Product = ModelFactory.Create(product);
                application.InterestRate = application.InterestRate == 0
                    ? product.InterestRate
                    : application.InterestRate;
                application.RepaymentPeriod = application.RepaymentPeriod == 0
                    ? product.MaximumRepaymentPeriod
                    : application.RepaymentPeriod;

                var message = string.Empty;

                //Perform validation
                if (!ValidateApplication(application,product,ref message))
                {
                    return ComposeResponse(new Exception(message));
                }

                //Perform appraisal
                var applicationAppraisals = new List<LoanApplicationAppraisal>();
                if (!PassedRejectRules(ref application, ref message,ref applicationAppraisals))
                {
                    return ComposeResponse(new Exception(message));
                }
                
                application.InterestRate = application.InterestRate == 0
                    ? product.InterestRate
                    : application.InterestRate;
                application.RepaymentPeriod = application.RepaymentPeriod == 0
                    ? product.MaximumRepaymentPeriod
                    : application.RepaymentPeriod;

                //Save the customer
                Customer customer;
                if (application.Customer.CaytreeCompanyId > 0)
                {
                    customer =
                        Uow.CustomerRepository.Get()
                            .FirstOrDefault(c => c.CaytreeCompanyId == application.Customer.CaytreeCompanyId);
                }
                else
                {
                    customer =
                        Uow.CustomerRepository.Get(
                            c => c.IdNo == application.Customer.IdNo && c.OrganizationId == product.OrganizationId)
                            .FirstOrDefault();
                }
                var customerId = customer == null ? 0 : customer.Id;

                if (customerId==0)
                {
                    customer = EntityFactory.Create(application.Customer);
                    customer.OrganizationId = product.OrganizationId;
                }
                else
                {
                    application.Customer.Id = customerId;
                    application.Customer.OrganizationId = product.OrganizationId;

                    customer = EntityFactory.Create(application.Customer);
                }  

                Uow.CustomerRepository.InsertOrUpdate(customer);
                Uow.Save();

                //Save the application
                var loanApplication = EntityFactory.Create(application);
                loanApplication.CustomerId = customer.Id;
                loanApplication.OrganizationId = customer.OrganizationId;

                //load appraisals
                if (loanApplication.Id<=0)
                {
                    loanApplication.Appraisals = applicationAppraisals;
                }

                //In the event that the approval is automatic, this code will be activated
                //if (loanApplication.Id==0) //This is a new application
                //{
                //    loanApplication.Status = ApplicationStatus.Approved;
                //}
                
                Uow.LoanApplicationRepository.InsertOrUpdate(loanApplication);


                if (application.Status == ApplicationStatus.Disbursed)
                {
                    //Create the loan from the application
                    return CreateLoanFromApplication(application, customer);
                }
                else
                {
                    Uow.Save();

                    foreach (var document in application.Documents)
                    {
                        var documentType = document.DocumentType;
                        LoanApplicationDocument applicationDocument;
                        if (Uow.LoanApplicationDocumentRepository.Get()
                            .Any(c => c.LoanApplicationId == loanApplication.Id && c.DocumentType == documentType))
                        {
                            applicationDocument =
                                Uow.LoanApplicationDocumentRepository.Get()
                                    .First(
                                        c =>
                                            c.LoanApplicationId == loanApplication.Id &&
                                            c.DocumentType == documentType);
                            applicationDocument.ImageGuid = document.ImageGuid;
                            applicationDocument.ImageName = document.ImageName;
                        }
                        else
                        {
                            document.LoanApplicationId = loanApplication.Id;
                            applicationDocument = EntityFactory.Create(document);
                        }

                        Uow.LoanApplicationDocumentRepository.InsertOrUpdate(applicationDocument);
                    }
                    Uow.Save();

                    var loanApplicationModel = ModelFactory.Create(loanApplication);
                    loanApplicationModel.AmortizationSchedule =
                        GetAmortizationSchedule(loanApplicationModel.Amount, loanApplication.RepaymentPeriod,
                            loanApplication.InterestRate).Select(ModelFactory.Create).ToList();
                    return new OperationResponse()
                    {
                        HasSucceeded = true,
                        ReturnObject = loanApplicationModel
                    };
                }

                
                
            }
            catch (Exception ex)
            {
                return ComposeResponse(ex);
            }
        }

        public bool ValidateApplication(LoanApplicationModel application,Product product,ref string message)
        {
            try
            {
                //Check amount
                if (application.Amount > product.MaximumAmount)
                {
                    message = "The maximum amount for this loan is " + product.MaximumAmount.ToString("#,##0,00");
                    return false;
                }

                //Check length
                if (application.RepaymentPeriod > product.MaximumRepaymentPeriod)
                {
                    message = "The maximum repayment period for this loan is " + product.MaximumRepaymentPeriod + " months";
                    return false;
                }

                //Check wallet number
                if (application.Status == ApplicationStatus.Pending
                    && string.IsNullOrWhiteSpace(application.Customer.CaytreeWalletPhoneNumber))
                {
                    message = "The Caytree Wallet ID/phone number is required.";
                    return false;
                }
                else
                {
                    //Check if it is a valid wallet
                    var walletService = Resolve<ICaytreeWalletService>();
                    if (application.Customer!=null)
                    {
                        if (!walletService.WalletExists(application.Customer.CaytreeWalletPhoneNumber))
                        {
                            message = "The Caytree Wallet provided is invalid.";
                            return false;
                        }
                    }
                    
                }

                return true;
            }
            catch (Exception)
            {
                message = "Unable to validate application.";
                return false;
            }
        }

        private LoanApplication ComputeAppraisalOld(LoanApplication application)
        {
            try
            {
                #region Check reject rules
                //Step 1: Check reject rules
                //-> Check that the applicant is more than 18 years
                //-> Check if the applicant has a previous default
                //-> Check how long the applicant has been tracking
                //-> Check the number of transactions

                if (Utility.GetAgeInYears(application.Customer.DateOfBirth)<18)
                {
                    
                }

                

                #endregion


                //Delete any previous appraisals if any
                var currentAppraisals =
                    Uow.LoanApplicationAppraisalRepository.Get(c => c.ApplicationId == application.Id).ToList();

                if (currentAppraisals.Any())
                {
                    foreach (var currentAppraisal in currentAppraisals)
                    {
                        Uow.LoanApplicationAppraisalRepository.Delete(currentAppraisal);
                    }
                    Uow.Save();
                }

                var customer = Uow.CustomerRepository.GetById(application.CustomerId);

                var appraisals = new List<LoanApplicationAppraisal>();

                //1) Debt to income ratio
                var appraisal = new LoanApplicationAppraisal()
                {
                    Type = AppraisalType.Risk,
                    Weight = 40,
                    Area = AppraisalArea.DebtToIncomeRatio,
                    Calculation =
                        (application.Amount / application.RepaymentPeriod) /
                        (customer.BusinessIncome + customer.GrossMonthlySalary + customer.OtherBusinessMonthlyIncome)
                };
                appraisal.RatingCode = appraisal.Calculation <= 10
                    ? "A"
                    : appraisal.Calculation > 10 && appraisal.Calculation < 20 ? "B" : "C";
                appraisal.StrengthText = appraisal.Calculation <= 10
                    ? "Expected monthly repayment amount is less than 10% of borrower's reported monthly income"
                    : "";
                appraisal.WeaknessText = appraisal.Calculation >= 20
                    ? "Expected monthly repayment amount may exceed 20% of borrower's monthly income making borrower susceptible to potential repayment shocks"
                    : "";

                appraisals.Add(appraisal);

                //2) Employment length
                appraisal = new LoanApplicationAppraisal()
                {
                    Type = AppraisalType.Risk,
                    Weight = 30,
                    Area = AppraisalArea.EmploymentLength,
                    Calculation = DateTimeSpan.CompareDates(customer.EmploymentStartDate == null ? DateTime.Now : (DateTime)customer.EmploymentStartDate, DateTime.Now).Months
                };
                appraisal.RatingCode = appraisal.Calculation >= 36
                    ? "A"
                    : appraisal.Calculation > 12 && appraisal.Calculation < 36 ? "B" : "C";
                appraisal.StrengthText = appraisal.Calculation >= 36
                    ? "Borrower appears to have stable employment history, subject to verification"
                    : "";
                appraisal.WeaknessText = appraisal.Calculation <= 12
                    ? "Borrower has short employment history indicating potential instability"
                    : "";

                appraisals.Add(appraisal);

                //3) 

                //System Usage

                //Return
                application.Appraisals = appraisals;
                return application;


            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while computing appraisal.");
            }
        }

        private bool PassedRejectRules(ref LoanApplicationModel application, ref string message, ref List<LoanApplicationAppraisal> appraisals)
        {
            try
            {
                var crbService = Resolve<ICrbService>();

                if (application.Customer == null)
                {
                    application.Customer = ModelFactory.Create(Uow.CustomerRepository.GetById(application.CustomerId));
                }

                var crbCustomerLoans = crbService.GetUserActiveLoans(application.Customer.IdNo);
                var customerCrbScores = crbService.GetUserCrbScores(application.Customer.IdNo);
                var customerCrbScore = customerCrbScores.Count > 0 ? customerCrbScores.OrderBy(c => c.Date).First().Grade : "B1";
                appraisals.Add(new LoanApplicationAppraisal()
                {
                    Area = AppraisalArea.CrbScore,
                    Score = customerCrbScore
                });

                application.CrbScore = customerCrbScore;
                var qualificationAmountFromInvoices = GetQualificationAmountFromInvoice(ref application, ref message);
                var invoiceQualificationMessage = message;
                var qualificationAmountFromImportedTransactions =
                    GetQualificationAmountFromImportedTransactions(ref application, ref message);
                var importedTransactionsMessage = message;


                decimal maximumAmount;

                if (qualificationAmountFromInvoices > qualificationAmountFromImportedTransactions)
                {
                    maximumAmount = qualificationAmountFromInvoices;
                    application.ApprovalBasis = "Invoice Transactions";
                }
                else
                {
                    maximumAmount = qualificationAmountFromImportedTransactions;
                    application.ApprovalBasis = "Imported Transactions";
                }
                application.MaximumQualificationAmount = maximumAmount;
                if (maximumAmount == 0)
                {
                    message = string.IsNullOrWhiteSpace(invoiceQualificationMessage)
                        ? importedTransactionsMessage
                        : invoiceQualificationMessage;// "You do not qualify for this loan.";
                    return false;
                }

                if (maximumAmount<application.Amount)
                {
                    if (maximumAmount < application.Product.MinimumAmount)
                    {
                        message = "You do not qualify for this loan.";
                        return false;
                    }
                    message = "The maximum loan amount you qualify for is " + maximumAmount.ToString("#,##0.00");
                    return false;
                }

                #region Process Reject Rules

                //1) Check age
                if (Utility.GetAgeInYears(application.Customer.DateOfBirth) < 18)
                {
                    message = "You need to be at least 18 years old to qualify for this loan.";
                    return false;
                }

                //2) Check default on CRB
                if (crbCustomerLoans.Any(c => c.DaysInArrears > 0))
                {
                    message = "You do not qualify because you have defaulted on some of your previous loans.";
                    return false;
                }

                
                #endregion

                return true;

                //#region Cash Balance Coverage Parameters

                //var cashAndCashEquivalents =
                //    Resolve<ICaytreeService>()
                //        .GetCaytreeCashAndCashEquivalentAccounts(application.Customer.CaytreeCompanyId)
                //        .Sum(c => c.Balance);

                //#endregion

                //return ComputeAppraisal(application.Amount, application.RepaymentPeriod, application.InterestRate,
                //    application.Product.MinimumAmount,
                //    cashAndCashEquivalents, customerCrbScore, ref message, ref appraisals);
            }
            catch (Exception)
            {
                return false;
            }
        }


        private decimal GetQualificationAmountFromInvoice(ref LoanApplicationModel application, ref string message)
        {
            try
            {
                var customer = application.Customer ??
                               ModelFactory.Create(Uow.CustomerRepository.GetById(application.CustomerId));

                if (customer==null)
                {
                    message = "Customer was not found.";
                    return 0;
                }

                var caytreeInvoices =
                    Resolve<ICaytreeService>().GetCaytreeInvoices(customer.CaytreeCompanyId);

                application.InvoiceTransactionsCount = caytreeInvoices.Count;
                application.TotalOutstandingInvoices = caytreeInvoices.Sum(c => c.OutstandingAmount);

                if (caytreeInvoices.Count == 0)
                {
                    message =
                        "You do not have any outstanding invoices. You therefore do not qualify for this loan.";
                    return 0M;
                }

                var acceptedInvoices =
                    caytreeInvoices.Where(
                        c =>
                            (c.InvoiceStatus == InvoiceStatus.Accepted || c.InvoiceStatus == InvoiceStatus.Verified ||
                            c.InvoiceStatus == InvoiceStatus.Trusted) || c.InvoiceIsVerified).ToList();

                application.VerifiedTrustedInvoiceCount = acceptedInvoices.Count;
                application.TotalOutstandingTrustedVerifiedInvoices = acceptedInvoices.Sum(c => c.OutstandingAmount);

                if (acceptedInvoices.Count==0)
                {
                    message =
                        "You do not have any outstanding invoices that have been accepted by the supplier. You therefore do not qualify for this loan.";
                    return 0M;
                }


                var verifiedAndTrustedInvoices =
                    acceptedInvoices.Where(
                        c =>
                            c.Supplier != null &&
                            (c.Supplier.Status == CaytreeCompanyStatus.Verified ||
                             c.Supplier.Status == CaytreeCompanyStatus.Trusted)).ToList();

                if (verifiedAndTrustedInvoices.Count==0)
                {
                    message =
                        "You do not have any outstanding invoices sent to verified/trusted suppliers. You therefore do not qualify for this loan.";
                    return 0M;
                }

                var qualifiedAmount = 0M;
                foreach (var invoice in verifiedAndTrustedInvoices)
                {
                    qualifiedAmount += invoice.Supplier.ApprovedLimit*invoice.OutstandingAmount;
                }

                message = string.Empty;
                return qualifiedAmount;

            }
            catch (Exception)
            {
                message = "Errors occurred while getting qualification amount from invoices.";
                throw new Exception("Errors occurred while getting qualification amount from invoices.");
            }
        }

        public void GetQualificationAmountFromInvoice(int caytreeCompanyId, ref decimal qualificationAmount,
            ref decimal potentialQualificationAmount)
        {
            try
            {
                var caytreeInvoices =
                    Resolve<ICaytreeService>().GetCaytreeInvoices(caytreeCompanyId);

                if (caytreeInvoices.Count == 0)
                {
                    qualificationAmount = 0;
                    potentialQualificationAmount = 0;
                }

                var acceptedInvoices =
                    caytreeInvoices.Where(
                        c =>
                            (c.InvoiceStatus == InvoiceStatus.Accepted || c.InvoiceStatus == InvoiceStatus.Verified ||
                            c.InvoiceStatus == InvoiceStatus.Trusted) || c.InvoiceIsVerified).ToList();

                if (acceptedInvoices.Count == 0)
                {
                    qualificationAmount = 0;
                }


                var verifiedAndTrustedInvoices =
                    acceptedInvoices.Where(
                        c =>
                            c.Supplier != null &&
                            (c.Supplier.Status == CaytreeCompanyStatus.Verified ||
                             c.Supplier.Status == CaytreeCompanyStatus.Trusted)).ToList();

                if (verifiedAndTrustedInvoices.Count == 0)
                {
                    qualificationAmount = 0;
                }

                foreach (var invoice in verifiedAndTrustedInvoices)
                {
                    qualificationAmount += invoice.Supplier.ApprovedLimit * invoice.OutstandingAmount;
                    
                }

                foreach (var invoice in caytreeInvoices)
                {
                    if (invoice.Supplier != null && (invoice.Supplier.Status == CaytreeCompanyStatus.Verified ||
                             invoice.Supplier.Status == CaytreeCompanyStatus.Trusted))
                    {
                        potentialQualificationAmount += invoice.Supplier.ApprovedLimit * invoice.OutstandingAmount;
                    }
                    else
                    {
                        potentialQualificationAmount += invoice.OutstandingAmount;
                    }
                   
                }
                
            }
            catch (Exception)
            {
                throw new Exception("Errors occurred while computing qualification and potential qualification amount.");
            }
        }
        
        private decimal GetQualificationAmountFromImportedTransactions(ref LoanApplicationModel application, ref string message)
        {
            try
            {
                var customer = application.Customer ?? ModelFactory.Create(Uow.CustomerRepository.GetById(application.CustomerId));

                if (customer==null)
                {
                    message = "Unable to get the customer";
                    return 0M;

                }

                var caytreeImportedTransactions =
                    Resolve<ICaytreeService>().GetCaytreeImportedTransactions(customer.CaytreeCompanyId);

                if (caytreeImportedTransactions.Count==0)
                {
                    message = "You do not have any imported transactions. You do not qualify for this loan.";
                    return 0M;
                }

                var startDate = caytreeImportedTransactions.OrderBy(c => c.TransactionDate).First().TransactionDate;

                var months = DateTime.Today.Subtract(startDate).Days/(365.2425/12);
                application.NumberOfMonthsForImportedTransactions = Convert.ToDecimal(Math.Round(months, 1));

                var inflowTransactionsCount =
                    caytreeImportedTransactions.Count(
                        c => c.ImportedTransactionType == CaytreeImportedTransactionType.KopoKopo) +
                    caytreeImportedTransactions.Count(
                        c => c.ImportedTransactionType == CaytreeImportedTransactionType.MPesa && c.Category == "income");

                application.AverageNumberOfInflowsPerMonth =
                    Convert.ToDecimal(Math.Round(inflowTransactionsCount/months, 2));

                var totalInflows =
                    caytreeImportedTransactions.Where(
                        c => c.ImportedTransactionType == CaytreeImportedTransactionType.KopoKopo).Sum(c => c.Amount) +
                    caytreeImportedTransactions.Where(
                        c => c.ImportedTransactionType == CaytreeImportedTransactionType.MPesa && c.Category == "income")
                        .Sum(c => c.Amount);


                var totalOutflows =
                    caytreeImportedTransactions.Where(
                        c =>
                            c.ImportedTransactionType == CaytreeImportedTransactionType.MPesa && c.Category == "expense")
                        .Sum(c => c.Amount);

                var totalBalance = totalInflows - totalOutflows;

                

                if (totalBalance < 0)
                {
                    message =
                        "You do not qualify for this loan because you have a negative balance for your imported transactions.";
                    return 0M;
                }

                var averageMonthlyBalance = totalBalance/(decimal) months;
                application.AverageNumberOfInflowsPerMonth = averageMonthlyBalance;

                if (inflowTransactionsCount <= 15 || totalBalance <=30000)
                {
                    if (inflowTransactionsCount < 15)
                        message = "You do not qualify for this loan because you do not have enough inflow transactions.";
                    else
                        message =
                            "You do not qualify for this loan because your imported transactions balance is too low.";

                    return 0;
                }

                var qualifiedAmount = (30/100)*averageMonthlyBalance;
                return qualifiedAmount;

            }
            catch (Exception)
            {
                message = "Errors occurred while getting qualification amount from imported transactions.";
                return 0;
               
                throw new Exception("Errors occurred while getting qualification amount from imported transactions.");
            }
        }

        private bool PassedRejectRulesOld(LoanApplicationModel application, ref string message, ref List<LoanApplicationAppraisal> appraisals)
        {
            try
            {
                var crbService = Resolve<ICrbService>();
                var transactionMonths = 8;

                if (application.Customer == null)
                {
                    application.Customer = ModelFactory.Create(Uow.CustomerRepository.GetById(application.CustomerId));
                }

                var crbCustomerLoans = crbService.GetUserActiveLoans(application.Customer.IdNo);
                var feneraCustomerLoans =
                    Resolve<ILoanService>().GetLoansForCaytreeUser(application.Customer.CaytreeCompanyId);
                var customerCrbScores = crbService.GetUserCrbScores(application.Customer.IdNo);
                var customerCrbScore = customerCrbScores.Count > 0 ? customerCrbScores.OrderBy(c => c.Date).First().Grade : "B1";
                appraisals.Add(new LoanApplicationAppraisal()
                {
                    Area = AppraisalArea.CrbScore,
                    Score = customerCrbScore
                });

                var caytreeTransactions =
                    Resolve<ICaytreeService>()
                        .GetCaytreeTransactions(application.Customer.CaytreeCompanyId, transactionMonths)
                        .OrderBy(c => c.Date)
                        .ToList();

                #region Process Reject Rules

                //1) Check age
                if (Utility.GetAgeInYears(application.Customer.DateOfBirth) < 18)
                {
                    message = "You need to be at least 18 years old to qualify for this loan.";
                    return false;
                }

                //2) Check default on CRB
                if (crbCustomerLoans.Any(c => c.DaysInArrears > 0))
                {
                    message = "You do not qualify because you have defaulted on some of your previous loans.";
                    return false;
                }

                //3) Less than two weeks tracking on CF
                if (!caytreeTransactions.Any())
                {
                    message =
                        "You haven't tracked anything on Caytree Financial. Please start tracking to qualify for a loan.";
                    return false;
                }
                if (caytreeTransactions.First().Date > DateTime.Today.AddDays(-14))
                {
                    message = "You need to track your finances with Caytree Financial for at least two weeks to qualify for a loan.";
                    return false;
                }

                //4) Less than 10 transactions in CF
                if (caytreeTransactions.Count < 10)
                {
                    message = "You need to have tracked more than 10 transactions in Caytree Financial to qualify for a loan.";
                    return false;
                }

                #endregion

                #region Debt-Income Ratio Parameters

                var currentDate = DateTime.Today;
                var lastMonth = currentDate.AddMonths(-1);

                var lastMonthStartDate = new DateTime(lastMonth.Year, lastMonth.Month, 1);
                var lastMonthEndDate = (new DateTime(currentDate.Year, currentDate.Month, 1)).AddDays(-1);

                var applicantMonthlyIncome =
                    caytreeTransactions.Where(
                        c =>
                            (c.TransactionType == "Income" || c.TransactionType == "Invoice") &&
                            (c.Date >= lastMonthStartDate || c.Date <= lastMonthEndDate)).Sum(c => c.Amount);

                //Compute monthly installments from CRB and Fenera
                var applicantCurrentTotalMonthlyInstallments = crbCustomerLoans.Sum(c => c.InstallmentAmount);

                foreach (var feneraLoan in feneraCustomerLoans)
                {
                    applicantCurrentTotalMonthlyInstallments += feneraLoan.AmortizationSchedule.First().Repayment;
                }

                #endregion

                #region Cash Balance Coverage Parameters

                var cashAndCashEquivalents =
                    Resolve<ICaytreeService>()
                        .GetCaytreeCashAndCashEquivalentAccounts(application.Customer.CaytreeCompanyId)
                        .Sum(c => c.Balance);
                var totalCurrentDebt = crbCustomerLoans.Sum(c => c.OutstandingAmount);

                #endregion

                #region Caytree Confidence Score

                var caytreeConfidenceScore = ComputeCaytreeConfidenceScore(caytreeTransactions, transactionMonths,
                    applicantMonthlyIncome, application.Customer.GrossMonthlySalary);
                appraisals.Add(new LoanApplicationAppraisal()
                {
                    Area = AppraisalArea.CaytreeConfidenceScore,
                    Score = caytreeConfidenceScore.ToString("#,##0.00")
                });

                #endregion


                return ComputeAppraisal(application.Amount, application.RepaymentPeriod, application.InterestRate,
                    application.Product.MinimumAmount, applicantMonthlyIncome, applicantCurrentTotalMonthlyInstallments,
                    cashAndCashEquivalents, customerCrbScore, caytreeConfidenceScore, ref message, ref appraisals);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool ComputeAppraisal(decimal amount,int repaymentPeriod, decimal interestRate,decimal minimumLoanAmount, decimal applicantMonthlyIncome,
            decimal applicantCurrentTotalMonthlyLoanRepayments,decimal cashAndCashEquivalents,string customerCrbScore,decimal caytreeConfidenceScore, ref string message,
            ref List<LoanApplicationAppraisal> applicationAppraisals)
        {
            try
            {
                var loanAmortizationSchedule = GetAmortizationSchedule(amount, repaymentPeriod, interestRate);
                var monthlyRepayment = loanAmortizationSchedule.First().Repayment;
                var totalMonthlyInstallments = monthlyRepayment + applicantCurrentTotalMonthlyLoanRepayments;
                var totalLoanRepaymentAmount = loanAmortizationSchedule.Sum(c => c.Repayment);


                if (applicantMonthlyIncome <= 0)
                {
                    message = "You do not have any recorded income. You don't qualify for the loan.";
                    return false;
                }
                
                var debtToIncomeRatio = (totalMonthlyInstallments / applicantMonthlyIncome) * 100;
                applicationAppraisals.Add(new LoanApplicationAppraisal()
                {
                    Area = AppraisalArea.DebtToIncomeRatio,
                    Score = debtToIncomeRatio.ToString("#,##0.00")
                });

                //var cashBalanceCoverage = cashAndCashEquivalents / totalCurrentDebt;

                var cashBalanceCoverage = 2M;
                if (monthlyRepayment > 0)
                {
                    cashBalanceCoverage = cashAndCashEquivalents / monthlyRepayment;
                }
                applicationAppraisals.Add(new LoanApplicationAppraisal()
                {
                    Area = AppraisalArea.CashBalanceCoverage,
                    Score = cashBalanceCoverage.ToString("#,##0.00")
                });
               
                var riskMatrix = RiskMatrix.High;
                var affordabilityMatrix = AffordabilityMatrix.Low;
                string[] lowRiskScores = { "A1", "A2", "A3" };
                string[] mediumRiskScores = { "B1", "B2", "B3" };
                string[] highRiskScores = { "C1", "C2", "C3", "D1", "D2", "D3", "E1", "E2", "E3" };

                //Compute affordability metrics
                if (debtToIncomeRatio > 20 || cashBalanceCoverage < 1)
                {
                    affordabilityMatrix = AffordabilityMatrix.Low;
                }
                else if ((debtToIncomeRatio > 10 && debtToIncomeRatio <= 20) ||
                         (cashBalanceCoverage >= 1 && cashBalanceCoverage < 2))
                {
                    affordabilityMatrix = AffordabilityMatrix.Medium;
                }
                else if (debtToIncomeRatio <= 10 && cashBalanceCoverage >= 2)
                {
                    affordabilityMatrix = AffordabilityMatrix.High;
                }

                //Compute risk metrics
                if (debtToIncomeRatio > 20 || highRiskScores.Contains(customerCrbScore) || caytreeConfidenceScore<=1)
                {
                    if (debtToIncomeRatio > 20)
                    {
                        message = "You do not qualify for this loan because you have a high debt to income ratio of " +
                                  debtToIncomeRatio.ToString("##,###.##") + "%";
                    }
                    else if (highRiskScores.Contains(customerCrbScore))
                    {
                        message = "You do not qualify for this loan because you have a poor CRB score of " + customerCrbScore;
                    }
                    else if (caytreeConfidenceScore <= 1)
                    {
                        message = "You do not qualify for this loan because you have a poor Caytree Score of " +
                                  caytreeConfidenceScore.ToString("##,###.##");
                    }

                    riskMatrix = RiskMatrix.High;
                }
                else if (debtToIncomeRatio > 10 && debtToIncomeRatio <= 20 || mediumRiskScores.Contains(customerCrbScore) || (caytreeConfidenceScore>1 && caytreeConfidenceScore<=2))
                {
                    riskMatrix = RiskMatrix.Medium;
                }
                else if (debtToIncomeRatio <= 10 || lowRiskScores.Contains(customerCrbScore) || caytreeConfidenceScore>2)
                {
                    riskMatrix = RiskMatrix.Low;
                }

                if (riskMatrix == RiskMatrix.High)
                {
                    return false;
                }


                if ((riskMatrix == RiskMatrix.Medium && affordabilityMatrix == AffordabilityMatrix.Low) ||
                    (riskMatrix == RiskMatrix.Medium && affordabilityMatrix == AffordabilityMatrix.Medium) ||
                    (riskMatrix == RiskMatrix.Low && affordabilityMatrix == AffordabilityMatrix.Low))
                {
                    var reason = "";
                    var proposedAmount = GetProposedLoanAmount(monthlyRepayment, repaymentPeriod, interestRate,
                        affordabilityMatrix, riskMatrix, debtToIncomeRatio, totalMonthlyInstallments,
                        applicantMonthlyIncome, cashBalanceCoverage, cashAndCashEquivalents,
                        totalLoanRepaymentAmount,ref reason);

                    if (proposedAmount >= minimumLoanAmount)
                    {
                        message = "You do not qualify for this amount. The maximum amount you qualify for is " +
                                  proposedAmount.ToString("#,##0.00");
                    }
                    else
                    {
                        message = "You do not qualify for this loan. " + reason;
                    }

                    return false;
                }

                if ((riskMatrix == RiskMatrix.Medium && affordabilityMatrix == AffordabilityMatrix.High) ||
                    (riskMatrix == RiskMatrix.Low && affordabilityMatrix == AffordabilityMatrix.Medium) ||
                    (riskMatrix == RiskMatrix.Low && affordabilityMatrix == AffordabilityMatrix.High))
                {
                    message = "You have passed the appraisal process";
                    return true;
                }

                return true;
            }
            catch (Exception)
            {
                message = "Errors occurred while performing appraisal";
                return false;
            }
        }
        
        private decimal GetProposedLoanAmount(decimal monthlyRepayment,int repaymentPeriod,decimal interestRate,AffordabilityMatrix affordabilityMatrix,
            RiskMatrix riskMatrix, decimal debtToIncomeRatio,decimal totalMonthlyInstallments, decimal totalIncome,
            decimal cashBalanceCoverage,decimal cashAndCashEquivalents,decimal totalLoanRepaymentAmount,ref string reason)
        {
            
            try
            {
                decimal proposedLoanAmount = 0;
                decimal proposedDebtToIncomeRatio = 0;
                decimal proposedCashBalanceCoverage = 0;
                decimal proposedLoanAmountDir = 0;
                decimal proposedLoanAmountCbc = 0;
                        
                if (affordabilityMatrix==AffordabilityMatrix.Medium)
                {
                    proposedDebtToIncomeRatio = 10;
                    proposedCashBalanceCoverage = 2;

                    if ((debtToIncomeRatio>10 && debtToIncomeRatio<=20) && 
                        (cashBalanceCoverage >=1 && cashBalanceCoverage < 2) )
                    {
                        proposedLoanAmountDir = ComputeProposedLoanAmount(proposedDebtToIncomeRatio, totalIncome,
                            totalMonthlyInstallments, monthlyRepayment, repaymentPeriod, interestRate);
                        proposedLoanAmountCbc = ComputeProposedLoanAmount(proposedCashBalanceCoverage,
                            cashAndCashEquivalents, repaymentPeriod, interestRate);

                        proposedLoanAmount = proposedLoanAmountDir < proposedLoanAmountCbc
                            ? proposedLoanAmountDir
                            : proposedLoanAmountCbc;
                        reason = "Your debt to income ratio is " + debtToIncomeRatio.ToString("##,###.##") +
                                 " and your cash balance coverage is " + cashBalanceCoverage.ToString("##,###.##");
                    }
                    else if (debtToIncomeRatio > 10 && debtToIncomeRatio <= 20)
                    {
                        proposedLoanAmount = ComputeProposedLoanAmount(proposedDebtToIncomeRatio, totalIncome,
                            totalMonthlyInstallments,monthlyRepayment,repaymentPeriod,interestRate);

                        reason = "Your debt to income ratio is " + debtToIncomeRatio.ToString("##,###.##");

                    }
                    else if (cashBalanceCoverage >=1 && cashBalanceCoverage < 2)
                    {
                        proposedLoanAmount = ComputeProposedLoanAmount(proposedCashBalanceCoverage,
                            cashAndCashEquivalents, repaymentPeriod, interestRate);
                        reason = "Your cash balance coverage is " + cashBalanceCoverage.ToString("##,###.##");
                    }
                }
                else if (affordabilityMatrix == AffordabilityMatrix.Low && riskMatrix == RiskMatrix.Low)
                {
                    proposedDebtToIncomeRatio = 20;
                    proposedCashBalanceCoverage = 1;

                    if (debtToIncomeRatio > 20 &&
                        cashBalanceCoverage <= totalLoanRepaymentAmount)
                    {
                        proposedLoanAmountDir = ComputeProposedLoanAmount(proposedDebtToIncomeRatio, totalIncome,
                            totalMonthlyInstallments, monthlyRepayment, repaymentPeriod, interestRate);
                        proposedLoanAmountCbc = ComputeProposedLoanAmount(proposedCashBalanceCoverage,
                            cashAndCashEquivalents, repaymentPeriod, interestRate);

                        proposedLoanAmount = proposedLoanAmountDir < proposedLoanAmountCbc
                            ? proposedLoanAmountDir
                            : proposedLoanAmountCbc;

                        reason = "Your debt to income ratio is " + debtToIncomeRatio.ToString("##,###.##") +
                                 " and your cash balance coverage is " + cashBalanceCoverage.ToString("##,###.##");
                    }
                    else if (debtToIncomeRatio > 20)
                    {
                        proposedLoanAmount = ComputeProposedLoanAmount(proposedDebtToIncomeRatio, totalIncome,
                            totalMonthlyInstallments,monthlyRepayment,repaymentPeriod,interestRate);
                        reason = "Your debt to income ratio is " + debtToIncomeRatio.ToString("##,###.##");

                    }
                    else if (cashBalanceCoverage <= totalLoanRepaymentAmount)
                    {
                        proposedLoanAmount = ComputeProposedLoanAmount(proposedCashBalanceCoverage,
                           cashAndCashEquivalents, repaymentPeriod, interestRate);
                        reason = "Your cash balance coverage is " + cashBalanceCoverage.ToString("##,###.##");
                    }
                }
                else if (affordabilityMatrix == AffordabilityMatrix.Low && riskMatrix == RiskMatrix.Medium)
                {
                    proposedDebtToIncomeRatio = 10;
                    proposedCashBalanceCoverage = 2;

                    if (debtToIncomeRatio > 20 && cashBalanceCoverage < 1)
                    {
                        proposedLoanAmountDir = ComputeProposedLoanAmount(proposedDebtToIncomeRatio, totalIncome,
                            totalMonthlyInstallments, monthlyRepayment, repaymentPeriod, interestRate);
                        proposedLoanAmountCbc = ComputeProposedLoanAmount(proposedCashBalanceCoverage,
                            cashAndCashEquivalents, repaymentPeriod, interestRate);

                        proposedLoanAmount = proposedLoanAmountDir < proposedLoanAmountCbc
                            ? proposedLoanAmountDir
                            : proposedLoanAmountCbc;
                        reason = "Your debt to income ratio is " + debtToIncomeRatio.ToString("##,###.##") +
                                 " and your cash balance coverage is " + cashBalanceCoverage.ToString("##,###.##");
                    }
                    else if (debtToIncomeRatio > 20)
                    {
                        proposedLoanAmount = ComputeProposedLoanAmount(proposedDebtToIncomeRatio, totalIncome,
                            totalMonthlyInstallments, monthlyRepayment, repaymentPeriod,interestRate);
                        reason = "Your debt to income ratio is " + debtToIncomeRatio.ToString("##,###.##");

                    }
                    else if (cashBalanceCoverage < 1)
                    {
                        proposedLoanAmount = ComputeProposedLoanAmount(proposedCashBalanceCoverage,
                           cashAndCashEquivalents, repaymentPeriod, interestRate);
                        reason = "Your cash balance coverage is " + cashBalanceCoverage.ToString("##,###.##");
                    }
                }

                return proposedLoanAmount;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private decimal ComputeProposedLoanAmount(decimal proposedDebtToIncomeRatio,decimal totalIncome,
            decimal totalMonthlyInstallments, decimal monthlyRepayment,int repaymentPeriod,decimal interestRate)
        {
            try
            {
                var newTotalMonthlyInstallments = (totalIncome * proposedDebtToIncomeRatio) / 100;
                var differenceInInstallments = totalMonthlyInstallments - newTotalMonthlyInstallments;
                var newLoanMonthlyInstallment = monthlyRepayment - differenceInInstallments;
                decimal newPrincipal = 0;

                if (newLoanMonthlyInstallment > 0)
                {
                    newPrincipal = GetPrincipal(newLoanMonthlyInstallment, repaymentPeriod, interestRate);
                }

                return newPrincipal;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private decimal ComputeProposedLoanAmount(decimal proposedCashBalanceCoverage, decimal cashAndCashEquivalents,
            int repaymentPeriod, decimal interestRate)
        {
            try
            {
                var newMonthlyInstallment = cashAndCashEquivalents/proposedCashBalanceCoverage;
                decimal newPrincipal = 0;

                if (newMonthlyInstallment > 0)
                {
                    newPrincipal = GetPrincipal(newMonthlyInstallment, repaymentPeriod, interestRate);
                }

                return newPrincipal;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets caytree score based on transactions for the last N months
        /// </summary>
        /// <param name="transactions">Transactions for the last 6 months</param>
        /// <param name="numberOfMonths">Number of months the transactions cover</param>
        /// <returns></returns>
        public decimal ComputeCaytreeConfidenceScore(List<CaytreeTransaction> transactions,
            int numberOfMonths,decimal caytreeMonthlyIncome, decimal applicationMonthlyIncome)
        {
            try
            {
                if (transactions.Count == 0) return 0;
                var score = 0M;
                var scorePercentage = 0M;
                var today = DateTime.Today;
                
                //Score based on how long the user has been using Caytree
                var earliestDate = transactions.OrderBy(c => c.Date).First().Date;

                if (earliestDate >= today.AddMonths(-1))
                {
                    score += 1;
                }
                else if (earliestDate >= today.AddMonths(-6)
                          && earliestDate < today.AddMonths(-1))
                {
                    score += 2;
                }
                else if (earliestDate < today.AddMonths(-6))
                {
                    score += 3;
                }

                scorePercentage += (score/3M)*25;

                //Score based on usage frequency
                score = 0;
                var usageFrequency = transactions.Count/numberOfMonths;
                if (usageFrequency<=10)
                {
                    score += 1;
                }
                else if (usageFrequency > 10 && usageFrequency <= 30)
                {
                    score += 2;
                }
                else if (usageFrequency > 30)
                {
                    score += 3;
                }

                scorePercentage += (score/3M)*25;

                //Score based on financial completeness
                score = 0;
                var ratio = (caytreeMonthlyIncome/applicationMonthlyIncome)*100;

                if (ratio<=50)
                {
                    score += 1;
                }
                else if (ratio > 50 && ratio <= 90)
                {
                    score += 2;
                }
                else if (ratio > 90)
                {
                    score += 3;
                }


                scorePercentage += (score/3)*50;

                return (scorePercentage/100)*3;

            }
            catch (Exception)
            {
                return 0;
            }
        }

        public OperationResponse RejectApplication(RejectApplicationModel rejection)
        {
            try
            {
                //Update application status
                var application = Uow.LoanApplicationRepository.GetById(rejection.ApplicationId);
                application.Status = ApplicationStatus.Rejected;
                Uow.LoanApplicationRepository.InsertOrUpdate(application);

                //Insert rejection reasons
                #region Insert rejections

                var notes = new List<LoanApplicationNote>();
                if (rejection.ApplicationIncomplete)
                    notes.Add(new LoanApplicationNote()
                    {
                        ApplicationNoteType = ApplicationNoteType.RejectionReason,
                        LoanApplicationId = rejection.ApplicationId,
                        Note = "Application was not fully completed."
                    });

                if (rejection.LoanAmountNotQualified)
                    notes.Add(new LoanApplicationNote()
                    {
                        ApplicationNoteType = ApplicationNoteType.RejectionReason,
                        LoanApplicationId = rejection.ApplicationId,
                        Note = "Loan amount did not fulfil the qualification range."
                    });

                if (rejection.IncompleteDocuments)
                    notes.Add(new LoanApplicationNote()
                    {
                        ApplicationNoteType = ApplicationNoteType.RejectionReason,
                        LoanApplicationId = rejection.ApplicationId,
                        Note = "Not all documents were submitted."
                    });

                if (!string.IsNullOrWhiteSpace(rejection.OtherReasons))
                    notes.Add(new LoanApplicationNote()
                    {
                        ApplicationNoteType = ApplicationNoteType.RejectionReason,
                        LoanApplicationId = rejection.ApplicationId,
                        Note = rejection.OtherReasons
                    });

                if (!string.IsNullOrWhiteSpace(rejection.Recommendations))
                    notes.Add(new LoanApplicationNote()
                    {
                        ApplicationNoteType = ApplicationNoteType.Recommendation,
                        LoanApplicationId = rejection.ApplicationId,
                        Note = rejection.Recommendations
                    });

                foreach (var note in notes)
                {
                    Uow.LoanApplicationNoteRepository.InsertOrUpdate(note);
                }

                #endregion

                Uow.Save();

                application = Uow.LoanApplicationRepository.GetById(rejection.ApplicationId);
                return new OperationResponse()
                {
                    HasSucceeded = true,
                    ReturnObject = ModelFactory.Create(application)
                };
            }
            catch (Exception ex)
            {
                return ComposeResponse(ex);
            }
        }

        public OperationResponse CancelApplication(LoanApplicationModel model)
        {
            try
            {
                //Update application status
                var application = Uow.LoanApplicationRepository.GetById(model.Id);
                application.Status = ApplicationStatus.CancelledByUser;
                Uow.LoanApplicationRepository.InsertOrUpdate(application);
                
                Uow.Save();

                return new OperationResponse()
                {
                    HasSucceeded = true,
                    ReturnObject = ModelFactory.Create(application)
                };
            }
            catch (Exception ex)
            {
                return ComposeResponse(ex);
            }
        }

        private OperationResponse CreateLoanFromApplication(LoanApplicationModel application,Customer customer)
        {
            try
            {
                //Check that the organization has defined a wallet, and that the wallet is active
                if (!Uow.OrganizationWalletRepository.Get().Any())
                {
                    return
                        ComposeResponse(new Exception("You have not paired a Caytree Wallet. You must pair a wallet before disbursing any loans."));
                }

                var loan = new Loan()
                {
                    CustomerId = application.CustomerId,
                    ProductId = application.ProductId,
                    ApplicationId = application.Id,
                    Amount = application.Amount,
                    OrganizationId = application.OrganizationId,
                    RepaymentPeriod = application.RepaymentPeriod,
                    DisbursalDate = DateTime.Today,
                    MaturityDate = DateTime.Today.AddMonths(application.RepaymentPeriod),
                    AmortizationSchedule = GetAmortizationSchedule(application.Amount,application.RepaymentPeriod,application.InterestRate)
                };

                Uow.LoanRepository.InsertOrUpdate(loan);


                //Transfer money from the organization's wallet to the applicant's wallet
                var walletService = Resolve<ICaytreeWalletService>();

                if (walletService.PostTransferTransaction(customer.CaytreeWalletPhoneNumber, loan.Amount))
                {
                    Uow.Save();

                    return new OperationResponse()
                    {
                        HasSucceeded = true,
                        ReturnObject = ModelFactory.Create(loan)
                    };
                }
                else
                {
                    return
                        ComposeResponse(
                            new Exception("Unable to disburse the loan to " + customer.FirstName + " " +
                                          customer.LastName));
                }

                
            }
            catch (Exception ex)
            {
                return ComposeResponse(ex);
            }
        }

        private List<LoanAmortizationSchedule> GetAmortizationSchedule(decimal amount,int repaymentPeriod,decimal interestRate)
        {
            try
            {
                var principle = amount;
                var period = repaymentPeriod;
                var intRate = interestRate/100;

                var monthlyRepayment = Math.Round(GetMonthlyInstalment(principle, period, intRate));

                var armotizationSchedule = new List<LoanAmortizationSchedule>();

                for (int i = 0; i < period; i++)
                {
                    var interest = Math.Round(intRate * principle);

                    var armotization = new LoanAmortizationSchedule()
                    {
                        Period = i + 1,
                        Principal = principle,
                        Interest = interest,
                        Repayment = monthlyRepayment,
                        Balance = principle - monthlyRepayment-interest,
                    };
                    armotizationSchedule.Add(armotization);
                    principle -= (monthlyRepayment - interest);
                }

                return armotizationSchedule;

            }
            catch (Exception ex)
            {
                return new List<LoanAmortizationSchedule>();
            }
        }

        private decimal GetMonthlyInstalment(decimal principal, decimal period, decimal interestRate)
        {
            try
            {
                var result = interestRate * principal /
                             (decimal)(1 - Math.Pow((double)(1 + interestRate), (double)-period));
                return result;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        private decimal GetPrincipal(decimal monthlyInstallment, decimal period, decimal interestRate)
        {
            try
            {
                var interestR = interestRate/100M;
                var result = (monthlyInstallment*(decimal) (1 - Math.Pow((double) (1 + interestR), (double) -period)))/
                             interestR;
                return Math.Round(result);

            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
