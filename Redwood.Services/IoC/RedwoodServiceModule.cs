﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using Redwood.Services.Factories;
using Redwood.Services.Services;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.IoC
{
    public class RedwoodServiceModule : NinjectModule
    {
        public override void Load()
        {
            //Factories
            Bind<IEntityToModelFactory>().To<EntityToModelFactory>();
            Bind<IModelToEntityFactory>().To<ModelToEntityFactory>();

            //Services
            Bind<IApplicationService>().To<ApplicationService>();
            Bind<ICaytreeService>().To<CaytreeService>();
            Bind<ICaytreeWalletService>().To<CaytreeWalletService>();
            Bind<ICrbService>().To<CrbService>();
            Bind<IHomeService>().To<HomeService>();
            Bind<IInvitationService>().To<InvitationService>();
            Bind<ILoanService>().To<LoanService>();
            Bind<IOrganizationService>().To<OrganizationService>();
            Bind<IOrganizationWalletService>().To<OrganizationWalletService>();
            Bind<IProductService>().To<ProductService>(); 
        }
    }
}
