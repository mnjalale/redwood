﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Enums;
using Redwood.Core.Models;
using Redwood.ServicesContracts.IFactories;

namespace Redwood.Services.Factories
{
    internal class EntityToModelFactory : IEntityToModelFactory
    {
        public CustomerModel Create(Customer customer)
        {
            if (customer == null) return null;

            return new CustomerModel()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                MiddleName = customer.MiddleName,
                LastName = customer.LastName,
                BusinessIncome = customer.BusinessIncome,
                City = customer.City,
                CompanyLegalStatus = customer.CompanyLegalStatus,
                CompanyName = customer.CompanyName,
                County = customer.County,
                DateOfBirth = customer.DateOfBirth,
                Email = customer.Email,
                EmploymentStartDate = customer.EmploymentStartDate,
                EmploymentStatus = customer.EmploymentStatus,
                Gender = customer.Gender,
                GrossMonthlySalary = customer.GrossMonthlySalary,
                IdNo = customer.IdNo,
                OtherBusiness = customer.OtherBusiness,
                OtherBusinessMonthlyIncome = customer.OtherBusinessMonthlyIncome,
                PhoneNumber = customer.PhoneNumber,
                PhysicalAddress = customer.PhysicalAddress,
                PostalAddress = customer.PostalAddress,
                OrganizationId = customer.OrganizationId,
                MaritalStatus = customer.MaritalStatus,
                EducationLevel = customer.EducationLevel,
                BankName = customer.BankName,
                BankAccountNo = customer.BankAccountNo,
                BankBranch = customer.BankBranch,
                MobilePaymentPhoneNumber = customer.MobilePaymentPhoneNumber,
                CaytreeCompanyId = customer.CaytreeCompanyId,
                CaytreeWalletPhoneNumber = customer.CaytreeWalletPhoneNumber
            };
        }

        public InvitationModel Create(Invitation invitation)
        {
            if (invitation == null) return null;

            return new InvitationModel()
            {
                Id = invitation.Id,
                OrganizationName = invitation.OrganizationName,
                FirstName = invitation.FirstName,
                LastName = invitation.LastName,
                Email = invitation.Email,
                PhoneNumber = invitation.PhoneNumber,
                Processed = invitation.Processed
            };
        }

        public LoanAmortizationScheduleModel Create(LoanAmortizationSchedule loanAmortizationSchedule)
        {
            if (loanAmortizationSchedule == null) return null;

            return new LoanAmortizationScheduleModel()
            {
                Id = loanAmortizationSchedule.Id,
                LoanId = loanAmortizationSchedule.LoanId,
                Period = loanAmortizationSchedule.Period,
                Principal = loanAmortizationSchedule.Principal,
                Interest = loanAmortizationSchedule.Interest,
                Repayment = loanAmortizationSchedule.Repayment,
                Balance = loanAmortizationSchedule.Balance,
                InterestPaid = loanAmortizationSchedule.InterestPaid,
                PrincipalPaid = loanAmortizationSchedule.PrincipalPaid
            };
        }

        public LoanApplicationModel Create(LoanApplication loanApplication)
        {
            if (loanApplication == null) return null;

            return new LoanApplicationModel()
            {
                Id = loanApplication.Id,
                CustomerId = loanApplication.CustomerId,
                ProductId = loanApplication.ProductId,
                OrganizationId = loanApplication.OrganizationId,
                Amount = loanApplication.Amount,
                ApplicationDate = loanApplication.ApplicationDate,
                RepaymentPeriod = loanApplication.RepaymentPeriod,
                Status = loanApplication.Status,
                InterestRate = loanApplication.InterestRate,
                Customer = Create(loanApplication.Customer),
                Product = Create(loanApplication.Product),
                MaximumQualificationAmount = loanApplication.MaximumQualificationAmount,
                ApprovalBasis = loanApplication.ApprovalBasis,
                InvoiceTransactionsCount = loanApplication.InvoiceTransactionsCount,
                VerifiedTrustedInvoiceCount = loanApplication.VerifiedTrustedInvoiceCount,
                TotalOutstandingInvoices = loanApplication.TotalOutstandingInvoices,
                TotalOutstandingTrustedVerifiedInvoices = loanApplication.TotalOutstandingTrustedVerifiedInvoices,
                NumberOfMonthsForImportedTransactions = loanApplication.NumberOfMonthsForImportedTransactions,
                AverageNumberOfInflowsPerMonth = loanApplication.AverageNumberOfInflowsPerMonth,
                CrbScore = loanApplication.CrbScore,
                AverageNetInflows = loanApplication.AverageNetInflows,
                Notes = loanApplication.Notes.Select(Create).ToList(),
                Documents = loanApplication.Documents.Select(Create).ToList(),
                Appraisals = loanApplication.Appraisals.Select(Create).ToList()
            };
        }

        public LoanApplicationDocumentModel Create(LoanApplicationDocument loanApplicationDocument)
        {
            if (loanApplicationDocument == null) return null;
            return new LoanApplicationDocumentModel()
            {
                Id = loanApplicationDocument.Id,
                LoanApplicationId = loanApplicationDocument.LoanApplicationId,
                DocumentType = loanApplicationDocument.DocumentType,
                ImageGuid = loanApplicationDocument.ImageGuid,
                ImageName = loanApplicationDocument.ImageName,
                UploadDate = loanApplicationDocument.LastUpdated
            };
        }

        public LoanApplicationNoteModel Create(LoanApplicationNote loanApplicationNote)
        {
            if (loanApplicationNote == null) return null;

            return new LoanApplicationNoteModel()
            {
                Id = loanApplicationNote.Id,
                LoanApplicationId = loanApplicationNote.LoanApplicationId,
                ApplicationNoteType = loanApplicationNote.ApplicationNoteType,
                Note = loanApplicationNote.Note
            };
        }

        public LoanApplicationAppraisalModel Create(LoanApplicationAppraisal loanApplicationAppraisal)
        {
            if (loanApplicationAppraisal == null) return null;
            return new LoanApplicationAppraisalModel()
            {
                Id = loanApplicationAppraisal.Id,
                ApplicationId = loanApplicationAppraisal.ApplicationId,
                Area = loanApplicationAppraisal.Area,
                Type = loanApplicationAppraisal.Type,
                Weight = loanApplicationAppraisal.Weight,
                Calculation = loanApplicationAppraisal.Calculation,
                RatingCode = loanApplicationAppraisal.RatingCode,
                Rating = loanApplicationAppraisal.Rating,
                StrengthText = loanApplicationAppraisal.StrengthText,
                WeaknessText = loanApplicationAppraisal.WeaknessText,
                Score = loanApplicationAppraisal.Score
            };
        }

        public LoanModel Create(Loan loan)
        {
            if (loan == null) return null;

            return new LoanModel()
            {
                Id = loan.Id,
                CustomerId = loan.CustomerId,
                ProductId = loan.ProductId,
                ApplicationId = loan.ApplicationId,
                OrganizationId = loan.OrganizationId,
                Amount = loan.Amount,
                DisbursalDate = loan.DisbursalDate,
                MaturityDate = loan.MaturityDate,
                RepaymentPeriod = loan.RepaymentPeriod,
                LoanStatus = loan.LoanStatus,
                Customer = Create(loan.Customer),
                Product = Create(loan.Product),
                LoanPayments = loan.LoanPayments.Select(Create).ToList(),
                AmortizationSchedule = loan.AmortizationSchedule.Select(Create).ToList()
                //Application = Create(loan.Application)
            };
        }

        public LoanPaymentModel Create(LoanPayment loanPayment)
        {
            if (loanPayment == null) return null;

            return new LoanPaymentModel()
            {
                Id = loanPayment.Id,
                LoanId = loanPayment.LoanId,
                Amount = loanPayment.Amount,
                Date = loanPayment.Date,
                PaymentMethod = loanPayment.PaymentMethod,
                Reference = loanPayment.Reference,
                //Loan = Create(loanPayment.Loan)
            };
        }

        public OrganizationModel Create(Organization organization)
        {
            if (organization == null) return null;

            return new OrganizationModel()
            {
                Id = organization.Id,
                Name = organization.Name,
                Email = organization.Email,
                PhoneNumber = organization.PhoneNumber,
                PostalAddress = organization.PostalAddress,
                Website = organization.Website,
                ContactPersonName = organization.ContactPersonName,
                ContactPersonEmail = organization.ContactPersonEmail,
                ContactPersonPhone = organization.ContactPersonPhone,
                PrivacyPolicyLink = organization.PrivacyPolicyLink,
                TermsLink = organization.TermsLink,
                City = organization.City,
                Country = organization.Country,
                Logo = organization.Logo,
                BrandColorBg = organization.BrandColorBg,
                BrandColorText = organization.BrandColorText
                  
            };
        }

        public OrganizationWalletModel Create(OrganizationWallet organizationWallet)
        {
            if (organizationWallet == null) return null;

            return new OrganizationWalletModel()
            {
                Id = organizationWallet.Id,
                OrganizationId = organizationWallet.OrganizationId,
                Username = organizationWallet.Username,
                Password = organizationWallet.Password
            };
        }

        public ProductModel Create(Product product)
        {
            if (product == null) return null;

            return new ProductModel()
            {
                Id = product.Id,
                CategoryId = product.CategoryId,
                OrganizationId = product.OrganizationId,
                Name = product.Name,
                Description = product.Description,
                Visibility = product.Visibility,
                StartDate = product.StartDate,
                EndDate = product.EndDate,
                MinimumAmount = product.MinimumAmount,
                MaximumAmount = product.MaximumAmount,
                InterestRate = product.InterestRate,
                InterestPeriod = product.InterestPeriod,
                Published = product.Published,
                MinimumRepaymentPeriod = product.MinimumRepaymentPeriod,
                MaximumRepaymentPeriod = product.MaximumRepaymentPeriod,
                RepaymentStartDays = product.RepaymentStartDays,
                MinimumPercentageOfMonthlyIncome = product.MinimumPercentageOfMonthlyIncome,
                Employed = product.Employed,
                MinimumPeriodOfEmployment = product.MinimumPeriodOfEmployment,
                MinimumEmploymentPeriod = product.MinimumEmploymentPeriod,
                CrbHistory = product.CrbHistory,
                CrbHistoryDetails = product.CrbHistoryDetails,
                Collateral = product.Collateral,
                CollateralPercentage = product.CollateralPercentage,
                PersonalNationalId = product.PersonalNationalId,
                PersonalKraPin = product.PersonalKraPin,
                PersonalBankStatements = product.PersonalBankStatements,
                PersonalBankStatementMonths = product.PersonalBankStatementMonths,
                BusinessOwnerNationalId = product.BusinessOwnerNationalId,
                BusinessOwnerKraPin = product.BusinessOwnerKraPin,
                DirectorNationalId = product.DirectorNationalId,
                DirectorKraPin = product.DirectorKraPin,
                CompanyRegistration = product.CompanyRegistration,
                CompanyBusinessPermit = product.CompanyBusinessPermit,
                CompanyPin = product.CompanyPin,
                FinancialCollateral = product.FinancialCollateral,
                FinancialBankStatements = product.FinancialBankStatements,
                FinancialBankStatementMonths = product.FinancialBankStatementMonths,
                FinancialStatements = product.FinancialStatements,
                FinancialCrbReport = product.FinancialCrbReport,
                ProductFees = product.ProductFees.Select(Create).ToList()
            };
        }

        public ProductCategoryModel Create(ProductCategory productCategory)
        {
            if (productCategory == null) return null;

            return new ProductCategoryModel()
            {
                Id = productCategory.Id,
                Name = productCategory.Name,
                Description = productCategory.Description,
                Products = productCategory.Products.Select(Create).ToList()
            };
        }

        public ProductFeeModel Create(ProductFee productFee)
        {
            if (productFee == null) return null;

            return new ProductFeeModel()
            {
                Id = productFee.Id,
                ProductId = productFee.ProductId,
                Name = productFee.Name,
                Amount = productFee.Amount,
                Computation = productFee.Computation,
                Recovery = productFee.Recovery
            };
        }
    }
}
