﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Enums;
using Redwood.Core.Models;
using Redwood.ServicesContracts.IFactories;

namespace Redwood.Services.Factories
{
    internal class ModelToEntityFactory : IModelToEntityFactory
    {
        public Customer Create(CustomerModel customerModel)
        {
            if (customerModel == null) return null;
            
            return new Customer()
            {
                Id = customerModel.Id,
                FirstName = customerModel.FirstName,
                MiddleName = customerModel.MiddleName,
                LastName = customerModel.LastName,
                BusinessIncome = customerModel.BusinessIncome,
                City = customerModel.City,
                CompanyLegalStatus = customerModel.CompanyLegalStatus,
                CompanyName = customerModel.CompanyName,
                County = customerModel.County,
                DateOfBirth = customerModel.DateOfBirth,
                Email = customerModel.Email,
                EmploymentStartDate = customerModel.EmploymentStartDate,
                EmploymentStatus = customerModel.EmploymentStatus,
                Gender = customerModel.Gender,
                GrossMonthlySalary = customerModel.GrossMonthlySalary,
                IdNo = customerModel.IdNo,
                OtherBusiness = customerModel.OtherBusiness,
                OtherBusinessMonthlyIncome = customerModel.OtherBusinessMonthlyIncome,
                PhoneNumber = customerModel.PhoneNumber,
                PhysicalAddress = customerModel.PhysicalAddress,
                PostalAddress = customerModel.PostalAddress,
                OrganizationId = customerModel.OrganizationId,
                MaritalStatus = customerModel.MaritalStatus,
                EducationLevel = customerModel.EducationLevel,
                BankName = customerModel.BankName,
                BankAccountNo = customerModel.BankAccountNo,
                BankBranch = customerModel.BankBranch,
                MobilePaymentPhoneNumber = customerModel.MobilePaymentPhoneNumber,
                CaytreeCompanyId = customerModel.CaytreeCompanyId,
                CaytreeWalletPhoneNumber = customerModel.CaytreeWalletPhoneNumber
            };
        }

        public Invitation Create(InvitationModel invitationModel)
        {
            if (invitationModel == null) return null;

            return new Invitation()
            {
                Id = invitationModel.Id,
                OrganizationName = invitationModel.OrganizationName,
                FirstName = invitationModel.FirstName,
                LastName = invitationModel.LastName,
                Email = invitationModel.Email,
                PhoneNumber = invitationModel.PhoneNumber,
                Processed = invitationModel.Processed
            };
        }

        public LoanAmortizationSchedule Create(LoanAmortizationScheduleModel loanAmortizationScheduleModel)
        {
            if (loanAmortizationScheduleModel == null) return null;

            return new LoanAmortizationSchedule()
            {
                Id = loanAmortizationScheduleModel.Id,
                LoanId = loanAmortizationScheduleModel.LoanId,
                Period = loanAmortizationScheduleModel.Period,
                Principal = loanAmortizationScheduleModel.Principal,
                Interest = loanAmortizationScheduleModel.Interest,
                Repayment = loanAmortizationScheduleModel.Repayment,
                Balance = loanAmortizationScheduleModel.Balance,
                 InterestPaid = loanAmortizationScheduleModel.InterestPaid,
                PrincipalPaid = loanAmortizationScheduleModel.PrincipalPaid
            };
        }

        public LoanApplication Create(LoanApplicationModel loanApplicationModel)
        {
            if (loanApplicationModel == null) return null;

            return new LoanApplication()
            {
                Id = loanApplicationModel.Id,
                CustomerId = loanApplicationModel.CustomerId,
                ProductId = loanApplicationModel.ProductId,
                OrganizationId = loanApplicationModel.OrganizationId,
                Amount = loanApplicationModel.Amount,
                ApplicationDate = loanApplicationModel.ApplicationDate,
                RepaymentPeriod = loanApplicationModel.RepaymentPeriod,
                Status = loanApplicationModel.Status,
                InterestRate = loanApplicationModel.InterestRate,
                MaximumQualificationAmount = loanApplicationModel.MaximumQualificationAmount,
                ApprovalBasis = loanApplicationModel.ApprovalBasis,
                InvoiceTransactionsCount = loanApplicationModel.InvoiceTransactionsCount,
                VerifiedTrustedInvoiceCount = loanApplicationModel.VerifiedTrustedInvoiceCount,
                TotalOutstandingInvoices = loanApplicationModel.TotalOutstandingInvoices,
                TotalOutstandingTrustedVerifiedInvoices = loanApplicationModel.TotalOutstandingTrustedVerifiedInvoices,
                NumberOfMonthsForImportedTransactions = loanApplicationModel.NumberOfMonthsForImportedTransactions,
                AverageNumberOfInflowsPerMonth = loanApplicationModel.AverageNumberOfInflowsPerMonth,
                CrbScore = loanApplicationModel.CrbScore,
                AverageNetInflows = loanApplicationModel.AverageNetInflows,
                Notes = loanApplicationModel.Notes.Select(Create).ToList(),
                Documents = loanApplicationModel.Documents.Select(Create).ToList(),
                Appraisals = loanApplicationModel.Appraisals.Select(Create).ToList()
            };
        }

        public LoanApplicationDocument Create(LoanApplicationDocumentModel loanApplicationDocument)
        {
            if (loanApplicationDocument == null) return null;
            return new LoanApplicationDocument()
            {
                Id = loanApplicationDocument.Id,
                LoanApplicationId = loanApplicationDocument.LoanApplicationId,
                DocumentType = loanApplicationDocument.DocumentType,
                ImageGuid = loanApplicationDocument.ImageGuid,
                ImageName = loanApplicationDocument.ImageName
            };
        }

        public LoanApplicationNote Create(LoanApplicationNoteModel loanApplicationNoteModel)
        {
            if (loanApplicationNoteModel == null) return null;

            return new LoanApplicationNote()
            {
                Id = loanApplicationNoteModel.Id,
                LoanApplicationId = loanApplicationNoteModel.LoanApplicationId,
                ApplicationNoteType = loanApplicationNoteModel.ApplicationNoteType,
                Note = loanApplicationNoteModel.Note
            };
        }

        public LoanApplicationAppraisal Create(LoanApplicationAppraisalModel loanApplicationAppraisalModel)
        {
            if (loanApplicationAppraisalModel == null) return null;

            return new LoanApplicationAppraisal()
            {
                Id = loanApplicationAppraisalModel.Id,
                ApplicationId = loanApplicationAppraisalModel.ApplicationId,
                Area = loanApplicationAppraisalModel.Area,
                Type = loanApplicationAppraisalModel.Type,
                Weight = loanApplicationAppraisalModel.Weight,
                Calculation = loanApplicationAppraisalModel.Calculation,
                RatingCode = loanApplicationAppraisalModel.RatingCode,
                Rating = loanApplicationAppraisalModel.Rating,
                StrengthText = loanApplicationAppraisalModel.StrengthText,
                WeaknessText = loanApplicationAppraisalModel.WeaknessText,
                Score = loanApplicationAppraisalModel.Score
            };
        }
        
        public Loan Create(LoanModel loanModel)
        {
            if (loanModel == null) return null;

            return new Loan()
            {
                Id = loanModel.Id,
                CustomerId = loanModel.CustomerId,
                ProductId = loanModel.ProductId,
                ApplicationId = loanModel.ApplicationId,
                OrganizationId = loanModel.OrganizationId,
                Amount = loanModel.Amount,
                DisbursalDate = loanModel.DisbursalDate,
                MaturityDate = loanModel.MaturityDate,
                RepaymentPeriod = loanModel.RepaymentPeriod,
                LoanStatus = loanModel.LoanStatus,
                Customer = Create(loanModel.Customer),
                Product = Create(loanModel.Product),
                LoanPayments = loanModel.LoanPayments.Select(Create).ToList()
                //Application = Create(loanModel.Application)
            };
        }

        public LoanPayment Create(LoanPaymentModel loanPaymentModel)
        {
            if (loanPaymentModel == null) return null;
            return new LoanPayment()
            {
                Id = loanPaymentModel.Id,
                LoanId = loanPaymentModel.LoanId,
                Amount = loanPaymentModel.Amount,
                Date = loanPaymentModel.Date,
                PaymentMethod = loanPaymentModel.PaymentMethod,
                Reference = loanPaymentModel.Reference,
                Loan = Create(loanPaymentModel.Loan)
            };
        }

        public Organization Create(OrganizationModel organizationModel)
        {
            if (organizationModel == null) return null;

            return new Organization()
            {
                Id = organizationModel.Id,
                Name = organizationModel.Name,
                Email = organizationModel.Email,
                PhoneNumber = organizationModel.PhoneNumber,
                PostalAddress = organizationModel.PostalAddress,
                Website = organizationModel.Website,
                ContactPersonName = organizationModel.ContactPersonName,
                ContactPersonEmail = organizationModel.ContactPersonEmail,
                ContactPersonPhone = organizationModel.ContactPersonPhone,
                PrivacyPolicyLink = organizationModel.PrivacyPolicyLink,
                TermsLink = organizationModel.TermsLink,
                City = organizationModel.City,
                Country = organizationModel.Country,
                BrandColorBg = organizationModel.BrandColorBg,
                BrandColorText=organizationModel.BrandColorText,
                Logo = organizationModel.Logo
            };
        }

        public OrganizationWallet Create(OrganizationWalletModel organizationWalletModel)
        {
            if (organizationWalletModel == null) return null;

            return new OrganizationWallet()
            {
                Id = organizationWalletModel.Id,
                OrganizationId = organizationWalletModel.OrganizationId,
                Username = organizationWalletModel.Username,
                Password = organizationWalletModel.Password
            };
        }

        public Product Create(ProductModel productModel)
        {
            if (productModel == null) return null;

            return new Product()
            {
                Id = productModel.Id,
                CategoryId = productModel.CategoryId,
                OrganizationId = productModel.OrganizationId,
                Name = productModel.Name,
                Description = productModel.Description,
                Visibility = productModel.Visibility,
                StartDate = productModel.StartDate,
                EndDate = productModel.EndDate,
                MinimumAmount = productModel.MinimumAmount,
                MaximumAmount = productModel.MaximumAmount,
                InterestRate = productModel.InterestRate,
                InterestPeriod = productModel.InterestPeriod,
                Published = productModel.Published,
                MinimumRepaymentPeriod = productModel.MinimumRepaymentPeriod,
                MaximumRepaymentPeriod = productModel.MaximumRepaymentPeriod,
                RepaymentStartDays = productModel.RepaymentStartDays,
                MinimumPercentageOfMonthlyIncome = productModel.MinimumPercentageOfMonthlyIncome,
                Employed = productModel.Employed,
                MinimumPeriodOfEmployment = productModel.MinimumPeriodOfEmployment,
                MinimumEmploymentPeriod = productModel.MinimumEmploymentPeriod,
                CrbHistory = productModel.CrbHistory,
                CrbHistoryDetails = productModel.CrbHistoryDetails,
                Collateral = productModel.Collateral,
                CollateralPercentage = productModel.CollateralPercentage,
                PersonalNationalId = productModel.PersonalNationalId,
                PersonalKraPin = productModel.PersonalKraPin,
                PersonalBankStatements = productModel.PersonalBankStatements,
                PersonalBankStatementMonths = productModel.PersonalBankStatementMonths,
                BusinessOwnerNationalId = productModel.BusinessOwnerNationalId,
                BusinessOwnerKraPin = productModel.BusinessOwnerKraPin,
                DirectorNationalId = productModel.DirectorNationalId,
                DirectorKraPin = productModel.DirectorKraPin,
                CompanyRegistration = productModel.CompanyRegistration,
                CompanyBusinessPermit = productModel.CompanyBusinessPermit,
                CompanyPin = productModel.CompanyPin,
                FinancialCollateral = productModel.FinancialCollateral,
                FinancialBankStatements = productModel.FinancialBankStatements,
                FinancialBankStatementMonths = productModel.FinancialBankStatementMonths,
                FinancialStatements = productModel.FinancialStatements,
                FinancialCrbReport = productModel.FinancialCrbReport,
                ProductFees = productModel.ProductFees.Select(Create).ToList()
            };
        }

        public ProductCategory Create(ProductCategoryModel productCategoryModel)
        {
            if (productCategoryModel == null) return null;

            return new ProductCategory()
            {
                Id = productCategoryModel.Id,
                Name = productCategoryModel.Name,
                Description = productCategoryModel.Description,
                Products = productCategoryModel.Products.Select(Create).ToList()
            };
        }

        public ProductFee Create(ProductFeeModel productFeeModel)
        {
            if (productFeeModel == null) return null;
            return  new ProductFee()
            {
                Id = productFeeModel.Id,
                ProductId = productFeeModel.ProductId,
                Name = productFeeModel.Name,
                Amount = productFeeModel.Amount,
                Computation = productFeeModel.Computation,
                Recovery = productFeeModel.Recovery
            };
        }
    }
}
