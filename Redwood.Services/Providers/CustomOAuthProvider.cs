﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Redwood.Core.Entities;
using Redwood.Services.Infrastructure;
using Redwood.ServicesContracts.IServices;

namespace Redwood.Services.Providers
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            /*This is empty. We are considering the request valid always because
               in our implementation our clients (angular JS frontend app)
             * is a trusted client and we don't need to validate it
             * */
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //var allowedOrigin = "*";

            ////No need for the commented items below
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "Content-Type" });
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Methods", new[] { "GET", "POST", "OPTIONS" });
            //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();

            var user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            if (!user.EmailConfirmed)
            {
                context.SetError("invalid_grant", "User did not confirm email.");
                return;
            }

            var oAuthIdentity = await user.GenerateUserIdentityAsync(userManager, "JWT");
            var props = GetProperties(user);
            var ticket = new AuthenticationTicket(oAuthIdentity, props);
            context.Validated(ticket);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }


            return Task.FromResult<object>(null);
        }

        private AuthenticationProperties GetProperties(User user)
        {

            var organization = IoC.IoC.Resolve<IOrganizationService>().GetById(user.OrganizationId);

            string organizationInfo = JsonConvert.SerializeObject(organization, new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            var props = new AuthenticationProperties(new Dictionary<string, string>
            {
                {
                    "organizationId", organization.Id.ToString()
                },
                {
                    "brandColorBg",organization.BrandColorBg
                },
                {
                    "brandColorText",organization.BrandColorText
                },
                {
                    "userName", user.UserName
                }
            });

            return props;
        }
    }
}
