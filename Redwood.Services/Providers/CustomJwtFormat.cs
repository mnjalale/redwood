﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Redwood.Core.Utils.Settings;
using Thinktecture.IdentityModel.Tokens;

namespace Redwood.Services.Providers
{
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly string _issuer = string.Empty;

        public CustomJwtFormat(string issuer)
        {
            _issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            try
            {
                if (data == null)
                {
                    throw new ArgumentNullException("data");
                }

                var audienceId = SystemSettings.AudienceId;

                var symmetricKeyAsBase64 = SystemSettings.AudienceSecret;

                var keyByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);

                var signingKey = new HmacSigningCredentials(keyByteArray);

                var issued = data.Properties.IssuedUtc;

                var expires = data.Properties.ExpiresUtc;

                var token = new JwtSecurityToken(_issuer, audienceId, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey);

                var handler = new JwtSecurityTokenHandler();

                var jwt = handler.WriteToken(token);

                return jwt;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}
