﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebTestApp._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <script language="JavaScript">
        function onlyNumbers(evt)
        {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;

        }
    </script>
    <div class="jumbotron">
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <table style="width: 700px; float: none; margin: auto">
                <tr>
                    <td colspan="2">
                        <label><i>Appraisal Test</i></label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 450px">
                        Loan Amount
                    </td>
                    <td style="width: 250px">
                        <asp:TextBox ID="txtLoanAmount" runat="server" Width="250px" Height="25px" onkeypress="return onlyNumbers();"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        Repayment Period
                    </td>
                    <td>
                        <asp:TextBox ID="txtPeriod" runat="server" Width="150px" Height="25px" onkeypress="return onlyNumbers();"></asp:TextBox>&nbsp; Months
                    </td>
                </tr>
                 <tr>
                    <td>
                        Monthly Interest
                    </td>
                    <td>
                        <asp:TextBox ID="txtInterestRate" runat="server" Width="150px" Height="25px" onkeypress="return onlyNumbers();"></asp:TextBox>&nbsp; %
                    </td>
                </tr>
                <tr>
                    <td>
                        Minimum Loan Amount
                    </td>
                    <td>
                    
                        <asp:TextBox ID="txtMinimumLoanAmount" runat="server" Width="250px" Height="25px" onkeypress="return onlyNumbers();"></asp:TextBox>
                    
                    </td>
                </tr>
                <tr>
                    <td>
                       Monthly Income
                    </td>
                    <td>
                    
                        <asp:TextBox ID="txtMonthlyIncome" runat="server" Width="250px" Height="25px" onkeypress="return onlyNumbers();"></asp:TextBox>
                    
                    </td>
                </tr>
                <tr>
                    <td>
                        Total Current Monthly Installments
                    </td>
                    <td>
                    
                        <asp:TextBox ID="txtTotalCurrentMonthlyInstallments" runat="server" Width="250px" Height="25px" onkeypress="return onlyNumbers();"></asp:TextBox>
                    
                    </td>
                </tr>
                <tr>
                    <td>
                        Cash and Cash Equivalents
                    </td>
                    <td>
                        <asp:TextBox ID="txtCashAndCashEquivalents" runat="server" Width="250px" Height="25px" onkeypress="return onlyNumbers();"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        CRB Score (A1, A2, A3,....,E1, E2, E3)
                    </td>
                    <td>
                        <asp:TextBox ID="txtCrbScore" runat="server" Width="250px" Height="25px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Caytree Confidence Score (1=Low, 3=High)
                    </td>
                    <td>
                        <asp:TextBox ID="txtCaytreeConfidenceScore" runat="server" Width="250px" Height="25px" onkeypress="return onlyNumbers();"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td colspan="2" class="text-center">
                        <br/>
                    
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        <asp:Button ID="btnPerformAppraisal" runat="server" Text="Perform Appraisal" Width="700px" BackColor="White" OnClick="btnPerformAppraisal_Click"/>
                    
                    </td>
                </tr>
            </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    

</asp:Content>
