﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Redwood.Core.Entities;
using Redwood.Services.IoC;
using Redwood.ServicesContracts.IServices;

namespace WebTestApp
{
    public partial class _Default : Page
    {
        private IApplicationService applicationService;
        private decimal loanAmount;
        private int loanPeriod;
        private decimal interestRate;
        private decimal minimumLoanAmount;
        public decimal monthlyIncome;
        private decimal totalCurrentMonthlyInstallments;
        private decimal cashAndCashEquivalents;
        private decimal caytreeConfidenceScore;
        private string crbScore;

        private string[] crbScores =
        {
            "A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3", "D1", "D2", "D3", "E1", "E2",
            "E3"
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //Set defaults
                txtInterestRate.Text = "5";
            }
        }

        protected void btnPerformAppraisal_Click(object sender, EventArgs e)
        {
            try
            {
                var message = "";
                lblMessage.Text = string.Empty;
                if (!Validate(ref message))
                {
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Text = message;
                    return;
                }
                applicationService = IoC.Resolve<IApplicationService>();

                var appraisals = new List<LoanApplicationAppraisal>();
                var appraisalPassed = applicationService.ComputeAppraisal(loanAmount, loanPeriod, interestRate,
                    minimumLoanAmount, monthlyIncome, totalCurrentMonthlyInstallments, cashAndCashEquivalents,
                    crbScore, caytreeConfidenceScore, ref message,ref appraisals);

                lblMessage.ForeColor = appraisalPassed ? Color.Green : Color.Red;
                lblMessage.Text = message;
            }
            catch (Exception ex)
            {
                lblMessage.ForeColor = Color.Red;
                lblMessage.Text = "Errors have occurred.";
            }
        }

        private bool Validate(ref string message)
        {
            try
            {
                //Assign variables
                loanAmount = txtLoanAmount.Text.Trim().Length > 0 ? Convert.ToDecimal(txtLoanAmount.Text) : 0;
                loanPeriod = txtPeriod.Text.Trim().Length > 0 ? Convert.ToInt32(txtPeriod.Text) : 0;
                interestRate = txtInterestRate.Text.Trim().Length > 0 ? Convert.ToDecimal(txtInterestRate.Text) : 0;
                minimumLoanAmount = txtMinimumLoanAmount.Text.Trim().Length > 0
                    ? Convert.ToDecimal(txtMinimumLoanAmount.Text)
                    : 0;
                monthlyIncome = txtMonthlyIncome.Text.Trim().Length > 0
                    ? Convert.ToDecimal(txtMonthlyIncome.Text)
                    : 0;
                totalCurrentMonthlyInstallments = txtTotalCurrentMonthlyInstallments.Text.Trim().Length > 0
                    ? Convert.ToDecimal(txtTotalCurrentMonthlyInstallments.Text)
                    : 0;
                cashAndCashEquivalents = txtCashAndCashEquivalents.Text.Trim().Length > 0
                    ? Convert.ToDecimal(txtCashAndCashEquivalents.Text)
                    : 0;
                caytreeConfidenceScore = txtCaytreeConfidenceScore.Text.Trim().Length > 0
                    ? Convert.ToDecimal(txtCaytreeConfidenceScore.Text.Trim())
                    : 0;
                crbScore = txtCrbScore.Text;


                //Validate
                if (loanAmount <= 0)
                {
                    message = "Enter valid amount.";
                    return false;
                }

                if (loanPeriod <= 0)
                {
                    message = "Enter valid loan period.";
                    return false;
                }
                if (interestRate <= 0)
                {
                    message = "Enter valid interest rate.";
                    return false;
                }
                if (minimumLoanAmount <= 0)
                {
                    message = "Enter valid minimum loan amount";
                    return false;
                }
                if (!crbScores.Contains(crbScore))
                {
                    message = "Enter valid CRB score.";
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }
    }
}