﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Owin;
using Redwood.Core.Utils.Settings;
using Redwood.Data.Context;
using Redwood.Data.IoC;
using Redwood.Services.Infrastructure;
using Redwood.Services.IoC;
using Redwood.Services.Providers;
using Redwood.WebApi.IoC;
using StructureMap;

namespace Redwood.WebApi
{
    public class Startup
    {

        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);

            var httpConfig = new HttpConfiguration();

            ConfigureOAuthTokenGeneration(app);

            ConfigureOAuthTokenConsumption(app);

            ConfigureWebApi(httpConfig);

            app.UseWebApi(httpConfig);

            MigrateDatabase();

            InitializeIoC();
        }

        private void ConfigureOAuthTokenGeneration(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(RedwoodContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            // Plugin the OAuth bearer JSON Web Token tokens generation and Consumption will be here
            var oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
#if DEBUG
                AllowInsecureHttp = true,
#else
                AllowInsecureHttp = true,
#endif
                TokenEndpointPath = new PathString("/oauth/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                //Provider = new CustomOAuthProvider(),
                Provider = new CustomOAuthProvider(),
#if DEBUG
                AccessTokenFormat = new CustomJwtFormat(SystemSettings.WebApiUrlDevelopment)
#else
               AccessTokenFormat = new CustomJwtFormat(SystemSettings.WebApiUrlProduction)
#endif
            };

            // OAuth 2.0 Bearer access token generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
        }

        private void ConfigureWebApi(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }

        private void ConfigureOAuthTokenConsumption(IAppBuilder app)
        {

#if DEBUG
            var issuer = SystemSettings.WebApiUrlDevelopment;
#else
           var issuer = SystemSettings.WebApiUrlProduction;
#endif
            var audienceId = SystemSettings.AudienceId;
            var audienceSecret = TextEncodings.Base64Url.Decode(SystemSettings.AudienceSecret);

            // Api controllers with an [Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions()
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { audienceId },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                {
                    new SymmetricKeyIssuerSecurityTokenProvider(issuer, audienceSecret)
                }
            });
        }

        private void MigrateDatabase()
        {
            var redwoodConfiguration = new Data.Migrations.Configuration();
            var redwoodMigrator = new DbMigrator(redwoodConfiguration);
            redwoodMigrator.Update();
        }

        private void InitializeIoC()
        {
            IoC.IoC.Init();
        }


    }
}