﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using Ninject.Modules;
using Ninject.Parameters;
using Redwood.Data.IoC;
using Redwood.NotificationBridge.Lib.IoC;
using Redwood.Services.IoC;

namespace Redwood.WebApi.IoC
{
    public class IoC
    {
        static private IKernel _kernel = null;
        static IKernel NinjectKernel
        {
            get { return _kernel ?? (_kernel = new StandardKernel()); }
        }

        public static void Init()
        {
            NinjectKernel.Load(new INinjectModule[] { new RedwoodDataModule(), new RedwoodServiceModule(), new RedWoodNotificationBridgeModule(), });
        }

        public static T Resolve<T>() where T : class
        {
            if (_kernel == null) Init();

            return _kernel.Get<T>();
        }

        public static T Resolve<T>(ConstructorArgument constructorArgument) where T : class
        {
            if (_kernel == null) Init();

            return _kernel.Get<T>(constructorArgument);
        }
    }
}