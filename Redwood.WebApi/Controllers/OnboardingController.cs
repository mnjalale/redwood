﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.AspNet.Identity;
using Redwood.Core.Entities;
using Redwood.Core.Models;
using Redwood.Core.Models.AppUsers;
using Redwood.ServicesContracts.IServices;
using Redwood.WebApi.Controllers.Authentication;

namespace Redwood.WebApi.Controllers
{
    [RoutePrefix("api/onboarding")]
    public class OnboardingController : BaseAuthenticationController
    {
        [AllowAnonymous]
        [Route("createUserAndOrganization")]
        [HttpPost]
        public async Task<IHttpActionResult> CreateUser(CreateUserAndOrganizationModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                    return Content(HttpStatusCode.BadRequest, error);
                }

                var organizationService = Resolve<IOrganizationService>();

                //Validate organization
                var organizationExists = organizationService.OrganizationExists(model.OrganizationName);
                if (organizationExists)
                    return BadRequest("Organization is already registered.");

                //Create user and organization
                //Add user
                var user = new User()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    PhoneNumber = model.PhoneNumber,
                    UserName = model.Username,
                    EmailConfirmed = true
                };

                var addUserResult = await AppUserManager.CreateAsync(user, model.Password);
                if (!addUserResult.Succeeded)
                    return GetErrorResult(addUserResult);

                //Add organization
                var response = organizationService.InsertOrUpdate(new OrganizationModel() { Name = model.OrganizationName });
                if (!response.HasSucceeded)
                {
                    AppUserManager.Delete(user);

                    return BadRequest("Unable to create organization.");
                }

                //Update user with organization id
                var savedOrganization = (OrganizationModel)response.ReturnObject;
                user.OrganizationId = savedOrganization.Id;
                AppUserManager.Update(user);

                return Ok();
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while registering.");
            }
        }

        [AllowAnonymous]
        [Route("sendInvitation")]
        [HttpPost]
        public IHttpActionResult SendInvitation(InvitationModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                    return Content(HttpStatusCode.BadRequest, error);
                }

                var organizationService = Resolve<IOrganizationService>();
                var inviteService = Resolve<IInvitationService>();

                //Validate organization
                var organizationInviteExists = inviteService.OrganizationInvitationExists(model.OrganizationName);
                var organizationExists = organizationService.OrganizationExists(model.OrganizationName);

                if (organizationInviteExists)
                    return BadRequest("Organization is already sent invitation request.");
                if (organizationExists)
                    return BadRequest("Organization is already registered.");

                //Save
                inviteService.CreateInvite(model);
                return Ok("Invitation sent successfully.");
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while sending invite.");
            }
        }

        [Route("updateUserSettings")]
        [Authorize]
        [HttpPost]
        public IHttpActionResult UpdateUserSettings(CreateUserModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                    return BadRequest(error);
                }

                var userToUpdate = AppUserManager.FindByName(model.Username);
                if (userToUpdate == null)
                {
                    return BadRequest("User does not exist");
                }

                else
                {

                    if (!string.IsNullOrWhiteSpace(model.Password))
                    {

                        if (this.AppUserManager.CheckPassword(userToUpdate, model.Password))
                        {
                            userToUpdate.FirstName = model.FirstName;
                            userToUpdate.LastName = model.LastName;
                            userToUpdate.Email = model.Email;
                            userToUpdate.PhoneNumber = model.PhoneNumber;
                            userToUpdate.OrganizationId = model.OrganizationId;

                            //Update user
                            var updateUserResult = AppUserManager.Update(userToUpdate);

                            if (!updateUserResult.Succeeded)
                            {
                                var error = updateUserResult.Errors.First();
                                return BadRequest(error);
                            }

                            return Ok("Update successfull.");

                        }

                        // password not matching, so failed to updated 
                        else
                        {
                            return BadRequest("Password does not match");
                        }
                    }
                    else
                    {
                        return BadRequest("Password required");

                    }
                }


            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while updating user settings");
                throw;
            }
        }

        [Route("getByUsername")]
        [HttpGet]
        public IHttpActionResult GetByUsername(string username)
        {
            try
            {
                var user = AppUserManager.FindByName(username);
                if (user != null)
                {
                    return Ok(TheModelFactory.Create(user));
                }
                return NotFound();

            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting IScout.");
            }
        }

        [Authorize]
        [Route("user/{username}")]
        public async Task<IHttpActionResult> GetUserByName(string username)
        {
            var user = await AppUserManager.FindByNameAsync(username);
            if (user != null)
            {
                return Ok(TheModelFactory.Create(user));
            }
            return NotFound();
        }

    }
}