﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Redwood.ServicesContracts.IServices;

namespace Redwood.WebApi.Controllers
{
    [RoutePrefix("api/home")]
    public class HomeController : BaseApiController
    {
        [Authorize]
        [Route("load")]
        [HttpPost]
        public IHttpActionResult Load()
        {
            return Ok();
        }

        [Authorize]
        [Route("getDashboard")]
        [HttpGet]
        public IHttpActionResult GetDashboard(int organizationId)
        {
            try
            {
                var homeService = Resolve<IHomeService>();
                var homeDashboard = homeService.GetDashboard(organizationId);

                return Ok(homeDashboard);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while loading dashboard.");
            }
        }

    }
}