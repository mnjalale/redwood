﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using Redwood.Core.Models;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.WebApi.Controllers
{
    [RoutePrefix("api/products")]
    public class ProductsController : BaseApiController
    {
        [Route("save")]
        [HttpPost]
        [Authorize]
        public IHttpActionResult Save(ProductModel product)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                    return Content(HttpStatusCode.BadRequest, error);
                }

                var productService = Resolve<IProductService>();

                var response = productService.InsertOrUpdate(product);

                if (!response.HasSucceeded)
                {
                    return BadRequest(response.Message);
                }
                var productModel = (ProductModel) response.ReturnObject;
                return Ok(productModel);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while saving product.");
            }
        }

        [Route("get")]
        [HttpGet]
        public IHttpActionResult Get(int organizationId)
        {
            try
            {
                var productService = Resolve<IProductService>();
                var modelFactory = Resolve<IEntityToModelFactory>();

                var productModels =
                    productService.Get(organizationId).Select(modelFactory.Create).OrderBy(c => c.Name).ToList();

                return Ok(productModels);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting products.");
            }
        }

        [Route("getAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            try
            {
                var productService = Resolve<IProductService>();
                var organizationService = Resolve<IOrganizationService>();
                var modelFactory = Resolve<IEntityToModelFactory>();

                var productModels =
                    productService.GetAll().Select(modelFactory.Create).OrderBy(c => c.Name).ToList();

                foreach (var productModel in productModels)
                {
                    productModel.Organization =
                        modelFactory.Create(organizationService.GetById(productModel.OrganizationId));
                }

                productModels = productModels.OrderBy(c => c.Organization.Name).ThenBy(c => c.Name).ToList();

                return Ok(productModels);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting products.");
            }
        }

        [Route("getById")]
        [HttpGet]
        public IHttpActionResult GetById(int organizationId,int productId)
        {
            try
            {
                var productService = Resolve<IProductService>();
                var modelFactory = Resolve<IEntityToModelFactory>();

                var product =
                    productService.GetById(productId,organizationId);
                var productModel = modelFactory.Create(product);

                return Ok(productModel);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting products.");
            }
        }
    }
}