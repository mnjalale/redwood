﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity.Owin;
using Redwood.Core.Entities;
using Redwood.Core.Models.AppUsers;
using Redwood.Services.Infrastructure;
using Redwood.ServicesContracts.IFactories;
using Redwood.WebApi.Models;

namespace Redwood.WebApi.Controllers.Authentication
{
    public class BaseAuthenticationController : BaseApiController
    {
        private ModelFactory _modelFactory;
        private ApplicationUserManager _appUserManager = null;
        private ApplicationRoleManager _appRoleManager = null;

        protected ApplicationUserManager AppUserManager
        {
            get { return _appUserManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }

        protected ApplicationRoleManager AppRoleManager
        {
            get { return _appRoleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>(); }
        }

        protected ModelFactory TheModelFactory
        {
            get { return _modelFactory ?? (_modelFactory = new ModelFactory(Request, AppUserManager)); }
        }

        protected async Task<IHttpActionResult> CreateUser(CreateUserModel createUerModel)
        {
            var entityFactory = Resolve<IModelToEntityFactory>();
            var user = new User()
            {
                UserName = createUerModel.Username,
                Email = createUerModel.Email,
                FirstName = createUerModel.FirstName,
                Surname = createUerModel.LastName,
                PhoneNumber = createUerModel.PhoneNumber,
                OrganizationId = createUerModel.OrganizationId
            };

            var addUserResult = await this.AppUserManager.CreateAsync(user, createUerModel.Password);

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }

            //Add roles to user
            

            //Email confirmation
            var code = await AppUserManager.GenerateEmailConfirmationTokenAsync(user.Id);

            var callbackUrl = new Uri(Url.Link("ConfirmEmailRoute", new { userId = user.Id, code = code }));

            await
                AppUserManager.SendEmailAsync(user.Id, "Confirm your account",
                    "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

            var locationHeader = new Uri(Url.Link("GetUserById", new { id = user.Id }));
            return Created(locationHeader, TheModelFactory.Create(user));
        }
    }
}