﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Redwood.Core.Entities;
using Redwood.Core.Enums;
using Redwood.Core.Models;
using Redwood.Core.Models.Caytree;
using Redwood.Core.Utils;
using Redwood.NotificationBridge.Lib.Services.SMS.AfricasTalking;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.WebApi.Controllers
{
     [RoutePrefix("api/applications")]
    public class ApplicationsController : BaseApiController
    {
        [Route("save")]
        [HttpPost]
        public IHttpActionResult Save(LoanApplicationModel application)
        {
            try
            {

                //if (application.Customer!=null)
                //{
                //    if (application.Customer.Gender!= Gender.Male &&
                //        application.Customer.Gender!=Gender.Female)
                //    {
                //        application.Customer.Gender = (Gender?)null;
                //    }
                //}

                if (!ModelState.IsValid)
                {
                    if (ModelState.Keys.Count==1 && ModelState.Keys.First()=="application.customer.gender")
                    {
                        if (application.Customer != null &&
                        application.Customer.Gender != null &&
                        !Enum.IsDefined(typeof(Gender), application.Customer.Gender))
                        {
                            application.Customer.Gender = null;
                        }
                    }
                    else
                    {
                        var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                        return BadRequest(error);
                    }
                    //var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                    //return BadRequest(error);
                }

                
                var applicationService = Resolve<IApplicationService>();  
                var response = applicationService.InsertOrUpdate(application);
                if (!response.HasSucceeded)
                {
                    return BadRequest(response.Message);
                }

                SendApplicationSmsAlert(application,response);


                if (application.Status == ApplicationStatus.Disbursed)
                {
                    return Ok((LoanModel) response.ReturnObject);
                }
                else
                {
                    return Ok((LoanApplicationModel) response.ReturnObject);
                }

            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while adding application.");
            }
        }

         private void SendApplicationSmsAlert(LoanApplicationModel application, OperationResponse response)
         {
            try
            {
                var applicationService = Resolve<IApplicationService>();  
                var firstname = application.Customer.FirstName;
                var lastname = application.Customer.LastName;
                var fullname = string.Concat(firstname, " ", lastname);
                var num = application.Customer.PhoneNumber;
                var loanApplicationId = application.Status == ApplicationStatus.Disbursed
                    ? ((LoanModel)response.ReturnObject).Id
                    : 0;
                var applicationId =
                    application.Status != ApplicationStatus.Disbursed
                        ? ((LoanApplicationModel) response.ReturnObject).Id
                        : application.Id;
                var organizationId = application.OrganizationId;
                var mobilePaymentPhoneNumber = application.Customer.MobilePaymentPhoneNumber;
                var caytreeWalletNumber = application.Customer.CaytreeWalletPhoneNumber;
                var applicationByCustomer = applicationService.GetById(applicationId, organizationId);
                var principalAmount = applicationByCustomer.Amount;
                var interestRate = applicationByCustomer.InterestRate;
                var repaymentPeriod = applicationByCustomer.RepaymentPeriod;
                var totalRepayment = applicationByCustomer.TotalRepayment;

                if (!num.StartsWith("+"))
                {
                    num = num.TrimStart(new char[] { '0' });
                    num = "+254" + num;
                }
                var phoneNumber = num.StartsWith("+") ? num : string.Concat("+", num);
                //Pending SMS
                if (application.Status == ApplicationStatus.Pending && application.IsSmsApplication)
                {
                    var smsText = string.Format("Dear {0}, your loan application of Principal Amount: Ksh.{1}, Interest Rate: {2}%, Repayment Period: {3} months, & " +
                                                "Total Repayment: Ksh.{4} has been received." +
                                                " Please send the following word LC#{5} to Accept the terms & conditions." +
                                                " Thank You. #Caytree", fullname, principalAmount.ToString("#,##0.00"), interestRate, repaymentPeriod, totalRepayment, applicationId);
                    Resolve<IAfricasTalkingProvider>().SendSMS(new List<string>() { num }, smsText);
                }
                if (application.Status == ApplicationStatus.Approved)
                {
                    var smsText = string.Format("Dear {0}, your loan application of Principal Amount: Ksh.{1}, Interest Rate: {2}%, Repayment Period: {3} months," +
                                                " & Total Repayment: Ksh.{4} has been APPROVED." +
                                                " We will notify you upon disbursement." +
                                                " Thank You. #Caytree", fullname, principalAmount.ToString("#,##0.00"), interestRate, repaymentPeriod, totalRepayment);
                    Resolve<IAfricasTalkingProvider>().SendSMS(new List<string>() { num }, smsText);
                }

                //if (application.Status == ApplicationStatus.Approved)
                //{
                //    var smsText = string.Format("Dear {0}, your money will be disbursed to: {1}, which is the mobile payment phone number you provided us with." +
                //                                "Please confirm by sending the word YES#{2} to 22347. If its not the number you provided us with, please contact us on +254735085854." +
                //                                " Thank You. #Caytree", fullname, mobilePaymentPhoneNumber, applicationId);
                //    Resolve<IAfricasTalkingProvider>().SendSMS(new List<string>() { num }, smsText);
                //}
                if (application.Status == ApplicationStatus.Disbursed)
                {
                    var smsText = string.Format("Dear {0}, your loan application of Principal Amount: Ksh.{1}, Interest Rate: {2}%, Repayment Period: {3} months, " +
                                                "& Total Repayment: Ksh.{4} has been DISBURSED to your Caytree Wallet number {5}. " +
                                                "Thank You. #Caytree", fullname, principalAmount.ToString("#,##0.00"), interestRate, repaymentPeriod, totalRepayment, caytreeWalletNumber);
                    Resolve<IAfricasTalkingProvider>().SendSMS(new List<string>() { num }, smsText);
                }
            }
            catch (Exception)
            {
                //Just continue
            }
             
         }

        [Route("get")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult Get(int organizationId)
        {
            try
            {
                var applicationService = Resolve<IApplicationService>();
                var modelFactory = Resolve<IEntityToModelFactory>();

                var applicationModels =
                    applicationService.Get(organizationId).OrderBy(c => c.ApplicationDate).ToList();

                return Ok(applicationModels);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting applications.");
            }
        }

        [Route("getByStatus")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult Get(int organizationId,ApplicationStatus status)
        {
            try
            {
                var applicationService = Resolve<IApplicationService>();

                var applications = applicationService.Get(organizationId,status).OrderBy(c=>c.ApplicationDate).ToList();
                return Ok(applications);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting applications.");
            }
        }

        [Route("getCaytreeCompanyApplications")]
        [HttpGet]
        public IHttpActionResult GetCaytreeCompanyApplications(int companyId)
        {
            try
            {
                var applicationService = Resolve<IApplicationService>();
                var applications = applicationService.GetApplicationsForCaytreeUser(companyId).OrderBy(c=>c.ApplicationDate).ToList();

                return Ok(applications);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting applications.");
            }
        }

        [Route("getCaytreeCompanyApplicationDashboard")]
        [HttpGet]
        public IHttpActionResult GetCaytreeCompanyApplicationDashboard(int companyId)
        {
            try
            {
                var applicationService = Resolve<IApplicationService>();

                var applications = applicationService.GetApplicationsForCaytreeUser(companyId);

                var applicationDashboard = new CaytreeApplicationDashboard
                {
                    ApplicationsCount = applications.Count,
                    ApplicationsAmount = applications.Sum(c => c.Amount)
                };
                
                return Ok(applicationDashboard);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting applications.");
            }
        }

        [Route("getById")]
        [HttpGet]  
        public IHttpActionResult GetById(int organizationId, int applicationId)
        {
            try
            {
                var applicationService = Resolve<IApplicationService>();
                var modelFactory = Resolve<IEntityToModelFactory>();

                var application =
                    applicationService.GetById(applicationId, organizationId);

                return Ok(application);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting application.");
            }
        }

        [Route("reject")]
        [HttpPost]
        public IHttpActionResult Reject(RejectApplicationModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                    return BadRequest(error);
                }

                if (!model.ApplicationIncomplete && !model.IncompleteDocuments 
                    && !model.LoanAmountNotQualified && string.IsNullOrWhiteSpace(model.OtherReasons))
                {
                    return BadRequest("Please indicate a rejection reason.");
                } 
                var applicationService = Resolve<IApplicationService>();  
                var response = applicationService.RejectApplication(model); 
                if (!response.HasSucceeded)
                {
                    return BadRequest("Errors occurred while rejecting application.");
                }

                var applicationModel = (LoanApplicationModel)response.ReturnObject; 

                #region  Send User Application Status Message

                var firstname = applicationModel.Customer.FirstName;
                var lastname = applicationModel.Customer.LastName;
                var fullname = string.Concat(firstname, " ", lastname);
                var num = applicationModel.Customer.PhoneNumber;
                var loanApplicationId = applicationModel.Id;
                var organizationId = applicationModel.OrganizationId;
                var applicationByCustomer = applicationService.GetById(loanApplicationId, organizationId);
                var principalAmount = applicationByCustomer.Amount;
                var interestRate = applicationByCustomer.InterestRate;
                var repaymentPeriod = applicationByCustomer.RepaymentPeriod;
                var totalRepayment = applicationByCustomer.TotalRepayment;

                if (!num.StartsWith("+"))
                {
                    num = num.TrimStart(new char[] { '0' });
                    num = "+254" + num;
                } 
                if (applicationModel.Status == ApplicationStatus.Rejected)
                {
                    var smsText = string.Format("Dear {0}, your loan application ref no:{1} of Principal Amount: Ksh.{2}, Interest Rate: {3}%, Repayment Period: {4} months, & " +
                                                "Total Repayment: Ksh.{5} has been REJECTED. For more information please contact us on +254735085854 or send HELP to 22347.  #Caytree", fullname, loanApplicationId, principalAmount.ToString("#,##0.00"), interestRate, repaymentPeriod, totalRepayment);
                    Resolve<IAfricasTalkingProvider>().SendSMS(new List<string>() { num }, smsText);
                } 
                #endregion
                return Ok(applicationModel); 
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while rejecting application.");
            }
        }

        [Route("cancel")]
        [HttpPost]
        public IHttpActionResult CancelApplication(LoanApplicationModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                    return BadRequest(error);
                }
                
                var applicationService = Resolve<IApplicationService>();

                var response = applicationService.CancelApplication(model);

                if (!response.HasSucceeded)
                {
                    return BadRequest("Errors occurred while cancelling application.");
                }

                var applicationModel = (LoanApplicationModel)response.ReturnObject;
                return Ok(applicationModel);

            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while cancelling application.");
            }
        }

        [Route("test")]
        [HttpGet]
        public IHttpActionResult GetCaytreeTransactions(int companyId)
        {
            try
            {
                var caytreeService = Resolve<ICaytreeService>();
                var transactions = caytreeService.GetCaytreeCashAndCashEquivalentAccounts(companyId);

                var crbService = Resolve<ICrbService>();
                var trans = crbService.GetUserCrbScores("26448640");
               

                return Ok(trans);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting transactions.");
            }
        }
    }
}