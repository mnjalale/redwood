﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.WebApi.Controllers
{
    [RoutePrefix("api/test")]
    public class TestController : BaseApiController
    {
        [Route("getCaytreeInvoices")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                var invoiceService = Resolve<ICaytreeService>();

                var invoices =
                    invoiceService.GetCaytreeInvoices(11262).ToList();

                return Ok(invoices);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting products.");
            }
        }

        [Route("getCaytreeImportedTransactions")]
        [HttpGet]
        public IHttpActionResult GetImportedTransactions()
        {
            try
            {
                var importedTransactionService = Resolve<ICaytreeService>();

                var importedTransactions =
                    importedTransactionService.GetCaytreeImportedTransactions(5592).ToList();

                return Ok(importedTransactions);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting products.");
            }
        }

        [Route("getCaytreeContactDetails")]
        [HttpGet]
        public IHttpActionResult GetCaytreeContactDetails()
        {
            try
            {
                var importedTransactionService = Resolve<ICaytreeService>();

                var importedTransactions =
                    importedTransactionService.GetInvoiceSupplier(7618, new Guid("6C868343-CB47-4A24-9129-FAF938A3E918"));

                return Ok(importedTransactions);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting products.");
            }
        }
    }
}