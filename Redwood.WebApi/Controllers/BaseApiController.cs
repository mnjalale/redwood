﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Redwood.ServicesContracts.IFactories;

namespace Redwood.WebApi.Controllers
{
    public class BaseApiController : ApiController
    {
        protected IEntityToModelFactory DtoFactory;
        
        public BaseApiController()
        {
            DtoFactory = Resolve<IEntityToModelFactory>();
            GetCurrentUser();
        }

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    //No ModelState errors are available to sent, so just return an empty BadRequest
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        protected void GetCurrentUser()
        {
            var user = this.User;
        }

        protected T Resolve<T>() where T : class
        {
            return IoC.IoC.Resolve<T>();
        }
    }
}