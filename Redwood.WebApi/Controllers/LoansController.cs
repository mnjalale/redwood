﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using Redwood.Core.Entities;
using Redwood.Core.Enums;
using Redwood.Core.Models;
using Redwood.Core.Models.Caytree;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;

namespace Redwood.WebApi.Controllers
{
    [RoutePrefix("api/loans")]
    public class LoansController : BaseApiController
    {
        [Route("save")]
        [HttpPost]
        public IHttpActionResult Save(LoanModel loan)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                    return BadRequest(error);
                }

                var loanService = Resolve<ILoanService>();

                var response = loanService.InsertOrUpdate(loan);

                if (!response.HasSucceeded)
                {
                    return BadRequest("Errors occurred while adding loan.");
                }

                var loanModel = (LoanModel)response.ReturnObject;
                return Ok(loanModel);

            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while adding loan.");
            }
        }

        [Route("savePayment")]
        [HttpPost]
        public IHttpActionResult SavePayment(LoanPaymentModel payment)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                    return BadRequest(error);
                }

                var loanService = Resolve<ILoanService>();

                var response = loanService.InsertOrUpdatePayment(payment);

                if (!response.HasSucceeded)
                {
                    return BadRequest("Errors occurred while adding payment.");
                }

                var loanModel = (LoanModel)response.ReturnObject;
                loanModel.LoanPayments =
                    loanModel.LoanPayments.OrderByDescending(c => c.Date).ThenByDescending(c => c.Id).ToList();
                return Ok(loanModel);

            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while adding payment.");
            }
        }

        [Route("get")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult Get(int organizationId)
        {
            try
            {
                var loanService = Resolve<ILoanService>();
                var modelFactory = Resolve<IEntityToModelFactory>();

                var loanModels =
                    loanService.Get(organizationId).Select(modelFactory.Create).OrderBy(c => c.DisbursalDate).ToList();

                return Ok(loanModels);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting loans.");
            }
        }

        [Route("getCaytreeCompanyLoans")]
        [HttpGet]
        public IHttpActionResult GetCaytreeCompanyLoans(int companyId)
        {
            try
            {
                var loanService = Resolve<ILoanService>();
                var modelFactory = Resolve<IEntityToModelFactory>();

                var loans = loanService.GetLoansForCaytreeUser(companyId);
                var loanModels =
                    loans.Select(modelFactory.Create).OrderBy(c => c.DisbursalDate).ToList();


                return Ok(loanModels);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting loans.");
            }
        }

        [Route("getCaytreeCompanyLoanDashboard")]
        [HttpGet]
        public IHttpActionResult GetCaytreeCompanyLoanDashboard(int companyId)
        {
            try
            {
                var loanService = Resolve<ILoanService>();
                var applicationService = Resolve<IApplicationService>();
                decimal qualificationAmount = 0;
                decimal potentialQualificationAmount = 0;

                var modelFactory = Resolve<IEntityToModelFactory>();

                var loans = loanService.GetLoansForCaytreeUser(companyId);
                var applications =
                    applicationService.GetApplicationsForCaytreeUser(companyId)
                        .Where(c => c.Status != ApplicationStatus.Disbursed);

                applicationService.GetQualificationAmountFromInvoice(companyId, ref qualificationAmount,
                    ref potentialQualificationAmount);
                
                var loanDashboard = new CaytreeLoanDashboard
                {
                    LoansCount = loans.Count,
                    NextRepaymentDate = DateTime.Today,
                    NextRepaymentAmount = 0,
                    LoansAmount = loans.Sum(c => c.Amount),
                    Loans = loans.OrderBy(c => c.DisbursalDate).Select(modelFactory.Create).ToList(),
                    Applications = applications.OrderBy(c => c.ApplicationDate).ToList(),
                    OverduePaymentsCount = 0,
                    QualificationAmount = qualificationAmount,
                    PotentialQualificationAmount = potentialQualificationAmount
                };
                loanDashboard.LoansBalance = loanDashboard.Loans.Sum(c => c.Balance);

                return Ok(loanDashboard);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting applications.");
            }
        }

        [Route("getByStatus")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult Get(int organizationId, string status)
        {
            try
            {
                var loanService = Resolve<ILoanService>();
                var modelFactory = Resolve<IEntityToModelFactory>();

                List<Loan> loan;
                switch (status)
                {
                    case "Not Started":
                        loan = loanService.Get(organizationId, LoanStatus.NotStarted);
                        break;
                    case "Active":
                        loan = loanService.Get(organizationId, LoanStatus.Active);
                        break;
                    case "Completed":
                        loan = loanService.Get(organizationId, LoanStatus.Completed);
                        break;
                    case "Late Payment":
                        loan = loanService.Get(organizationId, LoanStatus.LatePayment);
                        break;
                    case "Defaulted":
                        loan = loanService.Get(organizationId, LoanStatus.Defaulted);
                        break;
                    default:
                        loan = loanService.Get(organizationId);
                        break;
                }

                var loanModels =
                    loan.Select(modelFactory.Create).OrderBy(c => c.DisbursalDate).ToList();
                
                return Ok(loanModels);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting applications.");
            }
        }
        
        [Route("getById")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult GetById(int organizationId, int loanId)
        {
            try
            {
                var loanService = Resolve<ILoanService>();
                var applicationService = Resolve<IApplicationService>();
                var modelFactory = Resolve<IEntityToModelFactory>();

                var loan =
                    loanService.GetById(loanId, organizationId);
                var loanModel = modelFactory.Create(loan);
                loanModel.Application = applicationService.GetById(loan.ApplicationId, loan.OrganizationId);
                loanModel.LoanPayments = loanModel.LoanPayments.OrderByDescending(c => c.Date).ToList();
                return Ok(loanModel);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting loan.");
            }
        }
    }
}