﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using Redwood.Core.Models;
using Redwood.ServicesContracts.IFactories;
using Redwood.ServicesContracts.IServices;
using Redwood.WebApi.Models;

namespace Redwood.WebApi.Controllers
{
    [RoutePrefix("api/organizations")]
    [Authorize]
    public class OrganizationController : BaseApiController
    {
        [Route("save")]
        [HttpPost]
        public IHttpActionResult Save(OrganizationModel organization)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                    return Content(HttpStatusCode.BadRequest, error);
                }

                var organizaitonService = Resolve<IOrganizationService>();

                var response = organizaitonService.InsertOrUpdate(organization);

                if (!response.HasSucceeded)
                {
                    return BadRequest("Errors occurred while saving organization.");
                }
                var organizationModel = (OrganizationModel)response.ReturnObject;
                return Ok(organizationModel);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while saving organization.");
            }
        }

        [Route("getById")]
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var organizationService = Resolve<IOrganizationService>();
                var modelFactory = Resolve<IEntityToModelFactory>();

                var organization =
                    organizationService.GetById(id);
                var organizationModel = modelFactory.Create(organization);

                return Ok(organizationModel);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting organization.");
            }
        }

        [HttpGet]
        [Route("getLogo/{organizationId}")]
        [AllowAnonymous]
        public IHttpActionResult GetLogo(int organizationId)
        {
            try
            {
                var organizationService = Resolve<IOrganizationService>();
                var imageBytes = organizationService.GetById(organizationId).Logo;

                if (imageBytes == null)
                {
                    SaveDefaultImage(organizationId);
                    imageBytes = organizationService.GetById(organizationId).Logo;
                    if (imageBytes == null) return Ok();
                }

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var ms = new MemoryStream(imageBytes);
                response.Content = new StreamContent(ms);
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");

                return ResponseMessage(response);

            }
            catch (Exception)
            {
               return BadRequest("Errors occurred while getting logo.");
            }
        }

        [HttpPost]
        [Route("saveLogo/{organizationId}")]
        public Task<IEnumerable<string>> SaveLogo(int organizationId)
        {
            try
            {
                if (Request.Content.IsMimeMultipartContent())
                {
                    var fullPath = HttpContext.Current.Server.MapPath("~/app_data/uploads");

                    if (!Directory.Exists(fullPath))
                    {
                        Directory.CreateDirectory(fullPath);
                    }

                    var streamProvider = new MultipartFormDataStreamProvider(fullPath);

                    var task = Request.Content.ReadAsMultipartAsync(streamProvider)
                        .ContinueWith(t =>
                        {
                            if (t.IsFaulted || t.IsCanceled)
                                throw new HttpResponseException(HttpStatusCode.InternalServerError);
                            var fileInfo = streamProvider.FileData.Select(i =>
                            {
                                var info = new FileInfo(i.LocalFileName);
                                var imageBytes = File.ReadAllBytes(info.FullName);


                                var organizationService = Resolve<IOrganizationService>();
                                var response = organizationService.UpdateLogo(organizationId, imageBytes);

                                return response.HasSucceeded
                                    ? "File uploaded successfully!"
                                    : "Errors occurred while uploading image!";

                            });
                            return fileInfo;
                        });
                    return task;
                }
                else
                {
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, "Invalid Request!"));
                }
            }
            catch (Exception ex)
            {

                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotAcceptable, "Invalid Request!"));
            }
        }

        [HttpDelete]
        [Route("deleteLogo/{organizationId}")]
        public IHttpActionResult DeleteLogo(int organizationId)
        {
            try
            {
                var organizationService = Resolve<IOrganizationService>();
                var operationResponse = organizationService.DeleteLogo(organizationId);

                if (operationResponse.HasSucceeded)
                {
                    SaveDefaultImage(organizationId);
                    return Ok();
                }

                return BadRequest("Errors occurred while deleting logo");
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while deleting logo.");
            }
        }

        [Route("saveWallet")]
        [HttpPost]
        public IHttpActionResult SaveWallet(OrganizationWalletModel organizationWallet)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(c => c.Errors).First().ErrorMessage;
                    return Content(HttpStatusCode.BadRequest, error);
                }

                var organizaitonWalletService = Resolve<IOrganizationWalletService>();

                var response = organizaitonWalletService.InsertOrUpdate(organizationWallet);

                if (!response.HasSucceeded)
                {
                    return BadRequest("Errors occurred while saving wallet.");
                }
                var organizationWalletModel = (OrganizationWalletModel)response.ReturnObject;
                return Ok(organizationWalletModel);
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while saving wallet.");
            }
        }

        [Route("getOrganizationWallet")]
        [HttpGet]
        public IHttpActionResult GetOrganizationWallets(int organizationId)
        {
            try
            {
                var organizationWalletService = Resolve<IOrganizationWalletService>();
                var modelFactory = Resolve<IEntityToModelFactory>();

                var organizationWallets =
                    organizationWalletService.GetOrganizationWallets(organizationId);

                return Ok(organizationWallets.Select(modelFactory.Create));

                
            }
            catch (Exception)
            {
                return BadRequest("Errors occurred while getting organization wallets.");
            }
        }

        #region helpers

        private void SaveDefaultImage(int organizationId)
        {
            try
            {
                var organizationService = Resolve<IOrganizationService>();
                var modelFactory = Resolve<IEntityToModelFactory>();
                var organization = organizationService.GetById(organizationId);
                var filePath = HttpContext.Current.Server.MapPath("~/app_data/uploads/img-default.png");
                var defaultImage = Image.FromFile(filePath);
                var imageConverter = new ImageConverter();
                var defaultImageBytes = (byte[]) imageConverter.ConvertTo(defaultImage, typeof (byte[]));
                organization.Logo = defaultImageBytes;
                organizationService.InsertOrUpdate(modelFactory.Create(organization));
            }
            catch (Exception)
            {
                //Fail silently for now
            }
        }

        #endregion
    }
}