﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Data.Context;
using Redwood.DataContracts.IRepositories;

namespace Redwood.Data.Repositories
{
    internal class CustomerRepository : RepositoryBase<Customer>,ICustomerRepository
    {
        public CustomerRepository(RedwoodContext context) : base(context)
        {
        }
    }
}
