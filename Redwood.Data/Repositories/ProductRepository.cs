﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Data.Context;
using Redwood.DataContracts.IRepositories;

namespace Redwood.Data.Repositories
{
    internal class ProductRepository : RepositoryBase<Product>,IProductRepository
    {
        public ProductRepository(RedwoodContext context) : base(context)
        {
        }
    }
}
