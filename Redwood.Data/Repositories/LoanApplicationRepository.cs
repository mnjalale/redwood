﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Data.Context;
using Redwood.DataContracts.IRepositories;

namespace Redwood.Data.Repositories
{
    internal class LoanApplicationRepository : RepositoryBase<LoanApplication>,ILoanApplicationRepository
    {
        public LoanApplicationRepository(RedwoodContext context) : base(context)
        {
        }
    }
}
