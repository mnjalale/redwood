﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities.Base;
using Redwood.Core.Enums;
using Redwood.Data.Context;
using Redwood.DataContracts.IRepositories;

namespace Redwood.Data.Repositories
{
    internal class RepositoryBase<T> : IRepositoryBase<T> where T : Entity
    {
        internal RedwoodContext Context;
        internal DbSet<T> DbSet;

        public RepositoryBase(RedwoodContext context)
        {
            this.Context = context;
            this.DbSet = context.Set<T>();
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "", bool showInactive = false)
        {
            try
            {
                IQueryable<T> query = DbSet;

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }

                query = showInactive
                            ? query.Where(c => c.EntityStatus != EntityStatus.Deleted).OrderBy(c => c.LastUpdated)
                            : query.Where(c => c.EntityStatus == EntityStatus.Active).OrderBy(c => c.LastUpdated);

                return orderBy != null ? orderBy(query) : query;
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while getting!");
            }
        }

        public T GetById(object id)
        {
            try
            {
                return DbSet.Find(id);
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while getting by Id");
            }
        }

        void Insert(T entity)
        {
            try
            {
                entity.DateCreated = DateTime.UtcNow;
                entity.LastUpdated = DateTime.UtcNow;
                DbSet.Add(entity);
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while inserting!");
            }
        }

        public void Delete(object id)
        {
            try
            {
                var entityToDelete = GetById(id);
                Delete(entityToDelete);
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while deleting!");
            }
        }

        public void Delete(T entityToDelete)
        {
            try
            {
                entityToDelete.EntityStatus = EntityStatus.Deleted;
                Update(entityToDelete);
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while deleting!");
            }
        }

        void Update(T entityToUpdate)
        {
            try
            {
                entityToUpdate.LastUpdated = DateTime.UtcNow;
                DbSet.Attach(entityToUpdate);
                Context.Entry(entityToUpdate).State = EntityState.Modified;
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while updating!");
            }
        }

        public void InsertOrUpdate(T entity)
        {
            try
            {
                if (entity.Id <= 0)
                    entity.DateCreated = DateTime.UtcNow;

                entity.LastUpdated = DateTime.UtcNow;
                DbSet.AddOrUpdate(entity);
            }
            catch (Exception)
            {
                throw new Exception("Errors occurred while inserting/updating!");
            }
           
        }

        public void Inactivate(int id)
        {
            try
            {
                var entity = GetById(id);
                entity.EntityStatus = EntityStatus.Inactive;
                Update(entity);
            }
            catch (Exception)
            {

                throw new Exception("Errors occurred while inactivating!");
            }
        }

        public void Activate(int id)
        {
            try
            {
                var entity = GetById(id);
                entity.EntityStatus = EntityStatus.Active;
                Update(entity);
            }
            catch (Exception)
            {
                throw new Exception("Errors occurred while activating!");
            }
        }

        public void Restore(int id)
        {
            try
            {
                var entity = GetById(id);
                entity.EntityStatus = EntityStatus.Active;
                Update(entity);
            }
            catch (Exception)
            {
                throw new Exception("Errors occurred while restoring!");
            }
        }

        //Gets added objects that haven't been committed as yet
        public IEnumerable<T> GetAddedEntities()
        {
            var query =
               ((IObjectContextAdapter)Context).ObjectContext.ObjectStateManager.GetObjectStateEntries(
                   EntityState.Added).Select(c => c.Entity).OfType<T>();

            return query;
        }
    }
}
