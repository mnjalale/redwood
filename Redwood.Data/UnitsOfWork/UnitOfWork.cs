﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Data.Context;
using Redwood.Data.Repositories;
using Redwood.DataContracts.IRepositories;
using Redwood.DataContracts.IUnitsOfWork;

namespace Redwood.Data.UnitsOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly RedwoodContext _context;
        //private readonly Container _container;
        public UnitOfWork()
        {
            _context = new RedwoodContext();
            //_container =new Container();
        }

        private ICustomerRepository _customerRepository;
        public ICustomerRepository CustomerRepository
        {
            get { return _customerRepository ?? (_customerRepository = new CustomerRepository(_context)); }
        }

        private IInvitationRepository _invitationRepository;
        public IInvitationRepository InvitationRepository
        {
            get { return _invitationRepository ?? (_invitationRepository = new InvitationRepository(_context)); }
        }

        private ILoanAmortizationScheduleRepository _loanAmortizationScheduleRepository;
        public ILoanAmortizationScheduleRepository LoanAmortizationScheduleRepository
        {
            get { return _loanAmortizationScheduleRepository ?? (_loanAmortizationScheduleRepository = new LoanAmortizationScheduleRepository(_context)); }
        }

        private ILoanApplicationRepository _loanApplicationRepository;
        public ILoanApplicationRepository LoanApplicationRepository
        {
            get { return _loanApplicationRepository ?? (_loanApplicationRepository = new LoanApplicationRepository(_context)); }
        }

        private ILoanApplicationAppraisalRepository _loanApplicationAppraisalRepository;
        public ILoanApplicationAppraisalRepository LoanApplicationAppraisalRepository
        {
            get { return _loanApplicationAppraisalRepository ?? (_loanApplicationAppraisalRepository = new LoanApplicationAppraisalRepository(_context)); }
        }

        private ILoanApplicationDocumentRepository _loanApplicationDocumentRepository;
        public ILoanApplicationDocumentRepository LoanApplicationDocumentRepository
        {
            get { return _loanApplicationDocumentRepository ?? (_loanApplicationDocumentRepository = new LoanApplicationDocumentRepository(_context)); }
        }

        private ILoanApplicationNoteRepository _loanApplicationNoteRepository;
        public ILoanApplicationNoteRepository LoanApplicationNoteRepository
        {
            get { return _loanApplicationNoteRepository ?? (_loanApplicationNoteRepository = new LoanApplicationNoteRepository(_context)); }
        }

        private ILoanRepository _loanRepository;
        public ILoanRepository LoanRepository
        {
            get { return _loanRepository ?? (_loanRepository = new LoanRepository(_context)); }
        }

        private ILoanPaymentRepository _loanPaymentRepository;
        public ILoanPaymentRepository LoanPaymentRepository
        {
            get { return _loanPaymentRepository ?? (_loanPaymentRepository = new LoanPaymentRepository(_context)); }
        }

        private IOrganizationRepository _organizationRepository;
        public IOrganizationRepository OrganizationRepository {
            get { return _organizationRepository ?? (_organizationRepository = new OrganizationRepository(_context)); }
        }

        private IOrganizationWalletRepository _organizationWalletRepository;
        public IOrganizationWalletRepository OrganizationWalletRepository
        {
            get { return _organizationWalletRepository ?? (_organizationWalletRepository = new OrganizationWalletRepository(_context)); }
        }

        private IProductCategoryRepository _productCategoryRepository;
        public IProductCategoryRepository ProductCategoryRepository {
            get
            {
                return _productCategoryRepository ??
                       (_productCategoryRepository = new ProductCategoryRepository(_context));
            } 
        }

        private IProductFeeRepository _productFeeRepository;

        public IProductFeeRepository ProductFeeRepository
        {
            get { return _productFeeRepository ?? (_productFeeRepository = new ProductFeeRepository(_context)); }
        }

        private IProductRepository _productRepository;
        public IProductRepository ProductRepository
        {
            get { return _productRepository ?? (_productRepository = new ProductRepository(_context)); }
        }


        public void Save()
        {
            try
            {
                var er = _context.GetValidationErrors().ToList();
                if (er.Count > 0)
                {
                    throw new Exception(er[0].ValidationErrors.First().ErrorMessage);
                }
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Errors occurred while saving: " + ex.Message);
            }
        }

        private bool _disposed;
        public void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
