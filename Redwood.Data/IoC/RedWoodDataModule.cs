﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using Redwood.Data.Context;
using Redwood.Data.Repositories;
using Redwood.Data.UnitsOfWork;
using Redwood.DataContracts.IRepositories;
using Redwood.DataContracts.IUnitsOfWork;

namespace Redwood.Data.IoC
{
    public class RedwoodDataModule : NinjectModule
    {
        public override void Load()
        {
            //Context
            Bind<RedwoodContext>().To<RedwoodContext>();

            //Repositories
            Bind<ICustomerRepository>().To<CustomerRepository>();
            Bind<IInvitationRepository>().To<InvitationRepository>();
            Bind<ILoanAmortizationScheduleRepository>().To<LoanAmortizationScheduleRepository>();
            Bind<ILoanApplicationAppraisalRepository>().To<LoanApplicationAppraisalRepository>();
            Bind<ILoanApplicationDocumentRepository>().To<LoanApplicationDocumentRepository>();
            Bind<ILoanApplicationNoteRepository>().To<LoanApplicationNoteRepository>();
            Bind<ILoanApplicationRepository>().To<LoanApplicationRepository>();
            Bind<ILoanPaymentRepository>().To<LoanPaymentRepository>();
            Bind<ILoanRepository>().To<LoanRepository>();
            Bind<IOrganizationRepository>().To<OrganizationRepository>();
            Bind<IOrganizationWalletRepository>().To<OrganizationWalletRepository>();
            Bind<IProductCategoryRepository>().To<ProductCategoryRepository>();
            Bind<IProductFeeRepository>().To<ProductFeeRepository>();
            Bind<IProductRepository>().To<ProductRepository>();

            //Unit of Work
            Bind<IUnitOfWork>().To<UnitOfWork>();
        }
    }
}
