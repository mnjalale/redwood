using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Redwood.Core.Entities;

namespace Redwood.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<Redwood.Data.Context.RedwoodContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Redwood.Data.Context.RedwoodContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //


            context.Organizations.AddOrUpdate(
                p => p.Name,
                new Organization
                {
                    Name = "Caytree Partners",
                    Email = "info@caytree.com",
                    PhoneNumber = "0777777777",
                    PostalAddress = "P.O. Box 111111 00100",
                    Website = "www.caytree.com",
                    ContactPersonName = "John Doe",
                    ContactPersonEmail = "johndoe@caytree.com"

                });
            context.SaveChanges();

            //Seed users
            var manager = new UserManager<User>(new UserStore<User>(context));

            var user = manager.FindByName("caytree");

            if (user == null)
            {
                user = new User()
                {
                    UserName = "caytree",
                    Email = "info@caytree.com",
                    EmailConfirmed = true,
                    FirstName = "John",
                    LastName = "Doe",
                    OrganizationId = 1
                };

                manager.Create(user, "12345678");
            }

            //var musoniUser = manager.FindByName("musoni");

            //if (musoniUser == null)
            //{
            //    user = new User()
            //    {
            //        UserName = "musoni",
            //        Email = "info@musoni.com",
            //        EmailConfirmed = true,
            //        FirstName = "Musoni",
            //        LastName = "User",
            //        OrganizationId = 3
            //    };

            //    manager.Create(user, "Lending2016");
            //}


        }
    }
}
