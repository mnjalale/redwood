﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Redwood.Core.Entities;

namespace Redwood.Data.Context
{
    public class RedwoodContext : IdentityDbContext<User>
    {
        public RedwoodContext()
            :base(ConnectionString)
        {
            //Configuration.LazyLoadingEnabled = false;
            //Configuration.ProxyCreationEnabled = false;
        }

        public static RedwoodContext Create()
        {
            return new RedwoodContext();
        }

        internal static string ConnectionString
        {
            get
            {
#if DEBUG
                var builder = new SqlConnectionStringBuilder
                {
                    DataSource = ".",
                    InitialCatalog = "Redwood",
                    UserID = "sa",
                    Password = "pass123",
                    ConnectTimeout = 30,
                    TrustServerCertificate = false,
                    Encrypt = true
                };

                //var builder = new SqlConnectionStringBuilder
                //{
                //    DataSource = ".",
                //    InitialCatalog = "Redwood",
                //    IntegratedSecurity = true,
                //    // UserID = "sa",
                //    //  Password = "",
                //    ConnectTimeout = 30,
                //    // TrustServerCertificate = false
                //    //Encrypt = true
                //};

                return builder.ConnectionString;
#else
                var builder = new SqlConnectionStringBuilder
                {
                    DataSource = "tcp:caytree.database.windows.net,1433",
                    InitialCatalog = "Redwood",
                    UserID = "caytree@caytree",
                    Password = "L3novo2013a",
                    ConnectTimeout = 30,
                    TrustServerCertificate = false,
                    Encrypt = true
                };

                return builder.ConnectionString;
#endif
            }
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Invitation> Invitations { get; set; }
        public DbSet<LoanAmortizationSchedule> LoanAmortizationSchedules { get; set; } 
        public DbSet<LoanApplication> LoanApplications { get; set; }
        public DbSet<LoanApplicationDocument> LoanApplicationDocuments { get; set; }
        public DbSet<LoanApplicationNote> LoanApplicationNotes { get; set; }
        public DbSet<LoanApplicationAppraisal> LoanApplicationAppraisals { get; set; } 
        public DbSet<LoanPayment> LoanPayments { get; set; } 
        public DbSet<Loan> Loans { get; set; } 
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<OrganizationWallet> OrganizationWallets { get; set; } 
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<ProductFee> ProductFees { get; set; } 
        
    }
}
