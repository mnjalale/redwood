﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json.Linq;

namespace Redwood.NotificationBridge.Lib.Services.SMS.AfricasTalking
{
    internal class AfricaTalkingSmsProvider : SmsGatewayBase, IAfricasTalkingProvider
    {
        private readonly string _username;
        private readonly string _apiKey;
        private static string URLString = "https://api.africastalking.com/version1/messaging";
       
        private readonly string gateway = "22347";
        public AfricaTalkingSmsProvider()
        {   
            _username = "Caytree";
            _apiKey = "f68c874cf4ac37887886a82c0340d79cd09995c6b88dd69285fcfea3e74b4ff3"; 
        }   

        public bool SendSMS(List<string> recipients, string message, string code = null)
        {
            return Send(string.Join(",", recipients), message);
        }

        public bool SendSMS(string recipient, string message)
        {
            return Send(recipient, message);
        }


        public bool SendSmsMessage(SmSContent smSContent)
        {
            return Send(smSContent.MobileNumber, smSContent.Message);
        } 

        private bool Send(string to, string message, int bulkSmsMode = 1)
        {
           
            if (IsValidSmsLength(message) && IsvalidRecipientNo(to))
            {
                try
                {
                    Hashtable data = new Hashtable();
                    data["username"] = _username;
                    data["to"] = to;
                    data["message"] = message;

                    data["from"] = gateway;
                    data["bulkSMSMode"] = Convert.ToString(bulkSmsMode);

                    string json = Post(data);
                    JObject obj = JObject.Parse(json);

                    JObject smsMessageData = (JObject)obj["SMSMessageData"];
                    var array = (JArray)smsMessageData["Recipients"];
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return false;

        }  

        string Post(Hashtable dataMap)
        {
            try
            {
                string dataStr = "";
                foreach (string key in dataMap.Keys)
                {
                    if (dataStr.Length > 0) dataStr += "&";
                    string value = (string)dataMap[key];
                    dataStr += HttpUtility.UrlEncode(key, Encoding.UTF8);
                    dataStr += "=" + HttpUtility.UrlEncode(value, Encoding.UTF8);
                }

                byte[] byteArray = Encoding.UTF8.GetBytes(dataStr);

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(URLString);

                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = byteArray.Length;
                webRequest.Accept = "application/json";

                webRequest.Headers.Add("apiKey", _apiKey);

                Stream webpageStream = webRequest.GetRequestStream();
                webpageStream.Write(byteArray, 0, byteArray.Length);
                webpageStream.Close();

                StreamReader webpageReader = new StreamReader(webRequest.GetResponse().GetResponseStream());

                return webpageReader.ReadToEnd();

            }
            catch (Exception e)
            {
                throw e;
            }
        }  
    }
}
