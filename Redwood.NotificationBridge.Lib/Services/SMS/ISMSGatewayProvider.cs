﻿using System.Collections.Generic;

namespace Redwood.NotificationBridge.Lib.Services.SMS
{
    public interface ISMSGatewayProvider
    {
        bool SendSMS(List<string> recipients, string message, string code = null);

        bool SendSMS(string recipients, string message);

        bool SendSmsMessage(SmSContent smSContents);  

    }
    public class SmSContent
    {
        public string MobileNumber { get; set; }
        public string Message { get; set; }
    }
}
