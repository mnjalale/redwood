﻿namespace Redwood.NotificationBridge.Lib.Services.SMS
{
    public class SmsGatewayBase
    {
        protected bool IsvalidRecipientNo(string recipientNo)
        {
            return recipientNo.Length > 10;
        }

        protected bool IsValidSmsLength(string message)
        {
            return (!string.IsNullOrWhiteSpace(message) /*&& message.Length>5 && message.Length <= 160*/);
        }
    }
}
