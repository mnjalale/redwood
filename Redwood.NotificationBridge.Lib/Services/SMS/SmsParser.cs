﻿using System;
using System.Linq;

namespace Redwood.NotificationBridge.Lib.Services.SMS
{
    public static class SmsParser
    { 
        public static void ParseText(string smsText, out  string fncCode, out String[] smsParams, out int count)
        {

            fncCode = smsText.Trim().ToLower();
            //ensure no one char command 
            smsParams = new string[3];
            smsParams[2] = "Unknown";
            char secondDigit = char.Parse("x");
            if (smsText.Length > 2)
            {
                secondDigit = char.Parse(smsText.Substring(1, 1));
            }


            if ((fncCode.Substring(0, 1).ToLower().Trim() == "e" || fncCode.Substring(0, 1).ToLower().Trim() == "i") && char.IsDigit(secondDigit) && smsText.Length > 2) // length to avoid i2 or e2 sms commands 
            {
                char lastChar = char.Parse("e");
                string alpha = string.Empty;
                string numer = string.Empty;
                int i = 0;

                smsParams[0] = null;
                foreach (char str in smsText)
                {
                    i++;
                    if (char.IsDigit(str))
                    {
                        smsParams[1] += str.ToString();
                    }
                    else
                    {
                        if (i != 1)
                        {
                            smsParams[2] = null;
                            smsParams[2] += smsText.Substring(i - 1, smsText.Length - (i - 1));
                            break;
                        }
                        else
                        {
                            smsParams[0] += str.ToString();
                            fncCode = str.ToString();
                        }
                    }
                    lastChar = str;

                }
                count = smsParams.Length;
            }
            else
            {

                smsParams = null;
                count = 0;


                if (smsText.Contains("#") || smsText.Contains(" "))
                {
                    if (smsText.Contains("#"))
                    {
                        smsParams = smsText.Split('#');
                        fncCode = smsParams[0];
                        count = smsParams.Length;
                    }
                    else if (smsText.Contains(' '))
                    {
                        smsParams = smsText.Split(' ');
                        fncCode = smsParams[0];
                        count = smsParams.Length;
                    }

                }
                else
                {
                    fncCode = smsText;
                    smsParams = smsText.Split('#');
                    count = smsParams.Length;
                }


            }

        }

    }
}
