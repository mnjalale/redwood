﻿using Ninject.Modules;
using Redwood.NotificationBridge.Lib.Services.SMS.AfricasTalking;

namespace Redwood.NotificationBridge.Lib.IoC
{
    public class RedWoodNotificationBridgeModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAfricasTalkingProvider>().To<AfricaTalkingSmsProvider>(); 
        }
    }
}
