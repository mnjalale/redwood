﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Enums;
using Redwood.Core.Models;

namespace Redwood.ServicesContracts.IFactories
{
    public interface IEntityToModelFactory
    {
        CustomerModel Create(Customer customer);
        InvitationModel Create(Invitation invitation);
        LoanAmortizationScheduleModel Create(LoanAmortizationSchedule loanAmortizationSchedule);
        LoanApplicationModel Create(LoanApplication loanApplication);
        LoanApplicationDocumentModel Create(LoanApplicationDocument loanApplicationDocument);
        LoanApplicationNoteModel Create(LoanApplicationNote loanApplicationNote);
        LoanApplicationAppraisalModel Create(LoanApplicationAppraisal loanApplicationAppraisal);
        LoanPaymentModel Create(LoanPayment loanPayment);
        LoanModel Create(Loan loan);
        OrganizationModel Create(Organization organization);
        OrganizationWalletModel Create(OrganizationWallet organization);
        ProductModel Create(Product product);
        ProductCategoryModel Create(ProductCategory productCategory);
        ProductFeeModel Create(ProductFee productFee);
    }
}
