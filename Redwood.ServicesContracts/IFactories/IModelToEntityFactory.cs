﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Enums;
using Redwood.Core.Models;

namespace Redwood.ServicesContracts.IFactories
{
    public interface IModelToEntityFactory
    {
        Customer Create(CustomerModel customerModel);
        Invitation Create(InvitationModel invitationModel);
        LoanAmortizationSchedule Create(LoanAmortizationScheduleModel loanAmortizationScheduleModel);
        LoanApplication Create(LoanApplicationModel loanApplicationModel);
        LoanApplicationDocument Create(LoanApplicationDocumentModel loanApplicationDocumentModel);
        LoanApplicationNote Create(LoanApplicationNoteModel loanApplicationNoteModel);
        LoanApplicationAppraisal Create(LoanApplicationAppraisalModel loanApplicationAppraisalModel);
        LoanPayment Create(LoanPaymentModel loanPaymentModel);
        Loan Create(LoanModel loanModel);
        Organization Create(OrganizationModel organizationModel);
        OrganizationWallet Create(OrganizationWalletModel organizationModel);
        Product Create(ProductModel productModel);
        ProductCategory Create(ProductCategoryModel productCategoryModel);
        ProductFee Create(ProductFeeModel productFeeModel);
    }
}
