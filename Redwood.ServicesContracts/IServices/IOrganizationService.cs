﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Models;
using Redwood.Core.Utils;

namespace Redwood.ServicesContracts.IServices
{
    public interface IOrganizationService
    {
        Organization GetById(int id);
        Organization GetByName(string name);
        Boolean OrganizationExists(int id);
        Boolean OrganizationExists(string organizationName);
        OperationResponse InsertOrUpdate(OrganizationModel organization);
        OperationResponse UpdateLogo(int organizationId, byte[] image);
        OperationResponse DeleteLogo(int organizationId);
    }
}
