﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Models;

namespace Redwood.ServicesContracts.IServices
{
    public interface IInvitationService
    {
        Boolean OrganizationInvitationExists(string organizationName);
        void CreateInvite(InvitationModel model);
    }
}
