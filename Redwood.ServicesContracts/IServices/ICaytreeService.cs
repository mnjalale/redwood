﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Models;
using Redwood.Core.Models.Caytree;

namespace Redwood.ServicesContracts.IServices
{
    public interface ICaytreeService
    {
        List<CaytreeTransaction> GetCaytreeTransactions(int companyId,int months);
        List<CaytreeFinancialAccount> GetCaytreeCashAndCashEquivalentAccounts(int companyId);
        List<CaytreeInvoice> GetCaytreeInvoices(int companyId);
        List<CaytreeImportedTransaction> GetCaytreeImportedTransactions(int companyId);
        CaytreeCompanyContact GetInvoiceSupplier(int companyId,Guid invoiceId);
    }
}
