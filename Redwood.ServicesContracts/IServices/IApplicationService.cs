﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Enums;
using Redwood.Core.Models;
using Redwood.Core.Utils;

namespace Redwood.ServicesContracts.IServices
{
    public interface IApplicationService
    {
        List<LoanApplicationModel> Get(int organizationId);
        List<LoanApplicationModel> Get(int organizationId, ApplicationStatus status);
        List<LoanApplicationModel> GetApplicationsForCaytreeUser(int caytreeCompanyId);
        LoanApplicationModel GetById(int id, int organizationId);
        OperationResponse InsertOrUpdate(LoanApplicationModel application);
        OperationResponse RejectApplication(RejectApplicationModel rejection);
        OperationResponse CancelApplication(LoanApplicationModel application);

        void GetQualificationAmountFromInvoice(int caytreeCompanyId, ref decimal qualificationAmount,
            ref decimal potentialQualificationAmount);

        bool ComputeAppraisal(decimal amount, int repaymentPeriod, decimal interestRate, decimal minimumLoanAmount,
            decimal applicantMonthlyIncome, decimal applicantCurrentTotalMonthlyLoanRepayments,
            decimal cashAndCashEquivalents, string customerCrbScore, decimal caytreeConfidenceScore, ref string message,ref List<LoanApplicationAppraisal> appraisals);
    }
}
