﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Models;
using Redwood.Core.Utils;

namespace Redwood.ServicesContracts.IServices
{
    public interface IProductService
    {
        List<Product> Get(int organizationId);
        List<Product> GetAll();
        Product GetById(int id,int organizationId);
        Product GetByName(string name, int organizationId);
        Boolean ProductExists(int id, int organizationId);
        Boolean ProductExists(string name, int organizationId);
        OperationResponse InsertOrUpdate(ProductModel product);

    }
}
