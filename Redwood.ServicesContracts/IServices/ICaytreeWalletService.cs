﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redwood.ServicesContracts.IServices
{
    public interface ICaytreeWalletService
    {
        bool WalletExists(string walletPhoneNumber);
        bool PostTransferTransaction(string customerWalletPhoneNumber, decimal amount);
    }
}
