﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Models;

namespace Redwood.ServicesContracts.IServices
{
    public interface ICrbService
    {
        List<CrbLoanModel> GetUserActiveLoans(string idNo);
        List<CrbScoreModel> GetUserCrbScores(string idNo);
    }
}
