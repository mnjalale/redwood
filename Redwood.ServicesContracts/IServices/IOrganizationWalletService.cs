﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Models;
using Redwood.Core.Utils;

namespace Redwood.ServicesContracts.IServices
{
    public interface IOrganizationWalletService
    {
        List<OrganizationWallet> GetOrganizationWallets(int organizationId);
        OrganizationWallet GetById(int id, int organizationId);
        OperationResponse InsertOrUpdate(OrganizationWalletModel wallet);
    }
}
