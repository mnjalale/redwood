﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Models;

namespace Redwood.ServicesContracts.IServices
{
    public interface IHomeService
    {
        HomeDashboardModel GetDashboard(int organizationId);
    }
}
