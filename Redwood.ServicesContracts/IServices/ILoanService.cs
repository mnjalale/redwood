﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redwood.Core.Entities;
using Redwood.Core.Enums;
using Redwood.Core.Models;
using Redwood.Core.Utils;

namespace Redwood.ServicesContracts.IServices
{
    public interface ILoanService
    {
        List<Loan> Get(int organizationId);
        List<Loan> Get(int organizationId, LoanStatus status);
        List<Loan> GetLoansForCaytreeUser(int caytreeCompanyId);
        Loan GetById(int id, int organizationId);
        OperationResponse InsertOrUpdate(LoanModel loan);
        OperationResponse InsertOrUpdatePayment(LoanPaymentModel payment);
    }
}
